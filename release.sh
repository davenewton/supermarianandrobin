#!/usr/bin/env sh
GITLAB_HOST="gitlab.com"
GITLAB_USER="davenewton"
SMR_VERSION="${SMR_VERSION}"

curl -u "${GITLAB_USER}:${GITLAB_TOKEN}" --upload-file game/build/supermarianandrobin.sfc https://${GITLAB_HOST}/api/v4/projects/${GITLAB_USER}%2Fsupermarianandrobin/packages/generic/supermarianandrobin/${SMR_VERSION}/supermarianandrobin.sfc | tee /dev/null

curl --request POST "https://${GITLAB_HOST}/api/v4/projects/${GITLAB_USER}%2Fsupermarianandrobin/releases" \
     --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
     --data @- <<REQUEST_BODY
{
  "name": "${SMR_VERSION}",
  "tag_name": "${SMR_VERSION}",
  "ref": "main",
  "description": "Release ${SMR_VERSION}",
  "assets": {
    "links": [
      {
        "name": "sfc",
        "url": "https://${GITLAB_HOST}/api/v4/projects/${GITLAB_USER}%2Fsupermarianandrobin/packages/generic/supermarianandrobin/${SMR_VERSION}/supermarianandrobin.sfc",
        "filepath": "/binaries/supermarianandrobin.sfc",
        "link_type": "package"
      }
    ]
  }
}

