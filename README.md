MOVED TO https://codeberg.org/devnewton/supermarianandrobin

# Super Marian and Robin

SNES implementation of [Marian and Robin](https://opengameart.org/content/marian-and-robin) game concept using [pvsneslib SDK](https://github.com/alekmaul/pvsneslib).

## How to build & run ?

### Install required libraries and software

On Debian Stable you can do it with:

    apt install make gcc libpng-dev libjson-c-dev

### Build

1. Init git submodules recursively ;
2. Follow instruction in pvsneslib subfolder to build this SDK ;
3. Run make in project top folder ;

### Run

Launch game/build/supermarianandrobin.sfc in your favorite SNES emulator or copy it in a ROM cart to play on a real console.