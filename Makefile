.PHONY: preparepng4gfx2snes game clean

all: preparepng4gfx2snes prepareromdata game

preparepng4gfx2snes:
	make -C tools/preparepng4gfx2snes

prepareromdata:
	make -C tools/prepareromdata

game:
	make -C game all

clean:
	make -C tools/prepareromdata clean
	make -C tools/preparepng4gfx2snes clean
	make -C game clean