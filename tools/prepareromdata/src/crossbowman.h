#ifndef PREPAREROMDATA_CROSSBOWMEN_H
#define PREPAREROMDATA_CROSSBOWMEN_H

#include "tmj.h"

extern void prepare_crossbowman_data(tmj_objects *objects, char *output_filename);

#endif