#ifndef PREPAREROMDATA_GUARDS_H
#define PREPAREROMDATA_GUARDS_H

#include "tmj.h"

extern void prepare_guard_data(tmj_objects *objects, char *output_filename);

#endif