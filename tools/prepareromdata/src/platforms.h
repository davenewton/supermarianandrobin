#ifndef PREPAREROMDATA_PLATFORMS_H
#define PREPAREROMDATA_PLATFORMS_H

#include "tmj.h"

extern void prepare_platforms_data(tmj_objects *objects, char *output_filename);

#endif