#include "guard.h"
#include "../../../game/src/smr_guard_rom_data.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

smr_guard_rom_data *convert_guards(tmj_objects *objects) {
  smr_guard_rom_data *guard =
      malloc(sizeof(smr_guard_rom_data) + sizeof(smr_guard_position_rom_data[0]));
  guard->nb_positions = 0;
  guard->nb_life = 0;
  for (int i = 0; i < objects->nb_objects; ++i) {
    tmj_object *object = &objects->objects[i];
    if (strcmp("guard", object->type) == 0) {
      ++guard->nb_positions;
      guard =
          realloc(guard,
                  sizeof(smr_guard_rom_data) +
                      sizeof(smr_guard_position_rom_data[guard->nb_positions]));
      smr_guard_position_rom_data *position =
          &guard->positions[guard->nb_positions - 1];
      position->x = object->x;
      position->y = object->y - object->h;

      for (int p = 0; p < object->nb_properties; ++p) {
        if (strcmp("nb_life", object->properties[p].name) == 0) {
          guard->nb_life += atoi(object->properties[p].value);
        }
      }
    }
  }
  return guard;
}

void prepare_guard_data(tmj_objects *objects, char *output_filename) {
  FILE *fp = fopen(output_filename, "wb");
  if (!fp) {
    printf("Cannot open %s\n", output_filename);
    exit(EXIT_FAILURE);
    return;
  }
  smr_guard_rom_data *guards = convert_guards(objects);
  fwrite(guards,
         sizeof(smr_guard_rom_data) +
             sizeof(smr_guard_position_rom_data[guards->nb_positions]),
         1, fp);
  fclose(fp);
  free(guards);
}