#ifndef PREPAREROMDATA_BOARS_H
#define PREPAREROMDATA_BOARS_H

#include "tmj.h"

extern void prepare_boars_data(tmj_objects *objects, char *output_filename);

#endif