#include "crossbowman.h"
#include "../../../game/src/smr_crossbowman_rom_data.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

smr_crossbowman_rom_data *convert_crossbowman(tmj_objects *objects) {
  smr_crossbowman_rom_data *crossbowman =
      malloc(sizeof(smr_crossbowman_rom_data) + sizeof(smr_crossbowman_position_rom_data[0]));
  crossbowman->nb_positions = 0;
  crossbowman->nb_life = 0;
  for (int i = 0; i < objects->nb_objects; ++i) {
    tmj_object *object = &objects->objects[i];
    if (strcmp("crossbowman", object->type) == 0) {
      ++crossbowman->nb_positions;
      crossbowman =
          realloc(crossbowman,
                  sizeof(smr_crossbowman_rom_data) +
                      sizeof(smr_crossbowman_position_rom_data[crossbowman->nb_positions]));
      smr_crossbowman_position_rom_data *position =
          &crossbowman->positions[crossbowman->nb_positions - 1];
      position->x = object->x;
      position->y = object->y - object->h;
      position->is_looking_left = object->flipped_horizontal;
      
      for(int p=0; p<object->nb_properties; ++p) {
        if(strcmp("nb_life", object->properties[p].name) == 0) {
          crossbowman->nb_life += atoi(object->properties[p].value);
        }
      }
    }
  }
  return crossbowman;
}

void prepare_crossbowman_data(tmj_objects *objects, char *output_filename) {
  FILE *fp = fopen(output_filename, "wb");
  if (!fp) {
    printf("Cannot open %s\n", output_filename);
    exit(EXIT_FAILURE);
    return;
  }
  smr_crossbowman_rom_data *crossbowman = convert_crossbowman(objects);
  fwrite(crossbowman,
         sizeof(smr_crossbowman_rom_data) +
             sizeof(smr_crossbowman_position_rom_data[crossbowman->nb_positions]),
         1, fp);
  fclose(fp);
  free(crossbowman);
}