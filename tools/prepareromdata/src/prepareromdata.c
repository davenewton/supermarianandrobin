#include "castle.h"
#include "forest.h"
#include "lookup.h"
#include "title.h"
#include "dialog.h"
#include "tournament.h"
#include "treasure_room.h"

#include <getopt.h>

#include <math.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct option long_options[] = {{"data", required_argument, 0, 'd'},
                                {"input", required_argument, 0, 'i'},
                                {"output", required_argument, 0, 'o'},
                                {0, 0, 0, 0}};

int main(int argc, char **argv) {
  char *data = 0;
  char *input = 0;
  char *output = 0;
  for (;;) {
    int option_index = 0;
    int c = getopt_long(argc, argv, "d:i:o:", long_options, &option_index);
    if (c == -1) {
      break;
    }
    switch (c) {
    case 'd':
      data = optarg;
      break;
    case 'i':
      input = optarg;
      break;
    case 'o':
      output = optarg;
      break;
    }
  }

  if (!data || !output) {
    printf("Usage : prepareromdata -d lookup_tables -o output_dir\n");
    return EXIT_FAILURE;
  }

  if (strcmp("lookup_tables", data) == 0) {
    prepare_lookup_tables(output);
    return EXIT_SUCCESS;
  }

  if (!input) {
    printf("Usage : prepareromdata -d forest -i input_tmj_map_file -o output_dir\n");
    return EXIT_FAILURE;
  }

  if (strcmp("title", data) == 0) {
    prepare_title_data(input, output);
    return EXIT_SUCCESS;
  }

  if (strcmp("dialog", data) == 0) {
    prepare_dialog_data(input, output);
    return EXIT_SUCCESS;
  }

  if (strcmp("forest", data) == 0) {
    prepare_forest_data(input, output);
    return EXIT_SUCCESS;
  }
  if (strcmp("tournament", data) == 0) {
    prepare_tournament_data(input, output);
    return EXIT_SUCCESS;
  }
  if (strcmp("castle", data) == 0) {
    prepare_castle_data(input, output);
    return EXIT_SUCCESS;
  }
    if (strcmp("treasure_room", data) == 0) {
    prepare_treasure_room_data(input, output);
    return EXIT_SUCCESS;
  }

  return EXIT_SUCCESS;
}
