#include "tmj.h"
#include "path.h"
#include <json-c/json.h>
#include <json-c/json_object.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

// Bits on the far end of the 32-bit global tile ID are used for tile flags
#define TMJ_FLIPPED_HORIZONTALLY_FLAG 0x80000000
#define TMJ_FLIPPED_VERTICALLY_FLAG 0x40000000
#define TMJ_FLIPPED_DIAGONALLY_FLAG 0x20000000

typedef struct {
  json_object *tile;
  uint32_t flags;
} tile_with_flags;

tile_with_flags find_object_tile(json_object *map, json_object *object) {

  tile_with_flags result = {.tile = 0, .flags = 0};

  json_object *gid = 0;
  if (!json_object_object_get_ex(object, "gid", &gid)) {
    return result;
  }
  result.flags = (uint32_t)json_object_get_int64(gid);
  uint32_t global_tile_id = result.flags;

  // Clear the flags
  global_tile_id &=
      ~(TMJ_FLIPPED_HORIZONTALLY_FLAG | TMJ_FLIPPED_VERTICALLY_FLAG |
        TMJ_FLIPPED_DIAGONALLY_FLAG);

  json_object *tilesets = 0;
  if (!json_object_object_get_ex(map, "tilesets", &tilesets)) {
    return result;
  }
  int nb_tilesets = json_object_array_length(tilesets);
  for (int ts = 0; ts < nb_tilesets; ++ts) {
    json_object *tileset = json_object_array_get_idx(tilesets, ts);
    json_object *firstgid = 0;
    if (!json_object_object_get_ex(tileset, "firstgid", &firstgid)) {
      continue;
    }
    int tileset_firstgid = json_object_get_int(firstgid);
    if (tileset_firstgid > global_tile_id) {
      continue;
    }
    int local_tile_id = global_tile_id - tileset_firstgid;
    json_object *tiles = 0;
    if (!json_object_object_get_ex(tileset, "tiles", &tiles)) {
      continue;
    }
    int nb_tiles = json_object_array_length(tiles);
    for (int t = 0; t < nb_tiles; ++t) {
      json_object *tile = json_object_array_get_idx(tiles, t);
      json_object *id = 0;
      if (!json_object_object_get_ex(tile, "id", &id)) {
        continue;
      }
      int tileid = json_object_get_int(id);
      if (local_tile_id == tileid) {
        result.tile = tile;
        return result;
      }
    }
  }
  return result;
}

void fill_tmj_object(tmj_object *tmjo, json_object *object, tile_with_flags tile) {
  json_object *type = 0;
  tmjo->type[0] = '\0';
  if (json_object_object_get_ex(object, "type", &type)) {
    snprintf(tmjo->type, TMJ_MAX_STRING_SIZE, "%s", json_object_get_string(type));
  }
  if (tile.tile) {
    if (strlen(tmjo->type) == 0 &&
        json_object_object_get_ex(tile.tile, "type", &type)) {
      snprintf(tmjo->type, TMJ_MAX_STRING_SIZE, "%s", json_object_get_string(type));
    }
    tmjo->flipped_horizontal = tile.flags & TMJ_FLIPPED_HORIZONTALLY_FLAG;
    tmjo->flipped_vertical = tile.flags & TMJ_FLIPPED_VERTICALLY_FLAG;
  } else {
    tmjo->flipped_horizontal = false;
    tmjo->flipped_vertical = false;
  }

  json_object *x = 0;
  if (json_object_object_get_ex(object, "x", &x)) {
    tmjo->x = json_object_get_int(x);
  } else {
    tmjo->x = 0;
  }

  json_object *y = 0;
  if (json_object_object_get_ex(object, "y", &y)) {
    tmjo->y = json_object_get_int(y);
  } else {
    tmjo->y = 0;
  }

  json_object *w = 0;
  if (json_object_object_get_ex(object, "width", &w)) {
    tmjo->w = json_object_get_int(w);
  } else {
    tmjo->w = 0;
  }

  json_object *h = 0;
  if (json_object_object_get_ex(object, "height", &h)) {
    tmjo->h = json_object_get_int(h);
  } else {
    tmjo->h = 0;
  }

  json_object *properties = 0;
  if (json_object_object_get_ex(object, "properties", &properties)) {
    tmjo->nb_properties = json_object_array_length(properties);
    for (int p = 0; p < tmjo->nb_properties; ++p) {
      json_object *property = json_object_array_get_idx(properties, p);

      json_object *property_name = 0;
      if (json_object_object_get_ex(property, "name", &property_name)) {
        snprintf(tmjo->properties[p].name, TMJ_MAX_STRING_SIZE, "%s", json_object_get_string(property_name));
      } else {
        tmjo->properties[p].name[0] = '\0';
      }

      json_object *property_type = 0;
      if (json_object_object_get_ex(property, "type", &property_type)) {
        snprintf(tmjo->properties[p].type, TMJ_MAX_STRING_SIZE, "%s", json_object_get_string(property_type));
      } else {
        tmjo->properties[p].type[0] = '\0';
      }

      json_object *property_value = 0;
      if (json_object_object_get_ex(property, "value", &property_value)) {
        snprintf(tmjo->properties[p].value, TMJ_MAX_STRING_SIZE, "%s", json_object_get_string(property_value));
      } else {
        tmjo->properties[p].value[0] = '\0';
      }
    }
  } else {
    tmjo->nb_properties = 0;
  }
}

tmj_objects *load_tmj_objects(char *map_filename) {
  json_object *map = json_object_from_file(map_filename);
  if (!map) {
    printf("Cannot open %s", map_filename);
    json_object_put(map);
    return NULL;
  }
  json_object *layers = 0;
  if (!json_object_object_get_ex(map, "layers", &layers)) {
    printf("No layers found");
    json_object_put(map);
    return NULL;
  }
  int nb_layers = json_object_array_length(layers);
  tmj_objects *results = malloc(sizeof(tmj_objects) + sizeof(tmj_object[0]));
  results->nb_objects = 0;
  int currentObject = 0;
  for (int l = 0; l < nb_layers; ++l) {
    json_object *layer = json_object_array_get_idx(layers, l);
    json_object *objects = 0;
    if (!json_object_object_get_ex(layer, "objects", &objects)) {
      continue;
    }
    int nb_objects = json_object_array_length(objects);
    results->nb_objects += nb_objects;
    results = realloc(results, sizeof(tmj_objects) +
                                   sizeof(tmj_object[results->nb_objects]));
    for (int o = 0; o < nb_objects; ++o) {
      json_object *object = json_object_array_get_idx(objects, o);
      tile_with_flags tile = find_object_tile(map, object);
      fill_tmj_object(&results->objects[currentObject], object, tile);
      ++currentObject;
    }
  }
  json_object_put(map);
  return results;
}

void convert_tmj_tile_layers_to_snes_tile_maps(char *map_filename, char *output_dir) {
  json_object *map = json_object_from_file(map_filename);
  if (!map) {
    printf("Cannot open %s", map_filename);
    json_object_put(map);
    return;
  }
  json_object *layers = 0;
  if (!json_object_object_get_ex(map, "layers", &layers)) {
    printf("No layers found");
    json_object_put(map);
    return;
  }
  int nb_layers = json_object_array_length(layers);
  for (int l = 0; l < nb_layers; ++l) {
    json_object *layer = json_object_array_get_idx(layers, l);

    const char *type = NULL;
    json_object *layer_type = 0;
    if (json_object_object_get_ex(layer, "type", &layer_type)) {
      type = json_object_get_string(layer_type);
    }
    if (NULL == type) {
      type = "";
    }
    if (strcmp("tilelayer", type) != 0) {
      continue;
    }

    const char *name = NULL;
    json_object *layer_name;
    if (json_object_object_get_ex(layer, "name", &layer_name)) {
      name = json_object_get_string(layer_name);
    }
    if (NULL == name) {
      printf("Cannot get layer name");
      return;
    }

    json_object *layer_data = 0;
    if (!json_object_object_get_ex(layer, "data", &layer_data)) {
      printf("Cannot get data of layer %s", name);
      continue;
    }

    uint16_t priority = 0;
    json_object *layer_properties = 0;
    if (json_object_object_get_ex(layer, "properties", &layer_properties)) {
      int nb_layer_properties = json_object_array_length(layer_properties);
      for (int p = 0; p < nb_layer_properties; ++p) {
        json_object *layer_property = json_object_array_get_idx(layer_properties, p);

        json_object *layer_property_name = 0;
        if (json_object_object_get_ex(layer_property, "name", &layer_property_name)) {
          if (strcmp("priority", json_object_get_string(layer_property_name)) == 0) {
            json_object *layer_property_value = 0;
            if (json_object_object_get_ex(layer_property, "value", &layer_property_value)) {
              if (json_object_get_boolean(layer_property_value)) {
                priority = 8192; // 0b0010000000000000;
              }
            }
          }
        }
      }
    }

    int data_len = json_object_array_length(layer_data);
    uint16_t snes_tilemap[data_len];

    for (int i = 0; i < data_len; ++i) {
      uint32_t tile_id = (uint32_t)json_object_get_int64(json_object_array_get_idx(layer_data, i));

      uint32_t global_tile_id = tile_id & ~(TMJ_FLIPPED_HORIZONTALLY_FLAG | TMJ_FLIPPED_VERTICALLY_FLAG | TMJ_FLIPPED_DIAGONALLY_FLAG);

      if (global_tile_id) {
        snes_tilemap[i] = global_tile_id - 1;
        if (tile_id & TMJ_FLIPPED_HORIZONTALLY_FLAG) {
          snes_tilemap[i] |= 0b0100000000000000;
        }
        if (tile_id & TMJ_FLIPPED_VERTICALLY_FLAG) {
          snes_tilemap[i] |= 0b1000000000000000;
        }
        snes_tilemap[i] |= priority;
      } else {
        snes_tilemap[i] = 0;
      }
    }

    char *snes_tilemap_filename = compute_path(output_dir, name);
    FILE *fp = fopen(snes_tilemap_filename, "wb");
    if (!fp) {
      printf("Cannot open %s\n", snes_tilemap_filename);
      return exit(EXIT_FAILURE);
    }
    fwrite(snes_tilemap, sizeof(snes_tilemap), 1, fp);
    fclose(fp);
    free(snes_tilemap_filename);
  }
  json_object_put(map);
}