#ifndef PREPAREROMDATA_FOREST_H
#define PREPAREROMDATA_FOREST_H

extern void prepare_forest_data(char *map_filename, char *output_dir);

#endif