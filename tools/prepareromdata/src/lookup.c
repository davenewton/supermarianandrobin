#include "lookup.h"
#include "../../../game/src/smr_lookup_tables_rom_data.h"
#include "path.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>


void prepare_circular_lookup_table(char *output_filename) {
  smr_lookup_path_point circular_path[SMR_LOOKUP_CIRCULAR_PATH_NB_POINTS];
  for (int p = 0; p < SMR_LOOKUP_CIRCULAR_PATH_NB_POINTS; ++p) {
    double r = p * 2 * M_PI / SMR_LOOKUP_CIRCULAR_PATH_NB_POINTS;
    circular_path[p].x = SMR_LOOKUP_CIRCULAR_PATH_RADIUS * cos(r);
    circular_path[p].y = SMR_LOOKUP_CIRCULAR_PATH_RADIUS * sin(r);
  }
  FILE *fp = fopen(output_filename, "wb");
  if (!fp) {
    printf("Cannot open %s\n", output_filename);
    return exit(EXIT_FAILURE);
  }
  fwrite(&circular_path, sizeof(circular_path), 1, fp);
  fclose(fp);
}


void prepare_lookup_tables(char *output_dir) {
  char *output_circular_lookup_table_path = compute_path(output_dir, "circular_lookup_table");
  prepare_circular_lookup_table(output_circular_lookup_table_path);
  free(output_circular_lookup_table_path);
}