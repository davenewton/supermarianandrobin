#include "flails.h"
#include "../../../game/src/smr_flails_rom_data.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

smr_flails_rom_data *convert_flails(tmj_objects *objects) {
  smr_flails_rom_data *flails =
      malloc(sizeof(smr_flails_rom_data) + sizeof(smr_flail_rom_data[0]));
  flails->nb_flails = 0;
  for (int i = 0; i < objects->nb_objects; ++i) {
    tmj_object *object = &objects->objects[i];
    if (strcmp("flail", object->type) == 0) {
      ++flails->nb_flails;
      flails =
          realloc(flails,
                  sizeof(smr_flails_rom_data) +
                      sizeof(smr_flail_rom_data[flails->nb_flails]));
      smr_flail_rom_data *flail =
          &flails->flails[flails->nb_flails - 1];
      flail->anchor_x = object->x;
      flail->anchor_y = object->y;
    }
  }
  return flails;
}

void prepare_flails_data(tmj_objects *objects, char *output_filename) {
  FILE *fp = fopen(output_filename, "wb");
  if (!fp) {
    printf("Cannot open %s\n", output_filename);
    exit(EXIT_FAILURE);
    return;
  }
  smr_flails_rom_data *flails = convert_flails(objects);
  fwrite(flails,
         sizeof(smr_flails_rom_data) +
             sizeof(smr_flail_rom_data[flails->nb_flails]),
         1, fp);
  fclose(fp);
  free(flails);
}
