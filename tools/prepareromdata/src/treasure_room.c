#include "treasure_room.h"
#include "../../../game/src/smr_treasure_room_rom_data.h"
#include "path.h"
#include "platforms.h"
#include "tmj.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// TODO factorize with prepare_treasure_room_header and prepare_treasure_room_header?
void prepare_treasure_room_level_header(tmj_objects *objects, char *output_filename) {
  smr_treasure_room_rom_data_header data;
  for (int i = 0; i < objects->nb_objects; ++i) {
    tmj_object *object = &objects->objects[i];
    printf("Object %s at %d,%d\n", object->type, object->x, object->y);
    if (strcmp("marian", object->type) == 0) {
      data.marianX = object->x;
      data.marianY = object->y - object->h;
    } else if (strcmp("robin", object->type) == 0) {
      data.robinX = object->x;
      data.robinY = object->y - object->h;
    } else if (strcmp("king", object->type) == 0) {
      data.kingX = object->x;
      data.kingY = object->y - object->h;
    } else if (strcmp("chest", object->type) == 0) {
      data.chestX = object->x;
      data.chestY = object->y - object->h;
    }
  }
  FILE *fp = fopen(output_filename, "wb");
  if (!fp) {
    printf("Cannot open %s\n", output_filename);
    return exit(EXIT_FAILURE);
  }
  fwrite(&data, sizeof(data), 1, fp);
  fclose(fp);
}

void prepare_treasure_room_data(char *map_filename, char *output_dir) {
  tmj_objects *objects = load_tmj_objects(map_filename);
  if (!objects) {
    exit(EXIT_FAILURE);
    return;
  }

  {
    char *output_header_path = compute_filename_by_replacing_extension(map_filename, output_dir, "_header_data");
    prepare_treasure_room_level_header(objects, output_header_path);
    free(output_header_path);
  }

  {
    char *output_platforms_path = compute_filename_by_replacing_extension(map_filename, output_dir, "_platforms_data");
    prepare_platforms_data(objects, output_platforms_path);
    free(output_platforms_path);
  }

  convert_tmj_tile_layers_to_snes_tile_maps(map_filename, output_dir);

  free(objects);
}
