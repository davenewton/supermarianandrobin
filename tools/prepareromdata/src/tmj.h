#ifndef PREPAREROMDATA_TMJ_H
#define PREPAREROMDATA_TMJ_H

#include <stdbool.h>

#define TMJ_MAX_STRING_SIZE (256)
#define TMJ_MAX_PROPERTIES_PER_OBJECT (32)

typedef struct {
  char name[TMJ_MAX_STRING_SIZE];
  char type[TMJ_MAX_STRING_SIZE];
  char value[TMJ_MAX_STRING_SIZE];
} tmj_property;

typedef struct {
  char type[TMJ_MAX_STRING_SIZE];
  int x, y, w, h;
  int nb_properties;
  bool flipped_horizontal;
  bool flipped_vertical;
  tmj_property properties[TMJ_MAX_PROPERTIES_PER_OBJECT];
} tmj_object;

typedef struct {
  int nb_objects;
  tmj_object objects[];
} tmj_objects;

extern tmj_objects *load_tmj_objects(char *map_filename);
extern void convert_tmj_tile_layers_to_snes_tile_maps(char *map_filename, char* output_dir);

#endif