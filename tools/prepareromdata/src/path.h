#ifndef PREPAREROMDATA_PATH_H
#define PREPAREROMDATA_PATH_H

extern char *compute_path(const char *dir, const char *filename);
extern char *compute_filename_by_replacing_extension(char *filename, char *dir, char *suffix);

#endif