#ifndef PREPAREROMDATA_FLAILS_H
#define PREPAREROMDATA_FLAILS_H

#include "tmj.h"

extern void prepare_flails_data(tmj_objects *objects, char *output_filename);

#endif