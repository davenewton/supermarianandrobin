#include "forest.h"
#include "../../../game/src/smr_forest_rom_data.h"
#include "path.h"
#include "platforms.h"
#include "tmj.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void prepare_forest_header(tmj_objects *objects, char *output_filename) {
  smr_forest_rom_data_header data;
  for (int i = 0; i < objects->nb_objects; ++i) {
    tmj_object *object = &objects->objects[i];
    printf("Object %s at %d,%d\n", object->type, object->x, object->y);
    if (strcmp("marian", object->type) == 0) {
      data.marianX = object->x;
      data.marianY = object->y - object->h;
    } else if (strcmp("robin", object->type) == 0) {
      data.robinX = object->x;
      data.robinY = object->y - object->h;
    }
  }
  FILE *fp = fopen(output_filename, "wb");
  if (!fp) {
    printf("Cannot open %s\n", output_filename);
    return exit(EXIT_FAILURE);
  }
  fwrite(&data, sizeof(data), 1, fp);
  fclose(fp);
}

void prepare_forest_data(char *map_filename, char *output_dir) {
  tmj_objects *objects = load_tmj_objects(map_filename);
  if (!objects) {
    exit(EXIT_FAILURE);
    return;
  }

  {
    char *output_header_path = compute_path(output_dir, "forest_header_data");
    prepare_forest_header(objects, output_header_path);
    free(output_header_path);
  }

  {
    char *output_platforms_path = compute_path(output_dir, "forest_platforms_data");
    prepare_platforms_data(objects, output_platforms_path);
    free(output_platforms_path);
  }

  free(objects);

  convert_tmj_tile_layers_to_snes_tile_maps(map_filename, output_dir);
}
