#include "title.h"
#include "tmj.h"

void prepare_dialog_data(char *map_filename, char *output_dir) {
  convert_tmj_tile_layers_to_snes_tile_maps(map_filename, output_dir);
}
