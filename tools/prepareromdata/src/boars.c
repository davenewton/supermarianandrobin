#include "boars.h"
#include "../../../game/src/smr_boars_rom_data.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

smr_boars_rom_data *convert_boars(tmj_objects *objects) {
  smr_boars_rom_data *boars =
      malloc(sizeof(smr_boars_rom_data) + sizeof(smr_boar_rom_data[0]));
  boars->nb_boars = 0;
  for (int i = 0; i < objects->nb_objects; ++i) {
    tmj_object *object = &objects->objects[i];
    if (strcmp("boar", object->type) == 0) {
      ++boars->nb_boars;
      boars =
          realloc(boars,
                  sizeof(smr_boars_rom_data) +
                      sizeof(smr_boar_rom_data[boars->nb_boars]));
      smr_boar_rom_data *boar =
          &boars->boars[boars->nb_boars - 1];
      boar->x = object->x;
      boar->y = object->y;
      boar->ticks_before_first_spit = 0;
      boar->ticks_between_spits = 4 * 60;
      for (int p = 0; p < object->nb_properties; ++p) {
        if (strcmp("ticks_before_first_spit", object->properties[p].name) == 0) {
          boar->ticks_before_first_spit = atoi(object->properties[p].value);
        } else if (strcmp("ticks_between_spits", object->properties[p].name) == 0) {
          boar->ticks_between_spits = atoi(object->properties[p].value);
        }
      }     
    }
  }
  return boars;
}

void prepare_boars_data(tmj_objects *objects, char *output_filename) {
  FILE *fp = fopen(output_filename, "wb");
  if (!fp) {
    printf("Cannot open %s\n", output_filename);
    exit(EXIT_FAILURE);
    return;
  }
  smr_boars_rom_data *boars = convert_boars(objects);
  fwrite(boars,
         sizeof(smr_boars_rom_data) +
             sizeof(smr_boar_rom_data[boars->nb_boars]),
         1, fp);
  fclose(fp);
  free(boars);
}