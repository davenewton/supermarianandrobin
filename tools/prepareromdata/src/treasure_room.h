#ifndef PREPAREROMDATA_TREASURE_ROOM_H
#define PREPAREROMDATA_TREASURE_ROOM_H

extern void prepare_treasure_room_data(char *map_filename, char *output_dir);

#endif