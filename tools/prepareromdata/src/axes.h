#ifndef PREPAREROMDATA_AXES_H
#define PREPAREROMDATA_AXES_H

#include "tmj.h"

extern void prepare_axes_data(tmj_objects *objects, char *output_filename);

#endif