#include "platforms.h"
#include "../../../game/src/smr_platform_rom_data.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


smr_platforms_rom_data *convert_platforms(tmj_objects *objects) {
  smr_platforms_rom_data *platforms =
      malloc(sizeof(smr_platforms_rom_data) + sizeof(smr_platform_rom_data[0]));
  platforms->nb_platforms = 0;
  for (int i = 0; i < objects->nb_objects; ++i) {
    tmj_object *object = &objects->objects[i];
    if (strcmp("platform", object->type) == 0) {
      ++platforms->nb_platforms;
      platforms =
          realloc(platforms,
                  sizeof(smr_platforms_rom_data) +
                      sizeof(smr_platform_rom_data[platforms->nb_platforms]));
      smr_platform_rom_data *platform =
          &platforms->platforms[platforms->nb_platforms - 1];
      platform->x = object->x;
      platform->y = object->y;
      platform->w = object->w;
      platform->h = object->h;
      platform->flags = 0;
      for (int p = 0; p < object->nb_properties; ++p) {
        if (strcmp("lethal", object->properties[p].name) == 0 && strcmp("true", object->properties[p].value) == 0) {
          platform->flags |= SMR_PLATFORM_FLAG_LETHAL;
        }
      }
    }
  }
  return platforms;
}

void prepare_platforms_data(tmj_objects *objects, char *output_filename) {
  FILE *fp = fopen(output_filename, "wb");
  if (!fp) {
    printf("Cannot open %s\n", output_filename);
    exit(EXIT_FAILURE);
    return;
  }
  smr_platforms_rom_data *platforms = convert_platforms(objects);
  fwrite(platforms,
         sizeof(smr_platforms_rom_data) +
             sizeof(smr_platform_rom_data[platforms->nb_platforms]),
         1, fp);
  fclose(fp);
  free(platforms);
}