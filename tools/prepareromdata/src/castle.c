#include "castle.h"
#include "../../../game/src/smr_castle_rom_data.h"
#include "axes.h"
#include "boars.h"
#include "crossbowman.h"
#include "flails.h"
#include "guard.h"
#include "path.h"
#include "platforms.h"
#include "tmj.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// TODO factorize with prepare_castle_header and prepare_castle_header?
void prepare_castle_level_header(tmj_objects *objects, char *output_filename) {
  smr_castle_level_rom_data_header data;
  for (int i = 0; i < objects->nb_objects; ++i) {
    tmj_object *object = &objects->objects[i];
    printf("Object %s at %d,%d\n", object->type, object->x, object->y);
    if (strcmp("marian", object->type) == 0) {
      data.marianX = object->x;
      data.marianY = object->y - object->h;
    } else if (strcmp("robin", object->type) == 0) {
      data.robinX = object->x;
      data.robinY = object->y - object->h;
    } else if (strcmp("door", object->type) == 0) {
      data.doorX = object->x;
      data.doorY = object->y - object->h;
    } else if (strcmp("key", object->type) == 0) {
      data.keyX = object->x;
      data.keyY = object->y - object->h;
    }
  }
  FILE *fp = fopen(output_filename, "wb");
  if (!fp) {
    printf("Cannot open %s\n", output_filename);
    return exit(EXIT_FAILURE);
  }
  fwrite(&data, sizeof(data), 1, fp);
  fclose(fp);
}

void prepare_castle_data(char *map_filename, char *output_dir) {
  tmj_objects *objects = load_tmj_objects(map_filename);
  if (!objects) {
    exit(EXIT_FAILURE);
    return;
  }

  {
    char *output_header_path = compute_filename_by_replacing_extension(map_filename, output_dir, "_header_data");
    prepare_castle_level_header(objects, output_header_path);
    free(output_header_path);
  }

  {
    char *output_platforms_path = compute_filename_by_replacing_extension(map_filename, output_dir, "_platforms_data");
    prepare_platforms_data(objects, output_platforms_path);
    free(output_platforms_path);
  }

  {
    char *output_flails_path = compute_filename_by_replacing_extension(map_filename, output_dir, "_flails_data");
    prepare_flails_data(objects, output_flails_path);
    free(output_flails_path);
  }

  {
    char *output_boars_path = compute_filename_by_replacing_extension(map_filename, output_dir, "_boars_data");
    prepare_boars_data(objects, output_boars_path);
    free(output_boars_path);
  }

  {
    char *output_axes_path = compute_filename_by_replacing_extension(map_filename, output_dir, "_axes_data");
    prepare_axes_data(objects, output_axes_path);
    free(output_axes_path);
  }

    {
    char *output_crossbowman_path = compute_filename_by_replacing_extension(map_filename, output_dir, "_crossbowman_data");
    prepare_crossbowman_data(objects, output_crossbowman_path);
    free(output_crossbowman_path);
  }

      {
    char *output_guards_path = compute_filename_by_replacing_extension(map_filename, output_dir, "_guard_data");
    prepare_guard_data(objects, output_guards_path);
    free(output_guards_path);
  }

  convert_tmj_tile_layers_to_snes_tile_maps(map_filename, output_dir);

  free(objects);
}
