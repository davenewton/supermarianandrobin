#include "axes.h"
#include "../../../game/src/smr_axes_rom_data.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

smr_axes_rom_data *convert_axes(tmj_objects *objects) {
  smr_axes_rom_data *axes =
      malloc(sizeof(smr_axes_rom_data) + sizeof(smr_axe_rom_data[0]));
  axes->nb_axes = 0;
  for (int i = 0; i < objects->nb_objects; ++i) {
    tmj_object *object = &objects->objects[i];
    if (strcmp("axe", object->type) == 0) {
      ++axes->nb_axes;
      axes =
          realloc(axes,
                  sizeof(smr_axes_rom_data) +
                      sizeof(smr_axe_rom_data[axes->nb_axes]));
      smr_axe_rom_data *axe =
          &axes->axes[axes->nb_axes - 1];
      axe->anchor_x = object->x;
      axe->anchor_y = object->y;
      axe->ticks_before_first_move = 0;
      axe->ticks_between_moves = 60;
      axe->is_looking_left = 0;
      for (int p = 0; p < object->nb_properties; ++p) {
        if (strcmp("ticks_before_first_move", object->properties[p].name) == 0) {
          axe->ticks_before_first_move = atoi(object->properties[p].value);
        } else if (strcmp("ticks_between_moves", object->properties[p].name) == 0) {
          axe->ticks_between_moves = atoi(object->properties[p].value);
        } else if (strcmp("is_looking_left", object->properties[p].name) == 0) {
          axe->is_looking_left = strcmp("true", object->properties[p].value) == 0 ? 1 : 0;
        }
      }     
    }
  }
  return axes;
}

void prepare_axes_data(tmj_objects *objects, char *output_filename) {
  FILE *fp = fopen(output_filename, "wb");
  if (!fp) {
    printf("Cannot open %s\n", output_filename);
    exit(EXIT_FAILURE);
    return;
  }
  smr_axes_rom_data *axes = convert_axes(objects);
  fwrite(axes,
         sizeof(smr_axes_rom_data) +
             sizeof(smr_axe_rom_data[axes->nb_axes]),
         1, fp);
  fclose(fp);
  free(axes);
}