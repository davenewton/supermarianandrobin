#include "path.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *compute_path(const char *dir, const char *filename) {
  const char *format = "%s/%s";
  size_t size = sizeof(char) * (1 + snprintf(NULL, 0, format, dir, filename));
  char *path = malloc(size);
  snprintf(path, size, format, dir, filename);
  return path;
}

char *compute_filename_by_replacing_extension(char *filename, char *dir, char *suffix) {
  char *basename = strrchr(filename, '/');
  if (NULL == basename) {
    basename = filename;
  } else {
    ++basename;
  }
  char *extension = strrchr(filename, '.');
  size_t basename_without_extension_length = NULL == extension ? strlen(basename) : extension - basename;
  size_t suffix_length = strlen(suffix);
  size_t dir_length = strlen(dir);
  size_t path_length = dir_length + 1 /*separator*/ + basename_without_extension_length + suffix_length;
  char *result = malloc(sizeof(char) * (path_length + 1 /* null terminating char*/));
  strncpy(result, dir, dir_length);
  strncpy(result + dir_length, "/", 1);
  strncpy(result + dir_length + 1, basename, basename_without_extension_length);
  strncpy(result + dir_length + 1 + basename_without_extension_length, suffix, suffix_length);
  result[path_length] = '\0';
  return result;
}