#ifndef PREPAREROMDATA_DIALOG_H
#define PREPAREROMDATA_DIALOG_H

extern void prepare_dialog_data(char *map_filename, char *output_dir);

#endif