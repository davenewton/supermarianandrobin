
#include <getopt.h>
#include <png.h>
#include <pngconf.h>
#include <stdio.h>
#include <stdlib.h>

struct option long_options[] = {{"input", required_argument, 0, 'i'},
                                {"output", required_argument, 0, 'o'},
                                {0, 0, 0, 0}};

const int marian_and_robin_palette_usefull_size = 16;
const int marian_and_robin_palette_total_size = 256;
const png_color marian_and_robin_palette[] = {
    {.red = 128, .green = 0, .blue = 255},
    {.red = 244, .green = 240, .blue = 244},
    {.red = 34, .green = 36, .blue = 40},
    {.red = 92, .green = 89, .blue = 111},
    {.red = 165, .green = 154, .blue = 182},
    {.red = 112, .green = 63, .blue = 48},
    {.red = 213, .green = 85, .blue = 95},
    {.red = 255, .green = 149, .blue = 207},
    {.red = 215, .green = 119, .blue = 45},
    {.red = 243, .green = 199, .blue = 117},
    {.red = 255, .green = 216, .blue = 195},
    {.red = 24, .green = 100, .blue = 68},
    {.red = 124, .green = 162, .blue = 48},
    {.red = 43, .green = 81, .blue = 167},
    {.red = 22, .green = 154, .blue = 200},
    {.red = 129, .green = 213, .blue = 215},

    // unused
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255},
    {.red = 128, .green = 0, .blue = 255}

};

void convert_image(const char *imageFilePath,
                   const char *convertedImageFilePath) {
  FILE *fp = fopen(imageFilePath, "r");
  if (!fp) {
    printf("Cannot open %s\n", imageFilePath);
    return;
  }
  png_structp png =
      png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  png_infop info = png_create_info_struct(png);
  png_init_io(png, fp);
  png_read_info(png, info);

  const png_uint_32 width = png_get_image_width(png, info);
  const png_uint_32 height = png_get_image_height(png, info);
  const png_byte colorType = png_get_color_type(png, info);
  const png_byte bitDepth = png_get_bit_depth(png, info);

  if (bitDepth == 16) {
    png_set_strip_16(png);
  }
  if (colorType == PNG_COLOR_TYPE_PALETTE) {
    png_set_palette_to_rgb(png);
  }
  if (colorType == PNG_COLOR_TYPE_GRAY && bitDepth < 8) {
    png_set_expand_gray_1_2_4_to_8(png);
  }
  if (png_get_valid(png, info, PNG_INFO_tRNS)) {
    png_set_tRNS_to_alpha(png);
  }
  if (colorType == PNG_COLOR_TYPE_RGB || colorType == PNG_COLOR_TYPE_GRAY ||
      colorType == PNG_COLOR_TYPE_PALETTE) {
    png_set_filler(png, 255, PNG_FILLER_AFTER);
  }
  if (colorType == PNG_COLOR_TYPE_GRAY ||
      colorType == PNG_COLOR_TYPE_GRAY_ALPHA) {
    png_set_gray_to_rgb(png);
  }

  png_read_update_info(png, info);

  const size_t rowBytes = png_get_rowbytes(png, info);

  png_bytep pixels = malloc(height * rowBytes);
  png_bytep row_pointers[height];
  for (int y = 0; y < height; ++y) {
    row_pointers[y] = &pixels[y * rowBytes];
  }
  png_read_image(png, row_pointers);
  png_destroy_read_struct(&png, &info, NULL);
  fclose(fp);

  // convert all transparent colors
  for (int y = 0; y < height; ++y) {
    for (int x = 0; x < width; ++x) {
      png_byte alpha = pixels[y * rowBytes + x * 4 + 3];
      if (alpha < 255) {
        pixels[y * rowBytes + x * 4] = marian_and_robin_palette[0].red;
        pixels[y * rowBytes + x * 4 + 1] = marian_and_robin_palette[0].green;
        pixels[y * rowBytes + x * 4 + 2] = marian_and_robin_palette[0].blue;
        pixels[y * rowBytes + x * 4 + 3] = 255;
      }
    }
  }

  FILE *outfp = fopen(convertedImageFilePath, "w");
  if (!outfp) {
    printf("Cannot open %s for writing", convertedImageFilePath);
    return;
  }

  png_structp outpng =
      png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  png_infop outinfo = png_create_info_struct(outpng);
  png_init_io(outpng, outfp);
  png_set_IHDR(outpng, outinfo, width, height, 8, PNG_COLOR_TYPE_PALETTE,
               PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT,
               PNG_FILTER_TYPE_DEFAULT);
  png_set_PLTE(outpng, outinfo, marian_and_robin_palette,
               marian_and_robin_palette_total_size);
  png_write_info(outpng, outinfo);

  png_bytep outpixels = malloc(height * width);
  png_bytep out_row_pointers[height];
  for (int y = 0; y < height; ++y) {
    out_row_pointers[y] = &outpixels[y * width];
  }

  for (int y = 0; y < height; ++y) {
    for (int x = 0; x < width; ++x) {
      png_byte r = pixels[y * rowBytes + x * 4];
      png_byte g = pixels[y * rowBytes + x * 4 + 1];
      png_byte b = pixels[y * rowBytes + x * 4 + 2];
      int found = 0;
      for (png_byte p = 0; p < marian_and_robin_palette_usefull_size; ++p) {
        if (r == marian_and_robin_palette[p].red &&
            g == marian_and_robin_palette[p].green &&
            b == marian_and_robin_palette[p].blue) {
          outpixels[y * width + x] = p;
          found = 1;
          break;
        }
      }
      if (!found) {
        printf("Pixel outside palette found at %d,%d\n", x, y);
        outpixels[y * width + x] = 0;
      }
    }
  }
  png_write_image(outpng, out_row_pointers);
  png_write_end(outpng, NULL);
  png_destroy_write_struct(&outpng, &outinfo);

  fclose(outfp);
}

int main(int argc, char **argv) {

  const char* inputFile = 0;
  const char* outputFile = 0;
  for (;;) {
    int option_index = 0;
    int c = getopt_long(argc, argv, "i:o:", long_options, &option_index);
    if (c == -1) {
      break;
    }

    switch (c) {

    case 'i':
      inputFile = optarg;
      break;
    case 'o':
      outputFile = optarg;
      break;
    }
  }
  if(!inputFile || !outputFile) {
    	printf("Usage : preparepng4gfx2snes -i input.png -o output.png\n");
      return 0;
  }
  convert_image(inputFile, outputFile);
  //convert_image("data/menus/title/title_tileset.png", "title_tileset.png");
  return 0;
}