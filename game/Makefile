ifeq ($(strip $(PVSNESLIB_HOME)),)
export PVSNESLIB_HOME := $(realpath ../pvsneslib)
endif

.PHONY: build_directories assets sprites all

build_directories:
	mkdir -p build
	mkdir -p build/src
	mkdir -p build/assets/sounds
	mkdir -p build/assets/sprites/16
	mkdir -p build/assets/sprites/32
	mkdir -p build/assets/menus/title
	mkdir -p build/assets/menus/dialog
	mkdir -p build/assets/menus/font
	mkdir -p build/assets/levels/forest
	mkdir -p build/assets/levels/tournament
	mkdir -p build/assets/levels/castle
	mkdir -p build/assets/levels/treasure_room

# tools

export PREPAREPNG4GFX2SNES	:=	../tools/preparepng4gfx2snes/preparepng4gfx2snes
export PREPAREROMDATA    	:=	../tools/prepareromdata/build/prepareromdata

export CC	:=	$(PVSNESLIB_HOME)/devkitsnes/bin/816-tcc
export AS	:=	$(PVSNESLIB_HOME)/devkitsnes/bin/wla-65816
export LD	:=	$(PVSNESLIB_HOME)/devkitsnes/bin/wlalink

export GFX4SNES	:=	$(PVSNESLIB_HOME)/devkitsnes/tools/gfx4snes
export SMCONV	:=	$(PVSNESLIB_HOME)/devkitsnes/tools/smconv
export BRCONV	:=	$(PVSNESLIB_HOME)/devkitsnes/tools/snesbrr
export TXCONV	:=	$(PVSNESLIB_HOME)/devkitsnes/tools/bin2txt
export SNTOOLS	:=	$(PVSNESLIB_HOME)/devkitsnes/tools/snestools
export OPT		:=	$(PVSNESLIB_HOME)/devkitsnes/tools/816-opt
export CTF		:=	$(PVSNESLIB_HOME)/devkitsnes/tools/constify

#---------------------------------------------------------------------------------
# Add default flag for compiling
#---------------------------------------------------------------------------------
CFLAGS += -I$(PVSNESLIB_HOME)/pvsneslib/include -I$(PVSNESLIB_HOME)/devkitsnes/include -I$(CURDIR)

all: build_directories assets build/supermarianandrobin.sfc

clean:
	rm -Rf build

# assets

## font

build/assets/menus/font/font_tileset.png: assets/menus/font/font_tileset.png
	$(PREPAREPNG4GFX2SNES) -i $< -o $@

build/assets/menus/font/font_tileset.pic: build/assets/menus/font/font_tileset.png
	@echo convert map tileset ... $(notdir $@)
	$(GFX4SNES) --file-type=png --til-size=8 --pal-col-output=4 --pal-col-use=4 --map-noreduction --file-input=$<

## sprites 16x16

SPRITE16_SOURCE_PNGS := $(wildcard assets/sprites/16/*.png)
SPRITE16_BUILD_PNGS  := $(addprefix build/, $(SPRITE16_SOURCE_PNGS))
SPRITE16_PICS        := $(SPRITE16_BUILD_PNGS:.png=.pic)

build/assets/sprites/16/%.png: assets/sprites/16/%.png
	$(PREPAREPNG4GFX2SNES) -i $< -o $@

build/assets/sprites/16/%.pic: build/assets/sprites/16/%.png
	@echo convert sprite ... $(notdir $@)
	$(GFX4SNES) --til-size=16 --pal-col-output=16 --pal-col-use=16 --file-input $<

#explicit rules to keep intermediate png (usefull for debug)
$(SPRITE16_BUILD_PNGS):

## sprites 32x32

SPRITE32_SOURCE_PNGS := $(wildcard assets/sprites/32/*.png)
SPRITE32_BUILD_PNGS  := $(addprefix build/, $(SPRITE32_SOURCE_PNGS))
SPRITE32_PICS        := $(SPRITE32_BUILD_PNGS:.png=.pic)

build/assets/sprites/32/%.png: assets/sprites/32/%.png
	$(PREPAREPNG4GFX2SNES) -i $< -o $@

build/assets/sprites/32/%.pic: build/assets/sprites/32/%.png
	@echo convert sprite ... $(notdir $@)
	$(GFX4SNES) --til-size=32 --pal-col-output=16 --pal-col-use=16 --file-input $<

#explicit rules to keep intermediate png (usefull for debug)
$(SPRITE32_BUILD_PNGS):

## for all sprites

sprites: $(SPRITE16_PICS) $(SPRITE32_PICS)

## title

build/assets/menus/title/title_tileset.png: assets/menus/title/title_tileset.png
	$(PREPAREPNG4GFX2SNES) -i $< -o $@

build/assets/menus/title/title_tileset.pic: build/assets/menus/title/title_tileset.png
	@echo convert map tileset ... $(notdir $@)
	$(GFX4SNES) --file-type=png --til-size=8 --pal-col-output=16 --pal-col-use=16 --file-input=$<

build/assets/menus/title/title.tmj: assets/menus/title/title.tmj
	cp $< $@

build/assets/menus/title/title_bg1 build/assets/menus/title/title_bg2 &: build/assets/menus/title/title.tmj
	$(PREPAREROMDATA) -d forest -i $< -o $(@D)

## dialog

build/assets/menus/dialog/dialog_tileset.png: assets/menus/dialog/dialog_tileset.png
	$(PREPAREPNG4GFX2SNES) -i $< -o $@

build/assets/menus/dialog/dialog_tileset.pic: build/assets/menus/dialog/dialog_tileset.png
	@echo convert map tileset ... $(notdir $@)
	$(GFX4SNES) --file-type=png --til-size=8 --map-output --map-noreduction --pal-col-output=16 --pal-col-use=16 --file-input=$<

build/assets/menus/dialog/dialog.tmj: assets/menus/dialog/dialog.tmj
	cp $< $@

build/assets/menus/dialog/dialog_bg1 build/assets/menus/dialog/dialog_bg2 &: build/assets/menus/dialog/dialog.tmj
	$(PREPAREROMDATA) -d dialog -i $< -o $(@D)

## forest

build/assets/levels/forest/forest_tileset.png: assets/levels/forest/forest_tileset.png
	$(PREPAREPNG4GFX2SNES) -i $< -o $@

build/assets/levels/forest/forest_tileset.pic: build/assets/levels/forest/forest_tileset.png
	@echo convert map tileset ... $(notdir $@)
	$(GFX4SNES) --file-type=png --til-size=8 --map-output --map-noreduction --pal-col-output=16 --pal-col-use=16 --file-input=$<

build/assets/levels/forest/forest.tmj: assets/levels/forest/forest.tmj
	cp $< $@

build/assets/levels/forest/forest_header_data build/assets/levels/forest/forest_platforms_data build/assets/levels/forest/forest_bg1 build/assets/levels/forest/forest_bg2 &: build/assets/levels/forest/forest.tmj
	$(PREPAREROMDATA) -d forest -i $< -o $(@D)

## tournament

build/assets/levels/tournament/tournament_tileset.png: assets/levels/tournament/tournament_tileset.png
	$(PREPAREPNG4GFX2SNES) -i $< -o $@

build/assets/levels/tournament/tournament_tileset.pic: build/assets/levels/tournament/tournament_tileset.png
	@echo convert map tileset ... $(notdir $@)
	$(GFX4SNES) --file-type=png --til-size=8 --map-output --map-noreduction --pal-col-output=16 --pal-col-use=16 --file-input=$<

build/assets/levels/tournament/tournament.tmj: assets/levels/tournament/tournament.tmj
	mkdir -p build/assets/levels/tournament
	cp $< $@

build/assets/levels/tournament/tournament_header_data build/assets/levels/tournament/tournament_platforms_data build/assets/levels/tournament/tournament_bg1 build/assets/levels/tournament/tournament_bg2 &: build/assets/levels/tournament/tournament.tmj
	$(PREPAREROMDATA) -d tournament -i $< -o $(@D)

## castle

CASTLE_SOURCE_TMJ := $(wildcard assets/levels/castle/*.tmj)
CASTLE_BUILD_TMJ  := $(addprefix build/, $(CASTLE_SOURCE_TMJ))
CASTLE_BG1        := $(CASTLE_BUILD_TMJ:.tmj=_bg1)
CASTLE_BG2        := $(CASTLE_BUILD_TMJ:.tmj=_bg2)
CASTLE_HEADERS    := $(CASTLE_BUILD_TMJ:.tmj=_header_data)
CASTLE_PLATFORMS  := $(CASTLE_BUILD_TMJ:.tmj=_platforms_data)
CASTLE_FLAILS     := $(CASTLE_BUILD_TMJ:.tmj=_flails_data)
CASTLE_BOARS      := $(CASTLE_BUILD_TMJ:.tmj=_boars_data)
CASTLE_AXES       := $(CASTLE_BUILD_TMJ:.tmj=_axes_data)
CASTLE_CROSSBOMAN := $(CASTLE_BUILD_TMJ:.tmj=_crossbowman_data)
CASTLE_GUARD      := $(CASTLE_BUILD_TMJ:.tmj=_guard_data)

build/assets/levels/castle/castle_tileset.png: assets/levels/castle/castle_tileset.png
	$(PREPAREPNG4GFX2SNES) -i $< -o $@

build/assets/levels/castle/castle_tileset.pic: build/assets/levels/castle/castle_tileset.png
	@echo convert map tileset ... $(notdir $@)
	$(GFX4SNES) --file-type=png --til-size=8 --map-output --map-noreduction --pal-col-output=16 --pal-col-use=16 --file-input=$<

build/assets/levels/castle/%.tmj: assets/levels/castle/%.tmj
	cp $< $@

build/assets/levels/castle/%_header_data build/assets/levels/castle/%_platforms_data build/assets/levels/castle/%_flails_data build/assets/levels/castle/%_boars_data build/assets/levels/castle/%_axes_data build/assets/levels/castle/%_crossbowman_data build/assets/levels/castle/%_guard_data build/assets/levels/castle/%_bg1 build/assets/levels/castle/%_bg2 &: build/assets/levels/castle/%.tmj
	$(PREPAREROMDATA) -d castle -i $< -o $(@D)

castle_assets : $(CASTLE_BG1) $(CASTLE_BG2) $(CASTLE_HEADERS) $(CASTLE_PLATFORMS) $(CASTLE_FLAILS) $(CASTLE_BOARS) $(CASTLE_AXES) $(CASTLE_CROSSBOMAN) $(CASTLE_GUARD) $(CASTLE_BUILD_TMJ) build/assets/levels/castle/castle_tileset.pic

## treasure_room

build/assets/levels/treasure_room/treasure_room_tileset.png: assets/levels/treasure_room/treasure_room_tileset.png
	$(PREPAREPNG4GFX2SNES) -i $< -o $@

build/assets/levels/treasure_room/treasure_room_tileset.pic: build/assets/levels/treasure_room/treasure_room_tileset.png
	@echo convert map tileset ... $(notdir $@)
	$(GFX4SNES) --file-type=png --til-size=8 --map-output --map-noreduction --pal-col-output=16 --pal-col-use=16 --file-input=$<

build/assets/levels/treasure_room/treasure_room.tmj: assets/levels/treasure_room/treasure_room.tmj
	mkdir -p build/assets/levels/treasure_room
	cp $< $@

build/assets/levels/treasure_room/treasure_room_header_data build/assets/levels/treasure_room/treasure_room_platforms_data build/assets/levels/treasure_room/treasure_room_bg1 build/assets/levels/treasure_room/treasure_room_bg2 &: build/assets/levels/treasure_room/treasure_room.tmj
	$(PREPAREROMDATA) -d treasure_room -i $< -o $(@D)

treasure_room_assets: build/assets/levels/treasure_room/treasure_room_tileset.pic build/assets/levels/treasure_room/treasure_room_header_data build/assets/levels/treasure_room/treasure_room_platforms_data build/assets/levels/treasure_room/treasure_room_bg1 build/assets/levels/treasure_room/treasure_room_bg2

## lookup tables

build/assets/circular_lookup_table:
	$(PREPAREROMDATA) -d lookup_tables -o $(@D)

## Musics

build/soundbank.asm : assets/musics/effects.it assets/musics/castle.it assets/musics/dialog.it assets/musics/forest.it assets/musics/title.it assets/musics/tournament.it
	@echo Compiling Soundbank ...
	$(SMCONV) -s -o build/soundbank -V -b 5 $^

musics : build/soundbank.asm

## assets goal

assets : musics sprites build/assets/menus/title/title_tileset.pic build/assets/menus/title/title_bg1 build/assets/menus/title/title_bg2 build/assets/menus/dialog/dialog_tileset.pic build/assets/menus/dialog/dialog_bg1 build/assets/menus/dialog/dialog_bg2 build/assets/levels/forest/forest_bg1 build/assets/levels/forest/forest_bg2 build/assets/levels/forest/forest_header_data build/assets/levels/forest/forest_tileset.pic build/assets/levels/forest/forest_platforms_data build/assets/levels/tournament/tournament_tileset.pic build/assets/levels/tournament/tournament_bg1 build/assets/levels/tournament/tournament_bg2 build/assets/levels/tournament/tournament_header_data build/assets/levels/tournament/tournament_platforms_data build/assets/menus/font/font_tileset.pic castle_assets treasure_room_assets build/assets/circular_lookup_table

# ps files

build/%.ps: src/%.c
	@echo Compiling to .ps... $<
	$(CC) $(CFLAGS) -Wall -c $< -o build/$(*F).ps
ifeq ($(DEBUG),1)
	cp build/$(*F).ps build/$(*F).ps.01.dbg
endif

# asm files

build/%.asm: build/%.ps
	@echo Assembling ... build/$(*F).ps
	$(OPT) build/$(*F).ps >build/$(*F).asp
ifeq ($(DEBUG),1)
	cp build/$(*F).asp build/$(*F).opt.02.dbg
endif
	@echo Moving constants ... build/$(*F).ps
	$(CTF) src/$(*F).c build/$(*F).asp build/$(*F).asm
ifeq ($(DEBUG),1)
	cp build/$(*F).asp build/$(*F).ctf.03.dbg
endif
	@rm build/$(*F).asp

# object files

build/%.obj: build/%.asm
	@echo Doing $(*F).obj file ...
	$(AS) -d -s -x -o build/$(*F).obj -I src build/$(*F).asm

build/data.obj: src/data.asm
	@echo Doing data.obj file ...
	$(AS) -d -s -x -o build/data.obj -I src src/data.asm

build/hdr.obj: src/hdr.asm
	@echo Doing hdr.obj file ...
	$(AS) -d -s -x -o build/hdr.obj -I src src/hdr.asm

build/soundbank.obj: build/soundbank.asm
	@echo Doing soundbank.obj file ...
	$(AS) -d -s -x -o build/soundbank.obj -I src build/soundbank.asm

# rom

build/supermarianandrobin.sfc: build/soundbank.obj build/supermarianandrobin.obj build/smr_animation.obj build/smr_arrow.obj build/smr_axe.obj build/smr_boar.obj build/smr_castle.obj build/smr_character.obj build/smr_chest.obj build/smr_crossbowman.obj build/smr_dialog.obj build/smr_door.obj build/smr_flail.obj build/smr_forest.obj build/smr_king.obj build/smr_guard.obj build/smr_pad.obj build/smr_sound.obj build/smr_target.obj build/smr_title.obj build/smr_text.obj build/smr_tournament.obj build/smr_treasure_room.obj build/smr_window.obj build/smr_world.obj build/data.obj build/hdr.obj
	@echo Creating linkfile ...
	@echo [objects] > build/linkfile
	echo "build/soundbank.obj" >> build/linkfile
	echo "build/smr_arrow.obj" >> build/linkfile
	echo "build/smr_animation.obj" >> build/linkfile
	echo "build/smr_axe.obj" >> build/linkfile
	echo "build/smr_boar.obj" >> build/linkfile
	echo "build/smr_castle.obj" >> build/linkfile
	echo "build/smr_character.obj" >> build/linkfile
	echo "build/smr_chest.obj" >> build/linkfile
	echo "build/smr_crossbowman.obj" >> build/linkfile
	echo "build/smr_dialog.obj" >> build/linkfile
	echo "build/smr_door.obj" >> build/linkfile
	echo "build/smr_flail.obj" >> build/linkfile
	echo "build/smr_forest.obj" >> build/linkfile
	echo "build/smr_king.obj" >> build/linkfile
	echo "build/smr_guard.obj" >> build/linkfile
	echo "build/smr_pad.obj" >> build/linkfile
	echo "build/smr_sound.obj" >> build/linkfile
	echo "build/smr_target.obj" >> build/linkfile
	echo "build/smr_title.obj" >> build/linkfile
	echo "build/smr_text.obj" >> build/linkfile
	echo "build/smr_tournament.obj" >> build/linkfile
	echo "build/smr_treasure_room.obj" >> build/linkfile
	echo "build/smr_window.obj" >> build/linkfile
	echo "build/smr_world.obj" >> build/linkfile
	echo "build/supermarianandrobin.obj" >> build/linkfile
	echo "build/data.obj" >> build/linkfile
	echo "build/hdr.obj" >> build/linkfile
	@for i in $(shell ls $(PVSNESLIB_HOME)/pvsneslib/lib/LoROM_SlowROM); do \
		echo $(PVSNESLIB_HOME)/pvsneslib/lib/LoROM_SlowROM/$$i >> build/linkfile; \
	done
	@echo Linking ... $(notdir build/$@)
	@rm -f build/supermarianandrobin.sym
# -c should be removed ASAP ! It allow duplicate labels and definitions
	$(LD) -d -s -v -A -c -L $(PVSNESLIB_HOME)/pvsneslib/lib/LoROM_SlowROM build/linkfile build/supermarianandrobin.sfc

	@sed -i 's/://' build/supermarianandrobin.sym

	@echo
	@echo Build finished successfully !
	@echo