#include "smr_arrow.h"
#include "smr_oam.h"
#include "smr_palette.h"
#include "smr_world.h"

smr_arrow arrows[SMR_MAX_ARROWS] = {
    {.oam_id = SMR_OAM_ID(62)},
    {.oam_id = SMR_OAM_ID(63)},
    {.oam_id = SMR_OAM_ID(64)}};

void smr_reset_arrows() {
  dmaCopySpr16Vram(&arrow_fly, SMR_VRAM_SPRITE_16_TILE_ADDRESS(0x1000));
  int a;
  for (a = 0; a < SMR_MAX_ARROWS; ++a) {
    smr_arrow *arrow = &arrows[a];
    arrow->tick = SMR_ARROW_MAX_TICKS;
    oamSetEx(arrow->oam_id, OBJ_SMALL, OBJ_HIDE);
  }
}

void smr_shoot_one_arrow(smr_arrow *arrow, s16 x, s16 y, s16 velocity_x) {
  if (arrow->tick >= SMR_ARROW_MAX_TICKS) {
    arrow->tick = 0;
    arrow->velocity_x = velocity_x;
    arrow->is_looking_left = velocity_x < 0 ? true : false;
    arrow->x = x;
    arrow->y = y;
    oamSetEx(arrow->oam_id, OBJ_SMALL, OBJ_SHOW);
    oamSetAttr(arrow->oam_id, x, y, SMR_VRAM_SPRITE_16_TILE_INDEX(0), (velocity_x < 0 ? 0b01110001 : 0b00110001) | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK); // flip if going left, palette 0 of sprite and sprite 16x16 and priority 3
  }
}
void smr_update_arrows() {
  int a;
  for (a = 0; a < SMR_MAX_ARROWS; ++a) {
    smr_arrow *arrow = &arrows[a];

    if (arrow->tick < SMR_ARROW_MAX_TICKS) {
      ++arrow->tick;

      s16 x = arrow->x;
      x += arrow->velocity_x;

      // keep in world
      if (world.wrap_x) {
        if (x < (SMR_WORLD_BOUNDS_LEFT - SMR_ARROW_WIDTH)) {
          x = SMR_WORLD_BOUNDS_RIGHT;
        } else if (x > SMR_WORLD_BOUNDS_RIGHT) {
          x = SMR_WORLD_BOUNDS_LEFT - SMR_ARROW_WIDTH;
        }
      } else {
        if (x < SMR_WORLD_BOUNDS_LEFT) {
          x = SMR_WORLD_BOUNDS_LEFT;
        } else if (x > (SMR_WORLD_BOUNDS_RIGHT - SMR_ARROW_WIDTH)) {
          x = SMR_WORLD_BOUNDS_RIGHT - SMR_ARROW_WIDTH;
        }
      }
      arrow->x = x;
      oamSetXY(arrow->oam_id, x, arrow->y);
    } else {
      oamSetEx(arrow->oam_id, OBJ_SMALL, OBJ_HIDE);
    }
  }
}

void smr_disable_arrow(smr_arrow* arrow) {
  arrow->tick = SMR_ARROW_MAX_TICKS;
}
