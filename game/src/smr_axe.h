#ifndef SMR_AXE_H
#define SMR_AXE_H

#include <snes.h>
#include "smr_axes_rom_data.h"
#include "smr_character.h"

typedef struct {
  u16 oam_ids[4];
  bool active;
  u16 ticks;
  u16 ticks_between_moves;
  s16 tip_x;
  s16 tip_y;
  s16 tip_x_min;
  s16 tip_x_max;
  s16 vx;
  bool is_looking_left;
} smr_axe;

#define SMR_MAX_AXES (4)
#define SMR_AXE_SPEED (1)

extern smr_axe axes[SMR_MAX_AXES];

extern void smr_reset_axes();
extern void smr_add_axes(smr_axes_rom_data* rom_data);
extern void smr_update_axes();
extern void smr_axes_collide(smr_character *character);

#endif