#ifndef SMR_KING_H
#define SMR_KING_H

#include "smr_character.h"
#include "smr_lookup_tables.h"

#define SMR_KING_WIDTH (32)
#define SMR_KING_HEIGHT (64)
#define SMR_KING_FALL_SPEED (2)
#define SMR_KING_IDLE_TICKS (60)
#define SMR_KING_HORIZONTAL_ATTACK_TICKS (16 * 8)
#define SMR_KING_CIRCULAR_ATTACK_TICKS (SMR_LOOKUP_CIRCULAR_PATH_NB_POINTS)
#define SMR_KING_NB_LINKS (8)
#define SMR_KING_HURT_BY_ARROW_TICKS (64)
#define SMR_KING_CRY_TICKS_PER_FRAME (10)
#define SMR_KING_CRY_TICKS (120)
#define SMR_KING_BALL_WIDTH (16)
#define SMR_KING_BALL_HEIGHT (16)
#define SMR_KING_MAX_HEALTH (16)

//tile from treasure room tileset
#define SMR_HEALTH_BAR_TILE_EMPTY (17)
#define SMR_HEALTH_BAR_TILE_FULL (18)

typedef enum {
  SMR_KING_STATE_IDLE = 0,
  SMR_KING_STATE_FALL = 1,
  SMR_KING_STATE_FALL_FROM_TOP = 2,
  SMR_KING_STATE_HORIZONTAL_ATTACK = 3,
  SMR_KING_STATE_CIRCULAR_ATTACK = 4,
  SMR_KING_STATE_CRY = 5,
  SMR_KING_STATE_FALL_FOR_GOOD = 6,
  SMR_KING_STATE_OUT= 7
} smr_king_state;

typedef struct {
  t_sprites *king_upperpart;
  t_sprites *king_lowerpart;
  s16 ball_dx;
  s16 ball_speed;
  s16 ball_x;
  s16 ball_y;
  u16 king_flail_ball_oam_id;
  u16 king_flail_links_oam_ids[SMR_KING_NB_LINKS];
  u16 king_scepter_oam_id;
  smr_king_state state;
  s16 x;
  s16 y;
  s16 ground_y;
  u16 ticks;
  u16 hurt_ticks;
  u16 cry_ticks;
  u8 health;
} smr_king;

extern smr_king king;

extern void smr_add_king(s16 x, s16 y);
extern void smr_update_king();
extern void smr_king_collide(smr_character *character);
extern void smr_king_update_health_bar();
extern void smr_king_fill_health_bar();

#endif