#include "smr_treasure_room.h"
#include "smr_adventure.h"
#include "smr_character.h"
#include "smr_chest.h"
#include "smr_king.h"
#include "smr_oam.h"
#include "smr_pad.h"
#include "smr_palette.h"
#include "smr_platform_rom_data.h"
#include "smr_text.h"
#include "smr_treasure_room_rom_data.h"
#include "smr_window.h"
#include "smr_world.h"

extern u8 treasure_room_tileset, treasure_room_tileset_end;
extern u8 treasure_room_bg1, treasure_room_bg1_end;
extern u8 treasure_room_bg2, treasure_room_bg2_end;

// Data in ROM
extern smr_treasure_room_rom_data_header treasure_room_header_data;
extern smr_platforms_rom_data treasure_room_platforms_data;

int current_treasure_room_level = 0;

typedef enum {
  SMR_TREASURE_ROOM_SCENE_STATE_FILL_KING_HEALTH_BAR = 0,
  SMR_TREASURE_ROOM_SCENE_STATE_FIGHT = 1,
  SMR_TREASURE_ROOM_SCENE_STATE_TAKE_CHEST = 2
} smr_treasure_room_scene_state;

smr_adventure_outcome smr_scene_treasure_room() {

  smr_treasure_room_scene_state scene_state = SMR_TREASURE_ROOM_SCENE_STATE_FILL_KING_HEALTH_BAR;

  // reinit
  setScreenOff();
  oamClear(0, 128);
  dmaClearVram();

  // Init layers with tiles
  bgInitTileSet(0, &treasure_room_tileset, &smr_palette, 0,
                (&treasure_room_tileset_end - &treasure_room_tileset), 16 * 2, BG_16COLORS,
                32 * 1024 / 2);
  bgInitTileSet(1, &treasure_room_tileset, &smr_palette, 0,
                (&treasure_room_tileset_end - &treasure_room_tileset), 16 * 2, BG_16COLORS,
                32 * 1024 / 2);
  smr_text_init_bg3_tileset();
  bgInitMapSet(0, &treasure_room_bg1,
               &treasure_room_bg1_end - &treasure_room_bg1, SC_32x32,
               60 * 1024 / 2);
  bgInitMapSet(1, &treasure_room_bg2,
               &treasure_room_bg2_end - &treasure_room_bg2, SC_32x32,
               62 * 1024 / 2);

  smr_text_init_bg3_map();
  smr_text_clear();

  // Now Put in 16 color mode and enable backgrounds
  setMode(BG_MODE1, BG3_MODE1_PRIORITY_HIGH);
  bgSetEnable(0);
  bgSetEnable(1);
  bgSetEnable(2);

  // init world
  world.platforms = &treasure_room_platforms_data;
  world.wrap_x = true;
  world.wrap_y = false;
  world.shooting_allowed = true;

  // init sprites

  dmaCopyCGram(&smr_palette, (128 + SMR_PALETTE_SPRITE_INDEX * 16), 16 * 2);
  dmaCopyCGram(&smr_red_palette, (128 + SMR_RED_PALETTE_SPRITE_INDEX * 16), 16 * 2);

  oamInitDynamicSprite(SMR_VRAM_SPRITE_32_ADDRESS, SMR_VRAM_SPRITE_16_ADDRESS, 0, 0, OBJ_SIZE16_L32); // TODO: comprendre les valeurs magiques tirées d'un exemple de pvsneslib

  // characters
  smr_character_reset_body(&characters[SMR_CHARACTER_TYPE_ROBIN], treasure_room_header_data.robinX, treasure_room_header_data.robinY);
  smr_character_reset_sprite(&characters[SMR_CHARACTER_TYPE_ROBIN]);

  // marian
  smr_character_reset_body(&characters[SMR_CHARACTER_TYPE_MARIAN], treasure_room_header_data.marianX, treasure_room_header_data.marianY);
  smr_character_reset_sprite(&characters[SMR_CHARACTER_TYPE_MARIAN]);

  // arrows
  smr_reset_arrows();

  // king
  smr_add_king(treasure_room_header_data.kingX, treasure_room_header_data.kingY);
  smr_king_update_health_bar();

  // chest
  smr_add_chest(treasure_room_header_data.chestX, treasure_room_header_data.chestY);

  // Scroll
  bgSetScroll(0, 0, -1);
  bgSetScroll(1, 0, -1);
  bgSetScroll(2, 0, -1);

  // Screen activated
  setScreenOn();

  while (1) {
    smr_update_pads();

    int ch;
    for (ch = 0; ch < SMR_CHARACTER_TYPE_COUNT; ++ch) {
      smr_character *character = &characters[ch];
      if (character->active) {
        smr_character_control_with_pad(character, ch);
        smr_character_update_body(character);
        smr_character_update_sprite(character);
      }
    }

    smr_update_king();
    smr_update_arrows();
    smr_update_chest();

    for (ch = 0; ch < SMR_CHARACTER_TYPE_COUNT; ++ch) {
      smr_character *character = &characters[ch];
      if (character->active) {
        smr_king_collide(character);
      }
    }

    spcProcess();
    oamInitDynamicSpriteEndFrame();
    WaitForVBlank();
    oamVramQueueUpdate();
    smr_king_update_health_bar();

    switch (scene_state) {
    case SMR_TREASURE_ROOM_SCENE_STATE_FILL_KING_HEALTH_BAR: {
      smr_king_fill_health_bar();
      scene_state = SMR_TREASURE_ROOM_SCENE_STATE_FIGHT;
      break;
    }
    case SMR_TREASURE_ROOM_SCENE_STATE_FIGHT: {
      if (SMR_KING_STATE_OUT == king.state) {
        smr_chest_set_fall();
        scene_state = SMR_TREASURE_ROOM_SCENE_STATE_TAKE_CHEST;
      }
      break;
    }
    case SMR_TREASURE_ROOM_SCENE_STATE_TAKE_CHEST: {
      break;
    }
    }

    int nb_active = 0;
    int nb_out = 0;
    int nb_dead = 0;
    for (ch = 0; ch < SMR_CHARACTER_TYPE_COUNT; ++ch) {
      smr_character *character = &characters[ch];
      if (character->active) {
        ++nb_active;
        if (character->state == SMR_CHARACTER_STATE_OUT) {
          ++nb_out;
        } else if (character->state == SMR_CHARACTER_STATE_DEAD_AND_LOVING_IT) {
          ++nb_dead;
        }
      }
    }
    if (nb_out > 0 && (nb_out + nb_dead) == nb_active) {
      return SMR_ADVENTURE_OUTCOME_WIN;
    } else if (nb_dead == nb_active) {
      return SMR_ADVENTURE_OUTCOME_LOSE;
    }
  }
}
