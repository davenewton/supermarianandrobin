#include "smr_character.h"

#include "smr_animation.h"
#include "smr_oam.h"
#include "smr_pad.h"
#include "smr_palette.h"
#include "smr_world.h"
#include "snes/sprite.h"
#include "smr_arrow.h"
#include "smr_sound.h"

extern smr_animation_rom_data robin_ghost;
extern smr_animation_rom_data robin_idle;
extern smr_animation_rom_data robin_ohai;
extern smr_animation_rom_data robin_shoot;
extern smr_animation_rom_data robin_walk;

extern smr_animation_rom_data marian_ghost;
extern smr_animation_rom_data marian_idle;
extern smr_animation_rom_data marian_ohai;
extern smr_animation_rom_data marian_shoot;
extern smr_animation_rom_data marian_walk;

smr_animation_rom_data *character_animations_data_in_rom
    [SMR_CHARACTER_TYPE_COUNT][SMR_CHARACTER_ANIMATION_COUNT] = {
        {&marian_ghost, &marian_idle, &marian_ohai, &marian_shoot, &marian_walk},
        {&robin_ghost, &robin_idle, &robin_ohai, &robin_shoot, &robin_walk}};

smr_character characters[SMR_CHARACTER_TYPE_COUNT] = {{.active = true,
                                                 .sprite = SMR_OAMBUFFER_ID_TO_PTR(0),
                                                 .type = SMR_CHARACTER_TYPE_MARIAN,
                                                 .arrow_stucked = NULL,
                                                 .own_arrow = &arrows[SMR_MARIAN_ARROW]},
                                                {.active = true,
                                                 .sprite = SMR_OAMBUFFER_ID_TO_PTR(1),
                                                 .type = SMR_CHARACTER_TYPE_ROBIN,
                                                 .arrow_stucked = NULL,
                                                 .own_arrow = &arrows[SMR_ROBIN_ARROW]}};

void smr_character_set_animation(smr_character *character,
                                 smr_character_animation_type type) {
  if (character->animation_type != type) {
    smr_animation_rom_data *anim = character_animations_data_in_rom[character->type][type];
    character->animation_type = type;
    smr_streamed_animation_start(&character->animation, anim, character->sprite);
  }
}

void smr_character_reset_body(smr_character *character, s16 x, s16 y) {
  character->x = x;
  character->y = y;
  character->velocity_x = 0;
  character->velocity_y = 0;
  character->jump_frames = 0;
  character->is_on_ground = FALSE;
  character->state = SMR_CHARACTER_STATE_MOVING;
}

void smr_character_kill(smr_character *character) {
  character->state = SMR_CHARACTER_STATE_DEAD;
  character->dead_x = character->x;
  character->velocity_x = 1;
}

void smr_character_control_with_pad(smr_character *character, u16 pad) {
  u16 pad_current = padsCurrent(pad);
  switch (character->state) {
  case SMR_CHARACTER_STATE_MOVING:
    if (pad_current & KEY_LEFT) {
      character->velocity_x = -SMR_CHARACTER_WALK_SPEED;
    } else if (pad_current & KEY_RIGHT) {
      character->velocity_x = SMR_CHARACTER_WALK_SPEED;
    } else {
      character->velocity_x = 0;
    }

    if (character->is_on_ground) {
      if (smr_test_and_reset_if_pressed(pad, KEY_B)) {
        smr_play_sound(SMR_SOUND_JUMP);
        character->jump_frames = SMR_CHARACTER_JUMP_IMPULSE_FRAMES;
      }
      if (world.shooting_allowed && smr_test_and_reset_if_pressed(pad, KEY_A) && character->own_arrow->tick >= SMR_ARROW_MAX_TICKS) {
        character->state = SMR_CHARACTER_STATE_SHOOTING;
        character->velocity_x = 0;
        character->velocity_y = 0;
      }
    }

    if (character->jump_frames > 0) {
      --character->jump_frames;
      character->velocity_y = SMR_CHARACTER_JUMP_IMPULSE;
    }
    break;
  case SMR_CHARACTER_STATE_SHOOTING:
    break;
  case SMR_CHARACTER_STATE_HIT_BY_ARROW:
    break;
  case SMR_CHARACTER_STATE_DEAD:
    break;
  case SMR_CHARACTER_STATE_DEAD_AND_LOVING_IT:
    break;
  case SMR_CHARACTER_STATE_OUT:
    break;
  }
}

void smr_character_update_body(smr_character *character) {
  // TODO use switch
  if (character->state == SMR_CHARACTER_STATE_HIT_BY_ARROW) {
    smr_arrow* arrow_stucked = character->arrow_stucked;
    if (arrow_stucked) {
      if (arrow_stucked->is_looking_left) { // TODO macro to detect if arrow is looking left macro or function
        character->x = arrow_stucked->x - SMR_CHARACTER_WIDTH / 2;
      } else {
        character->x = arrow_stucked->x + SMR_ARROW_WIDTH/2 - SMR_CHARACTER_WIDTH / 2;
      }

      if (character->arrow_stucked->tick >= SMR_ARROW_MAX_TICKS) {
        character->arrow_stucked = NULL;
        smr_character_kill(character);
      }
      return;
    } else {
      // TODO
    }
  } else if (character->state == SMR_CHARACTER_STATE_DEAD) {
    character->x += character->velocity_x;
    if (character->x > (character->dead_x + SMR_CHARACTER_GHOST_HORIZONTAL_AMPLITUDE)) {
      character->velocity_x = -1;
    } else if (character->x < (character->dead_x - SMR_CHARACTER_GHOST_HORIZONTAL_AMPLITUDE)) {
      character->velocity_x = 1;
    }
    character->y -= SMR_CHARACTER_WALK_SPEED;
    if (character->y < (SMR_WORLD_BOUNDS_TOP - SMR_CHARACTER_HEIGHT)) {
      character->state = SMR_CHARACTER_STATE_DEAD_AND_LOVING_IT;
    }
    return;
  } else if (character->state == SMR_CHARACTER_STATE_DEAD_AND_LOVING_IT) {
    return;
  } else if (character->state == SMR_CHARACTER_STATE_OUT) {
    return;
  }

  // update velocity
  s8 vx = character->velocity_x;
  s8 vy = character->velocity_y;
  vy += SMR_WORLD_GRAVITY;
  if (vx > SMR_CHARACTER_MAX_VELOCITY_X) {
    vx = SMR_CHARACTER_MAX_VELOCITY_X;
  } else if (vx < SMR_CHARACTER_MIN_VELOCITY_X) {
    vx = SMR_CHARACTER_MIN_VELOCITY_X;
  }
  if (vy > SMR_CHARACTER_MAX_VELOCITY_Y) {
    vy = SMR_CHARACTER_MAX_VELOCITY_Y;
  } else if (vy < SMR_CHARACTER_MIN_VELOCITY_Y) {
    vy = SMR_CHARACTER_MIN_VELOCITY_Y;
  }
  character->velocity_x = vx;
  character->velocity_y = vy;

  // update position
  character->is_on_ground = FALSE;
  s16 character_left = character->x;
  s16 character_top = character->y;
  s16 character_previous_left = character_left;
  s16 character_previous_top = character_top;
  character_left += vx;
  character_top += vy;

  // keep in world
  if (world.wrap_x) {
    if (character_left < (SMR_WORLD_BOUNDS_LEFT - SMR_CHARACTER_WIDTH)) {
      character_left = SMR_WORLD_BOUNDS_RIGHT;
    } else if (character_left > SMR_WORLD_BOUNDS_RIGHT) {
      character_left = SMR_WORLD_BOUNDS_LEFT - SMR_CHARACTER_WIDTH;
    }
  } else {
    if (character_left < SMR_WORLD_BOUNDS_LEFT) {
      character_left = SMR_WORLD_BOUNDS_LEFT;
    } else if (character_left > (SMR_WORLD_BOUNDS_RIGHT - SMR_CHARACTER_WIDTH)) {
      character_left = SMR_WORLD_BOUNDS_RIGHT - SMR_CHARACTER_WIDTH;
    }
  }
  if (world.wrap_y) {
    if (character_top < (SMR_WORLD_BOUNDS_TOP - SMR_CHARACTER_HEIGHT)) {
      // character_top = SMR_WORLD_BOUNDS_BOTTOM;
    } else if (character_top > SMR_WORLD_BOUNDS_BOTTOM) {
      character_top = SMR_WORLD_BOUNDS_TOP - SMR_CHARACTER_HEIGHT;
    }
  } else {
    if (character_top < SMR_WORLD_BOUNDS_TOP) {
      character_top = SMR_WORLD_BOUNDS_TOP;
    } else if (character_top > (SMR_WORLD_BOUNDS_BOTTOM - SMR_CHARACTER_HEIGHT)) {
      character_top = SMR_WORLD_BOUNDS_BOTTOM - SMR_CHARACTER_HEIGHT;
      character->is_on_ground = true;
    }
  }
  character->x = character_left;
  character->y = character_top;

  s16 character_right = character_left + SMR_CHARACTER_WIDTH;
  s16 character_bottom = character_top + SMR_CHARACTER_HEIGHT;

  // collide with arrows
  int a;
  for (a = 0; a < SMR_MAX_ARROWS; ++a) {
    smr_arrow *arrow = &arrows[a];
    if (arrow->tick >= SMR_ARROW_MAX_TICKS) {
      continue;
    }
    s16 arrow_tip_x = arrow->is_looking_left ? arrow->x : arrow->x + SMR_ARROW_WIDTH;
    if (arrow_tip_x > character_left && arrow_tip_x < character_right) {
      s16 arrow_tip_y = arrow->y + SMR_ARROW_HEIGHT / 2;
      if (arrow_tip_y > character_top && arrow_tip_y < character_bottom) {
        character->state = SMR_CHARACTER_STATE_HIT_BY_ARROW;
        character->arrow_stucked = arrow;
        return;
      }
    }
  }

  // collide with platforms
  smr_platforms_rom_data *platforms_data = world.platforms;
  u16 p = 0;
  for (p = 0; p < platforms_data->nb_platforms; ++p) {
    smr_platform_rom_data *platform = &platforms_data->platforms[p];
    s16 platform_left = platform->x;
    if (character_right <= platform_left) {
      continue;
    }
    // TODO precompute platform_right and platform_bottom ?
    s16 platform_right = platform_left + platform->w;
    if (character_left >= platform_right) {
      continue;
    }
    s16 platform_top = platform->y;
    s16 platform_bottom = platform_top + platform->h;
    bool is_character_going_down = character_top > character_previous_top;
    if (is_character_going_down && platform_top < character_bottom && character_bottom < platform_bottom) {
      // TODO precompute character->y when on top of platform?
      character->y = platform_top - SMR_CHARACTER_HEIGHT;
      character->is_on_ground = true;

      if (platform->flags & SMR_PLATFORM_FLAG_LETHAL) {
        smr_character_kill(character);
        break;
      }
    }
  }
}

void smr_character_reset_sprite(smr_character *character) {
  // vhoopppc v : vertical flip h: horizontal flip o: priority bits p: palette
  // num c : last byte of tile num)
  t_sprites* sprite = character->sprite;
  sprite->oamattribute = 0b00110000 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK; // sprite 32x32 and priority 3
  sprite->oamgraphics = 0;
  sprite->oamrefresh = 0;
  sprite->oamframeid = 0;
  character->animation_type = -1;
}

void smr_character_update_sprite(smr_character *character) {
  t_sprites* sprite = character->sprite;
  sprite->oamx = character->x + SMR_CHARACTER_SPRITE_OFFSET_X;
  sprite->oamy = character->y + SMR_CHARACTER_SPRITE_OFFSET_Y;

  s16 vx = character->velocity_x;

  switch (character->state) {
  case SMR_CHARACTER_STATE_MOVING:
    if (vx < 0) {
      sprite->oamattribute = 0b01110000 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK; // sprite 32x32 and priority 3
      smr_character_set_animation(character, SMR_CHARACTER_ANIMATION_WALK);
    } else if (vx > 0) {
      sprite->oamattribute = 0b00110000 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK; // sprite 32x32 and priority 3
      smr_character_set_animation(character, SMR_CHARACTER_ANIMATION_WALK);
    } else {
      smr_character_set_animation(character, SMR_CHARACTER_ANIMATION_IDLE);
    }
    break;
  case SMR_CHARACTER_STATE_SHOOTING:
    smr_character_set_animation(character, SMR_CHARACTER_ANIMATION_SHOOT);
    if (character->animation.loop_count > 0) {
      if (sprite->oamattribute & 0b01000000) { // TODO macro to detect if character is looking left macro or function
        smr_shoot_one_arrow(character->own_arrow, character->x - SMR_ARROW_WIDTH, character->y + (SMR_CHARACTER_HEIGHT / 2 - SMR_ARROW_HEIGHT / 2), -SMR_ARROW_VELOCITY);
      } else {
        smr_shoot_one_arrow(character->own_arrow, character->x + SMR_CHARACTER_WIDTH, character->y + (SMR_CHARACTER_HEIGHT / 2 - SMR_ARROW_HEIGHT / 2), SMR_ARROW_VELOCITY);
      }
      character->state = SMR_CHARACTER_STATE_MOVING;
    }
    break;
  case SMR_CHARACTER_STATE_HIT_BY_ARROW:
    break;
  case SMR_CHARACTER_STATE_DEAD:
    smr_character_set_animation(character, SMR_CHARACTER_ANIMATION_GHOST);
    break;
  case SMR_CHARACTER_STATE_DEAD_AND_LOVING_IT:
    break;
  case SMR_CHARACTER_STATE_OUT:
    break;
  }

  // TODO inline function?
  smr_streamed_animation_update(&character->animation, sprite);
  oamDynamic32Draw(sprite - oambuffer);
}

void smr_character_set_out(smr_character *character) {
  if (SMR_CHARACTER_STATE_MOVING == character->state) {
    character->state = SMR_CHARACTER_STATE_OUT;
    character->x = -100; // TODO find better technique to hide sprite
  }
}