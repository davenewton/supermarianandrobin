#include "smr_flail.h"
#include "smr_lookup_tables.h"
#include "smr_oam.h"
#include "smr_palette.h"
#include "snes/dma.h"
#include "snes/snestypes.h"
#include "snes/sprite.h"

extern u8 flail_ball;
extern u8 flail_link;

smr_flail flails[SMR_MAX_FLAILS] = {
    {.ball_oam_id = SMR_OAM_ID(6),
     .links_oam_ids = {SMR_OAM_ID(7), SMR_OAM_ID(8), SMR_OAM_ID(9), SMR_OAM_ID(10), SMR_OAM_ID(11), SMR_OAM_ID(12), SMR_OAM_ID(13), SMR_OAM_ID(14)},
     .active = false},
    {.ball_oam_id = SMR_OAM_ID(15),
     .links_oam_ids = {SMR_OAM_ID(16), SMR_OAM_ID(17), SMR_OAM_ID(18), SMR_OAM_ID(19), SMR_OAM_ID(20), SMR_OAM_ID(21), SMR_OAM_ID(22), SMR_OAM_ID(23)},
     .active = false},
};

void smr_reset_flails() {
  dmaCopySpr16Vram(&flail_ball, SMR_VRAM_SPRITE_16_TILE_ADDRESS(0x1020));
  dmaCopySpr16Vram(&flail_link, SMR_VRAM_SPRITE_16_TILE_ADDRESS(0x1040));

  int f;
  for (f = 0; f < SMR_MAX_FLAILS; ++f) {
    smr_flail* flail = &flails[f];
    flail->active =false;

    oamSetEx(flail->ball_oam_id, OBJ_SMALL, OBJ_HIDE);
    oamSetAttr(flail->ball_oam_id, -100, 0, SMR_VRAM_SPRITE_16_TILE_INDEX(2), 0b00110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK); // sprite 16x16 and priority 3

    int c;
    for (c = 0; c < SMR_FLAIL_NB_LINKS; ++c) {
      u16 link_oamid = flail->links_oam_ids[c];
      oamSetEx(link_oamid, OBJ_SMALL, OBJ_HIDE);
      oamSetAttr(link_oamid, -100, 0, SMR_VRAM_SPRITE_16_TILE_INDEX(4), 0b00110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK);// sprite 16x16 and priority 3
    }
  }
}

void smr_add_flail(s16 anchor_x, s16 anchor_y) {
  int f;
  for (f = 0; f < SMR_MAX_FLAILS; ++f) {
    smr_flail* flail = &flails[f];
    if (!flail->active) {
      flail->anchor_x = anchor_x + SMR_FLAIL_SPRITE_OFFSET_X;
      flail->anchor_y = anchor_y + SMR_FLAIL_SPRITE_OFFSET_Y;
      flail->ball_tick = 0;
      flail->active = true;
      oamSetEx(flail->ball_oam_id, OBJ_SMALL, OBJ_SHOW);
      int c;
      for (c = 0; c < SMR_FLAIL_NB_LINKS; ++c) {
        oamSetEx(flail->links_oam_ids[c], OBJ_SMALL, OBJ_SHOW);
      }
      return;
    }
  }
}

void smr_add_flails(smr_flails_rom_data *rom_data) {
  int f;
  for (f = 0; f < rom_data->nb_flails; ++f) {
    smr_add_flail(rom_data->flails[f].anchor_x, rom_data->flails[f].anchor_y);
  }
}

void smr_flails_collide(smr_character *character) {
  int f;
  s16 character_center_x = character->x + SMR_CHARACTER_WIDTH / 2;
  s16 character_center_y = character->y + SMR_CHARACTER_WIDTH / 2;
  for (f = 0; f < SMR_MAX_FLAILS; ++f) {
    smr_flail *flail = &flails[f];
    if (!flail->active) {
      continue;
    }
    s16 ball_left = flail->ball_x;
    if (character_center_x > ball_left && character_center_x < (ball_left + SMR_FLAIL_BALL_WIDTH)) {
      s16 ball_top = flail->ball_y;
      if (character_center_y > ball_top && character_center_y < (ball_top + SMR_FLAIL_BALL_HEIGHT)) {
        smr_character_kill(character);
        return;
      }
    }
  }
}

void smr_update_flail(smr_flail *flail) {
  if (!flail->active) {
    return;
  }
  u16 path_index = flail->ball_tick = (flail->ball_tick + 1) % SMR_LOOKUP_CIRCULAR_PATH_NB_POINTS;
  s16 path_x = circular_path[path_index].x;
  s16 path_y = circular_path[path_index].y;
  s16 anchor_x = flail->anchor_x;
  s16 anchor_y = flail->anchor_y;
  s16 ball_x = anchor_x + (path_x >> 2);
  s16 ball_y = anchor_y + (path_y >> 2);
  flail->ball_x = ball_x;
  flail->ball_y = ball_y;
  oamSetXY(flail->ball_oam_id, ball_x, ball_y);

  int c;
  s16 px = path_x << 2;
  s16 py = path_y << 2;
  u16 first_link_gfxoffset;
  for (c = 0; c < SMR_FLAIL_NB_LINKS; ++c) {
    oamSetXY(flail->links_oam_ids[c], anchor_x + (px >> SMR_LOOKUP_CIRCULAR_PATH_RADIUS_DIVIDE_SHIFT), anchor_y + (py >> SMR_LOOKUP_CIRCULAR_PATH_RADIUS_DIVIDE_SHIFT));
    px += path_x << 3;
    py += path_y << 3;
  }
}

extern void smr_update_flails() {
  int f;
  for (f = 0; f < SMR_MAX_FLAILS; ++f) {
    smr_update_flail(&flails[f]);
  }
}