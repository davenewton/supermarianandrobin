#include "smr_title.h"
#include "smr_pad.h"
#include "smr_palette.h"
#include "smr_sound.h"
#include "smr_text.h"

extern u8 title_tileset, title_tileset_end;
extern u8 title_bg1, title_bg1_end;
extern u8 title_bg2, title_bg2_end;

void smr_scene_title() {

  // reinit
  setScreenOff();
  oamClear(0, 128);
  dmaClearVram();

  // Init layers with tiles
  bgInitTileSet(0, &title_tileset, &smr_palette, 0,
                (&title_tileset_end - &title_tileset), 16 * 2, BG_16COLORS,
                32 * 1024 / 2);
  bgInitTileSet(1, &title_tileset, &smr_palette, 0,
                (&title_tileset_end - &title_tileset), 16 * 2, BG_16COLORS,
                32 * 1024 / 2);
  smr_text_init_bg3_tileset();
  bgInitMapSet(0, &title_bg1,
               &title_bg1_end - &title_bg1, SC_32x32,
               60 * 1024 / 2);
  bgInitMapSet(1, &title_bg2,
               &title_bg2_end - &title_bg2, SC_32x32,
               62 * 1024 / 2);

  smr_text_init_bg3_map();
  smr_text_clear();
  smr_text_print(10, 20, 24, 28, SMR_TEXT_MASK_WHITE, "PRESS START");
  smr_text_update_vram();

  // Now Put in 16 color mode and enable backgrounds
  setMode(BG_MODE1, BG3_MODE1_PRIORITY_HIGH);
  bgSetEnable(0);
  bgSetEnable(1);
  bgSetEnable(2);

  // Scroll
  bgSetScroll(0, 0, 0);
  bgSetScroll(1, 0, 0);
  bgSetScroll(2, 0, 0);

  // Music
  smr_play_music(MOD_TITLE);

  setScreenOn();

  while (1) {
    smr_update_pads();
    if (smr_test_if_confirm()) {
      smr_play_sound(SMR_SOUND_START);
      spcProcess();
      return;
    }

    spcProcess();
    WaitForVBlank();
  }
}
