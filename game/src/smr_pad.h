#ifndef SMR_PAD_H
#define SMR_PAD_H

#include <snes.h>

extern void smr_update_pads();
extern bool smr_test_and_reset_if_pressed(u16 pad, u16 key);
extern bool smr_test_if_confirm();

#endif