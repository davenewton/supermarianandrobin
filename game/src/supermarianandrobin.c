#include "smr_castle.h"
#include "smr_castle_rom_data.h"
#include "smr_character.h"
#include "smr_dialog.h"
#include "smr_forest.h"
#include "smr_pad.h"
#include "smr_palette.h"
#include "smr_text.h"
#include "smr_title.h"
#include "smr_tournament.h"
#include "smr_treasure_room.h"
#include "smr_world.h"
#include "smr_sound.h"

int main(void) {
  spcBoot();
  //consoleInit();

  smr_sound_init();

  // for debugging castle levels
  //smr_scene_treasure_room();

  /*while(1) {
  current_castle_level = SMR_CASTLE_NB_LEVELS-1;
  current_castle_level = 2;
  //characters[SMR_CHARACTER_TYPE_ROBIN].active = false;
  smr_scene_castle();
  }*/

  // for debugging dialogs
  /*
  smr_scene_dialog(&dialog_character_select);
  smr_scene_dialog(&dialog_activity_select);
  smr_scene_dialog(&dialog_mariansvictory);
  smr_scene_dialog(&dialog_robinsvictory);
  smr_scene_dialog(&dialog_castlebadending);
  smr_scene_dialog(&dialog_castlegoodending);
  smr_scene_dialog(&dialog_castlebadending);
  */

  while (1) {
    smr_scene_title();
    setFadeEffect(FADE_OUT);
    smr_character_selection character_selection = smr_scene_dialog(&dialog_character_select);
    switch (character_selection) {
    case SMR_SELECT_MARIAN:
      characters[SMR_CHARACTER_TYPE_MARIAN].active = true;
      characters[SMR_CHARACTER_TYPE_ROBIN].active = false;
      break;
    case SMR_SELECT_ROBIN:
      characters[SMR_CHARACTER_TYPE_MARIAN].active = false;
      characters[SMR_CHARACTER_TYPE_ROBIN].active = true;
      break;
    case SMR_SELECT_BOTH:
      characters[SMR_CHARACTER_TYPE_MARIAN].active = true;
      characters[SMR_CHARACTER_TYPE_ROBIN].active = true;
      break;
    }
    setFadeEffect(FADE_OUT);
    smr_after_activity_selection smr_after_activity_selection = SMR_EXIT;
    current_castle_level = 0;
    do {
      smr_activity_selection activity = smr_scene_dialog(&dialog_activity_select);
      setFadeEffect(FADE_OUT);
      do {
        switch (activity) {
        case SMR_CASTLE: {
          smr_adventure_outcome castle_scene_outcome = smr_scene_castle();
          switch (castle_scene_outcome) {
          case SMR_ADVENTURE_OUTCOME_LOSE:
            smr_after_activity_selection = smr_scene_dialog(&dialog_castlebadending);
            setFadeEffect(FADE_OUT);
            break;
          case SMR_ADVENTURE_OUTCOME_WIN:
            smr_after_activity_selection = smr_scene_dialog(&dialog_castlegoodending);
            current_castle_level = 0;
            setFadeEffect(FADE_OUT);
            break;
          }
          break;
        }
        case SMR_TOURNAMENT:
          smr_scene_tournament();
          setFadeEffect(FADE_OUT);
          smr_after_activity_selection = SMR_EXIT;
          break;
        case SMR_FOREST: {
          smr_forest_scene_outcome forest_scene_outcome = smr_scene_forest();
          switch (forest_scene_outcome) {
          case SMR_FOREST_SCENE_OUTCOME_MARIAN_S_VICTORY:
            smr_after_activity_selection = smr_scene_dialog(&dialog_mariansvictory);
            setFadeEffect(FADE_OUT);
            break;
          case SMR_FOREST_SCENE_OUTCOME_ROBIN_S_VICTORY:
            smr_after_activity_selection = smr_scene_dialog(&dialog_robinsvictory);
            setFadeEffect(FADE_OUT);
            break;
          }
          break;
        }
        }
      } while (smr_after_activity_selection == SMR_PLAY_AGAIN);
    } while (smr_after_activity_selection == SMR_DO_SOMETHING_ELSE);
  }
  return 0;
}
