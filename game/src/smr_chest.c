#include "smr_chest.h"
#include "smr_character.h"
#include "smr_oam.h"
#include "smr_palette.h"

extern u8 chest_open_close;

smr_chest chest = {
    .sprite = SMR_OAMBUFFER_ID_TO_PTR(40)};

void smr_add_chest(s16 x, s16 y) {
  chest.state = SMR_CHEST_STATE_INACTIVE;
  chest.x = x;
  chest.y = y;
}

void smr_chest_set_fall() {
  chest.state = SMR_CHEST_STATE_FALL;
  chest.sprite->oamx = chest.x;
  chest.sprite->oamy = -SMR_CHEST_HEIGHT;
  chest.sprite->oamattribute = 0b00110000 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK; // sprite 32x32
  chest.sprite->oamgraphics = &chest_open_close;
  chest.sprite->oamframeid = 0;
  chest.sprite->oamrefresh = 1;
}

void smr_chest_collide(smr_character *character) {
  s16 character_center_x = character->x + SMR_CHARACTER_WIDTH / 2;
  if (character_center_x >= (chest.x + SMR_CHEST_COLLIDE_DISTANCE) && character_center_x <= (chest.x + SMR_CHEST_WIDTH - SMR_CHEST_COLLIDE_DISTANCE)) {
    s16 character_center_y = character->y + SMR_CHARACTER_HEIGHT / 2;
    if (character_center_y >= chest.y && character_center_y <= (chest.y + SMR_CHEST_HEIGHT)) {
      smr_character_set_out(character);
    }
  }
}

void smr_update_chest() {
  switch (chest.state) {
  case SMR_CHEST_STATE_INACTIVE: {
    break;
  }
  case SMR_CHEST_STATE_FALL: {
    chest.sprite->oamy += SMR_CHEST_FALL_SPEED;
    if (chest.sprite->oamy >= chest.y) {
      chest.sprite->oamy = chest.y;
      chest.sprite->oamframeid = 1;
      chest.sprite->oamrefresh = 1;
      chest.state = SMR_CHEST_STATE_OPEN;
    }
    break;
  }
  case SMR_CHEST_STATE_OPEN: {
    int ch;
    for (ch = 0; ch < SMR_CHARACTER_TYPE_COUNT; ++ch) {
      smr_character *character = &characters[ch];
      if (character->active) {
        smr_chest_collide(character);
      }
    }
  }
  }
  oamDynamic32Draw(chest.sprite - oambuffer);
}