#include "smr_king.h"
#include "smr_arrow.h"
#include "smr_oam.h"
#include "smr_palette.h"
#include "smr_world.h"
#include "snes/interrupt.h"

extern u8 king_attacks;
extern u8 king_scepter;
extern u8 king_flail_ball;
extern u8 king_flail_link;
extern u8 king_cry;

smr_king king = {
    .king_upperpart = SMR_OAMBUFFER_ID_TO_PTR(20),
    .king_lowerpart = SMR_OAMBUFFER_ID_TO_PTR(21),
    .king_scepter_oam_id = SMR_OAM_ID(70),
    .king_flail_ball_oam_id = SMR_OAM_ID(71),
    .king_flail_links_oam_ids = {SMR_OAM_ID(72), SMR_OAM_ID(73), SMR_OAM_ID(74), SMR_OAM_ID(75), SMR_OAM_ID(76), SMR_OAM_ID(77), SMR_OAM_ID(78), SMR_OAM_ID(79)},
    .hurt_ticks = 0};

u16 healthbar[SMR_KING_MAX_HEALTH];
u16 healthbar_current_value;

void smr_king_fill_health_bar() {
  healthbar_current_value = 0;
  for (healthbar_current_value = 0; healthbar_current_value <= king.health; ++healthbar_current_value) {
    int h;
    for (h = 0; h < healthbar_current_value; ++h) {
      healthbar[h] = SMR_HEALTH_BAR_TILE_FULL;
    }
    for (h = healthbar_current_value; h < SMR_KING_MAX_HEALTH; ++h) {
      healthbar[h] = SMR_HEALTH_BAR_TILE_EMPTY;
    }
    WaitNVBlank(5);
    dmaCopyVram((u8 *)healthbar, (60 * 1024 + 64 + (32 - SMR_KING_MAX_HEALTH)) / 2, SMR_KING_MAX_HEALTH * 2);
  }
}

void smr_king_update_health_bar() {
  if (healthbar_current_value == king.health) {
    return;
  }
  healthbar_current_value = king.health;
  int h = 0;
  for (h = 0; h < healthbar_current_value; ++h) {
    healthbar[h] = SMR_HEALTH_BAR_TILE_FULL;
  }
  for (h = healthbar_current_value; h < SMR_KING_MAX_HEALTH; ++h) {
    healthbar[h] = SMR_HEALTH_BAR_TILE_EMPTY;
  }
  WaitForVBlank(); // useless or usefull?
  dmaCopyVram((u8 *)healthbar, (60 * 1024 + 64 + (32 - SMR_KING_MAX_HEALTH)) / 2, SMR_KING_MAX_HEALTH * 2);
}

void smr_king_set_cry() {
  king.state = SMR_KING_STATE_CRY;

  king.ticks = 0;
  king.cry_ticks = 0;
  king.king_upperpart->oamgraphics = &king_cry;
  king.king_upperpart->oamframeid = 0;
  king.king_upperpart->oamrefresh = 1;
  king.king_lowerpart->oamgraphics = &king_cry;
  king.king_lowerpart->oamframeid = 1;
  king.king_lowerpart->oamrefresh = 1;
}

void smr_king_set_idle() {
  king.state = SMR_KING_STATE_IDLE;
  king.ticks = 0;
  king.king_upperpart->oamgraphics = &king_attacks;
  king.king_upperpart->oamframeid = 0;
  king.king_upperpart->oamrefresh = 1;
  king.king_lowerpart->oamgraphics = &king_attacks;
  king.king_lowerpart->oamframeid = 1;
  king.king_lowerpart->oamrefresh = 1;
}

void smr_king_set_fall() {
  king.state = SMR_KING_STATE_FALL;
  king.y = king.ground_y + SMR_KING_FALL_SPEED;
  king.king_upperpart->oamgraphics = &king_attacks;
  king.king_upperpart->oamframeid = 4;
  king.king_upperpart->oamrefresh = 1;
  king.king_lowerpart->oamgraphics = &king_attacks;
  king.king_lowerpart->oamframeid = 5;
  king.king_lowerpart->oamrefresh = 1;
}

void smr_king_set_fall_for_good() {
  king.state = SMR_KING_STATE_FALL_FOR_GOOD;
  king.y = king.ground_y + SMR_KING_FALL_SPEED;
  king.king_upperpart->oamgraphics = &king_attacks;
  king.king_upperpart->oamframeid = 4;
  king.king_upperpart->oamrefresh = 1;
  king.king_lowerpart->oamgraphics = &king_attacks;
  king.king_lowerpart->oamframeid = 5;
  king.king_lowerpart->oamrefresh = 1;
}

void smr_king_set_horizontal_attack() {
  king.state = SMR_KING_STATE_HORIZONTAL_ATTACK;
  king.ticks = 0;
  king.ball_dx = 0;
  king.ball_speed = 1;
  king.king_upperpart->oamgraphics = &king_attacks;
  king.king_upperpart->oamframeid = 6;
  king.king_upperpart->oamrefresh = 1;
  king.king_lowerpart->oamgraphics = &king_attacks;
  king.king_lowerpart->oamframeid = 7;
  king.king_lowerpart->oamrefresh = 1;
}

void smr_king_set_circular_attack() {
  king.state = SMR_KING_STATE_CIRCULAR_ATTACK;
  king.ticks = 0;
  king.king_upperpart->oamgraphics = &king_attacks;
  king.king_upperpart->oamframeid = 2;
  king.king_upperpart->oamrefresh = 1;
  king.king_lowerpart->oamgraphics = &king_attacks;
  king.king_lowerpart->oamframeid = 3;
  king.king_lowerpart->oamrefresh = 1;
}

void smr_add_king(s16 x, s16 y) {
  dmaCopySpr16Vram(&king_scepter, SMR_VRAM_SPRITE_16_TILE_ADDRESS(0x1240));
  dmaCopySpr16Vram(&king_flail_ball, SMR_VRAM_SPRITE_16_TILE_ADDRESS(0x1260));
  dmaCopySpr16Vram(&king_flail_link, SMR_VRAM_SPRITE_16_TILE_ADDRESS(0x1280));

  king.health = SMR_KING_MAX_HEALTH;
  king.x = x;
  king.y = y;
  king.ticks = 0;
  king.ground_y = y;

  smr_king_set_idle();
}

void collides_king_with_arrows() {
  int a;
  for (a = 0; a < SMR_MAX_ARROWS; ++a) {
    smr_arrow *arrow = &arrows[a];
    if (arrow->tick >= SMR_ARROW_MAX_TICKS) {
      continue;
    }
    s16 arrow_tip_x = arrow->is_looking_left ? arrow->x : arrow->x + SMR_ARROW_WIDTH;
    s16 king_left = king.x;
    if (arrow_tip_x > king_left) {
      s16 king_right = king_left + SMR_KING_WIDTH;
      if (arrow_tip_x < king_right) {
        s16 arrow_tip_y = arrow->y + SMR_ARROW_HEIGHT / 2;
        s16 king_top = king.y;
        if (arrow_tip_y > king_top) {
          s16 king_bottom = king_top + SMR_KING_HEIGHT;
          if (arrow_tip_y < king_bottom) {
            king.hurt_ticks = SMR_KING_HURT_BY_ARROW_TICKS;
            smr_disable_arrow(arrow);
            if (king.health > 0) {
              --king.health;
            }
            return;
          }
        }
      }
    }
  }
}

void smr_king_collide(smr_character *character) {
  switch (king.state) {
  case SMR_KING_STATE_HORIZONTAL_ATTACK:
  case SMR_KING_STATE_CIRCULAR_ATTACK:
    break;
  default:
    return;
  }
  int f;
  s16 character_center_x = character->x + SMR_CHARACTER_WIDTH / 2;
  s16 ball_left = king.ball_x;
  if (character_center_x > ball_left && character_center_x < (ball_left + SMR_KING_BALL_WIDTH)) {
    s16 ball_top = king.ball_y;
    s16 character_center_y = character->y + SMR_CHARACTER_WIDTH / 2;
    if (character_center_y > ball_top && character_center_y < (ball_top + SMR_KING_BALL_HEIGHT)) {
      smr_character_kill(character);
      return;
    }
  }
}

void smr_update_king() {
  collides_king_with_arrows();

  switch (king.state) {
  case SMR_KING_STATE_IDLE: {
    if (king.ticks >= SMR_KING_IDLE_TICKS) {
      if (rand() & 1) {
        smr_king_set_horizontal_attack();
      } else {
        smr_king_set_circular_attack();
      }
    } else {
      ++king.ticks;
    }
    break;
  }
  case SMR_KING_STATE_FALL: {
    king.y += SMR_KING_FALL_SPEED;
    if (king.y >= SMR_WORLD_BOUNDS_BOTTOM) {
      king.y = -32;
      king.x = SMR_WORLD_BOUNDS_LEFT + (rand() % (SMR_WORLD_WIDTH - SMR_KING_WIDTH));
      king.state = SMR_KING_STATE_FALL_FROM_TOP;
    }
    break;
  }
  case SMR_KING_STATE_FALL_FROM_TOP: {
    king.y += SMR_KING_FALL_SPEED;
    if (king.y >= king.ground_y) {
      king.y = king.ground_y;
      if (king.health > 0) {
        smr_king_set_idle();
      } else {
        smr_king_set_cry();
      }
    }
    break;
  }
  case SMR_KING_STATE_HORIZONTAL_ATTACK: {
    ++king.ticks;
    if (king.ticks >= SMR_KING_HORIZONTAL_ATTACK_TICKS / 2) {
      king.ball_speed = -1;
    }
    if (king.ticks >= SMR_KING_HORIZONTAL_ATTACK_TICKS) {
      smr_king_set_fall();
    }
    break;
  }
  case SMR_KING_STATE_CIRCULAR_ATTACK: {
    ++king.ticks;
    if (king.ticks >= SMR_KING_CIRCULAR_ATTACK_TICKS) {
      smr_king_set_fall();
    }
    break;
  }
  case SMR_KING_STATE_CRY: {
    ++king.ticks;
    if (king.ticks >= SMR_KING_CRY_TICKS) {
      smr_king_set_fall_for_good();
    } else {
      ++king.cry_ticks;
      if (king.cry_ticks >= SMR_KING_CRY_TICKS_PER_FRAME) {
        king.cry_ticks = 0;
        king.king_upperpart->oamframeid += 2;
        if (king.king_upperpart->oamframeid > 6) {
          king.king_upperpart->oamframeid = 0;
        }
        king.king_upperpart->oamrefresh = 1;
        king.king_lowerpart->oamframeid = king.king_upperpart->oamframeid + 1;
        king.king_lowerpart->oamrefresh = 1;
      }
    }
    break;
  }
  case SMR_KING_STATE_FALL_FOR_GOOD: {
    king.y += SMR_KING_FALL_SPEED;
    if (king.y >= SMR_WORLD_BOUNDS_BOTTOM) {
      king.y = 0;
      king.x = -100;
      king.state = SMR_KING_STATE_OUT;
    }
    break;
  }
  case SMR_KING_STATE_OUT: {
    king.x = -100;
    king.y = 0;
    break;
  }
  }
  s16 king_x = king.x;
  s16 king_y = king.y;

  king.king_upperpart->oamx = king_x;
  king.king_upperpart->oamy = king_y;
  king.king_lowerpart->oamx = king_y < (SMR_WORLD_BOUNDS_BOTTOM - 32) ? king_x : -100;
  king.king_lowerpart->oamy = king_y + 32;

  if (king.hurt_ticks > 0) {
    --king.hurt_ticks;
  }
  u8 king_body_oamattribute = (king_x > SMR_WORLD_CENTER_X ? 0b01110000 : 0b00110000) | ((king.hurt_ticks % 32) > 16 ? SMR_RED_PALETTE_SPRITE_OAMATTRIBUTE_MASK : SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK);
  king.king_upperpart->oamattribute = king_body_oamattribute;
  king.king_lowerpart->oamattribute = king_body_oamattribute;

  // switch to draw flail according to current state
  switch (king.state) {
  case SMR_KING_STATE_HORIZONTAL_ATTACK: {
    if (king_x > SMR_WORLD_CENTER_X) {
      oamSetEx(king.king_scepter_oam_id, OBJ_SMALL, OBJ_SHOW);
      oamSetAttr(king.king_scepter_oam_id, king_x - 16, king_y + 32, SMR_VRAM_SPRITE_16_TILE_INDEX(36), 0b01110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK); // sprite 16x16 and priority 3
      king.ball_dx += king.ball_speed;
      king.ball_x = king_x - 32 - king.ball_dx;
      king.ball_y = king_y + 30;
      oamSetEx(king.king_flail_ball_oam_id, OBJ_SMALL, OBJ_SHOW);
      oamSetAttr(king.king_flail_ball_oam_id, king.ball_x, king.ball_y, SMR_VRAM_SPRITE_16_TILE_INDEX(38), 0b01110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK);
      int l;
      s16 links_dx_inc = king.ball_dx / SMR_KING_NB_LINKS;
      s16 links_dx = king_x - 28;
      for (l = 0; l < SMR_KING_NB_LINKS; ++l) {
        oamSetEx(king.king_flail_links_oam_ids[l], OBJ_SMALL, OBJ_SHOW);
        oamSetAttr(king.king_flail_links_oam_ids[l], links_dx, king.ball_y, SMR_VRAM_SPRITE_16_TILE_INDEX(40), 0b01110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK);
        links_dx -= links_dx_inc;
      }
    } else {
      oamSetEx(king.king_scepter_oam_id, OBJ_SMALL, OBJ_SHOW);
      oamSetAttr(king.king_scepter_oam_id, king_x + 32, king_y + 32, SMR_VRAM_SPRITE_16_TILE_INDEX(36), 0b00110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK); // sprite 16x16 and priority 3
      king.ball_dx += king.ball_speed;
      king.ball_x = king_x + 48 + king.ball_dx;
      king.ball_y = king_y + 30;
      oamSetEx(king.king_flail_ball_oam_id, OBJ_SMALL, OBJ_SHOW);
      oamSetAttr(king.king_flail_ball_oam_id, king.ball_x, king.ball_y, SMR_VRAM_SPRITE_16_TILE_INDEX(38), 0b00110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK);
      int l;
      s16 links_dx_inc = king.ball_dx / SMR_KING_NB_LINKS;
      s16 links_dx = king_x + 44;
      for (l = 0; l < SMR_KING_NB_LINKS; ++l) {
        oamSetEx(king.king_flail_links_oam_ids[l], OBJ_SMALL, OBJ_SHOW);
        oamSetAttr(king.king_flail_links_oam_ids[l], links_dx, king.ball_y, SMR_VRAM_SPRITE_16_TILE_INDEX(40), 0b00110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK);
        links_dx += links_dx_inc;
      }
    }
    break;
  }
  case SMR_KING_STATE_CIRCULAR_ATTACK: {
    oamSetEx(king.king_scepter_oam_id, OBJ_SMALL, OBJ_HIDE);
    oamSetEx(king.king_flail_ball_oam_id, OBJ_SMALL, OBJ_HIDE);

    u16 path_index = (king.ticks + 64) % SMR_LOOKUP_CIRCULAR_PATH_NB_POINTS;
    s16 path_x = circular_path[path_index].x;
    s16 path_y = circular_path[path_index].y;
    s16 anchor_x = king_x + (king_x < SMR_WORLD_CENTER_X ? 25 - 8 : 6 - 8);
    s16 anchor_y = king_y + 10 - 8;
    s16 ball_x = anchor_x + (path_x >> 2);
    s16 ball_y = anchor_y + (path_y >> 2);
    king.ball_x = ball_x;
    king.ball_y = ball_y;
    oamSetEx(king.king_flail_ball_oam_id, OBJ_SMALL, OBJ_SHOW);
    oamSetAttr(king.king_flail_ball_oam_id, ball_x, ball_y, SMR_VRAM_SPRITE_16_TILE_INDEX(38), 0b00110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK);
    s16 px = path_x << 2;
    s16 py = path_y << 2;
    u16 first_link_gfxoffset;
    int l;
    for (l = 0; l < SMR_KING_NB_LINKS; ++l) {
      oamSetEx(king.king_flail_links_oam_ids[l], OBJ_SMALL, OBJ_SHOW);
      oamSetAttr(king.king_flail_links_oam_ids[l], anchor_x + (px >> SMR_LOOKUP_CIRCULAR_PATH_RADIUS_DIVIDE_SHIFT), anchor_y + (py >> SMR_LOOKUP_CIRCULAR_PATH_RADIUS_DIVIDE_SHIFT), SMR_VRAM_SPRITE_16_TILE_INDEX(40), 0b00110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK);
      px += path_x << 3;
      py += path_y << 3;
    }
    break;
  }
  default: {
    oamSetEx(king.king_scepter_oam_id, OBJ_SMALL, OBJ_HIDE);
    oamSetEx(king.king_flail_ball_oam_id, OBJ_SMALL, OBJ_HIDE);
    int l;
    for (l = 0; l < SMR_KING_NB_LINKS; ++l) {
      oamSetEx(king.king_flail_links_oam_ids[l], OBJ_SMALL, OBJ_HIDE);
    }
    break;
  }
  }

  oamDynamic32Draw(king.king_upperpart - oambuffer);
  oamDynamic32Draw(king.king_lowerpart - oambuffer);
}