#ifndef SMR_WINDOW_H
#define SMR_WINDOW_H

extern void smr_show_fullscreen_window();
extern void smr_hide_fullscreen_window();

#endif