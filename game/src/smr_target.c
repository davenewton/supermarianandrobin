#include "smr_target.h"
#include "smr_animation.h"
#include "smr_character.h"
#include "smr_oam.h"
#include "smr_palette.h"
#include "smr_world.h"

extern smr_animation_rom_data balloon_fly;
extern smr_animation_rom_data balloon_pop;
extern smr_animation_rom_data target_fly;

u16 target_hits;

smr_target targets[SMR_MAX_TARGET] = {
    {.balloon_sprite = SMR_OAMBUFFER_ID_TO_PTR(40), .target_sprite = SMR_OAMBUFFER_ID_TO_PTR(41)},
    {.balloon_sprite = SMR_OAMBUFFER_ID_TO_PTR(42), .target_sprite = SMR_OAMBUFFER_ID_TO_PTR(43)},
    {.balloon_sprite = SMR_OAMBUFFER_ID_TO_PTR(44), .target_sprite = SMR_OAMBUFFER_ID_TO_PTR(45)},
    {.balloon_sprite = SMR_OAMBUFFER_ID_TO_PTR(46), .target_sprite = SMR_OAMBUFFER_ID_TO_PTR(47)}};

void smr_reset_target(smr_target *target) {
  target->target_sprite->oamgraphics = target_fly.data;
  target->target_sprite->oamframeid = 0;
  target->target_sprite->oamrefresh = 1;
  target->balloon_sprite->oamgraphics = balloon_fly.data;
  target->balloon_sprite->oamframeid = 0;
  target->balloon_sprite->oamrefresh = 1;
  target->x = -100;
  target->y = 0;
  target->state = SMR_TARGET_STATE_LAUNCHABLE;
  target->arrow_stucked = NULL;
}

void smr_reset_targets() {
  int t;
  target_hits = 0;
  for (t = 0; t < SMR_MAX_TARGET; ++t) {
    smr_reset_target(&targets[t]);
  }
}

void smr_launch_one_target(bool is_looking_left) {
  int t;
  for (t = 0; t < SMR_MAX_TARGET; ++t) {
    smr_target *target = &targets[t];
    if (target->state == SMR_TARGET_STATE_LAUNCHABLE) {

      target->is_looking_left = is_looking_left;
      u8 oamattribute = (is_looking_left ? 0b01110000 : 0b00110000) | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK; // flip if going left, palette 0 of sprite and sprite 16x16 and priority 3
      {
        t_sprites* balloon_sprite = target->balloon_sprite;
        balloon_sprite->oamx = -SMR_BALLOON_WIDTH;
        balloon_sprite->oamy = -SMR_BALLOON_HEIGHT;
        balloon_sprite->oamgraphics = balloon_fly.data;
        balloon_sprite->oamattribute = oamattribute;
        balloon_sprite->oamrefresh = 1;
        balloon_sprite->oamframeid = 0;
      }

      {
        t_sprites*  target_sprite = target->target_sprite;
        target_sprite->oamx = -SMR_TARGET_WIDTH;
        target_sprite->oamy = -SMR_TARGET_HEIGHT;
        target_sprite->oamgraphics = target_fly.data;
        target_sprite->oamattribute = oamattribute;
        target_sprite->oamrefresh = 1;
        target_sprite->oamframeid = 0;
      }
      target->x = SMR_WORLD_BOUNDS_LEFT + SMR_TARGET_WIDTH + rand() % (SMR_WORLD_BOUNDS_RIGHT - SMR_WORLD_BOUNDS_LEFT - SMR_TARGET_WIDTH * 2);
      target->y = SMR_WORLD_BOUNDS_TOP - SMR_TARGET_HEIGHT;
      target->state = SMR_TARGET_STATE_FLY;
      return;
    }
  }
}

void smr_update_target_fly(smr_target *target) {
  s16 x = target->x;
  s16 y = target->y;
  y += SMR_TARGET_FLY_SPEED_Y;
  target->y = y;
  t_sprites* target_sprite = target->target_sprite;
  s16 target_sprite_x = x + SMR_TARGET_SPRITE_OFFSET_X;
  s16 target_sprite_y = target_sprite->oamy = y + SMR_TARGET_SPRITE_OFFSET_Y;
  target_sprite->oamx = target_sprite_y < SMR_WORLD_BOUNDS_BOTTOM ? target_sprite_x : -100; // TODO find better technique to hide sprite
  oamDynamic32Draw(target_sprite - oambuffer);

  t_sprites* balloon_sprite = target->balloon_sprite;
  s16 balloon_sprite_y = balloon_sprite->oamy = target_sprite_y - SMR_BALLOON_HEIGHT + SMR_BALLOON_SPRITE_OFFSET_Y;
  balloon_sprite->oamx = balloon_sprite_y > (SMR_WORLD_BOUNDS_TOP - SMR_BALLOON_HEIGHT) ? target_sprite_x + SMR_BALLOON_SPRITE_OFFSET_X : -100; // TODO find better technique to hide sprite
  oamDynamic32Draw(balloon_sprite - oambuffer);

  if (y >= (SMR_WORLD_BOUNDS_BOTTOM + SMR_TARGET_HEIGHT)) {
    target->state = SMR_TARGET_STATE_LAUNCHABLE;
  } else {
    // collide with arrows
    int a;
    for (a = 0; a < SMR_MAX_ARROWS; ++a) {
      smr_arrow *arrow = &arrows[a];
      if (arrow->tick >= SMR_ARROW_MAX_TICKS) {
        continue;
      }
      bool is_arrow_looking_left = arrow->is_looking_left;
      s16 arrow_tip_x = is_arrow_looking_left ? arrow->x : arrow->x + SMR_ARROW_WIDTH;
      if (target->is_looking_left != is_arrow_looking_left && arrow_tip_x > x && arrow_tip_x < (x + SMR_TARGET_WIDTH)) {
        s16 arrow_tip_y = arrow->y + SMR_ARROW_HEIGHT / 2;
        if (arrow_tip_y > y && arrow_tip_y < (y + SMR_TARGET_HEIGHT)) {
          ++target_hits;
          target->state = SMR_TARGET_STATE_HIT_BY_ARROW;
          target->arrow_stucked = arrow;
          break;
        }
      }
    }
  }
}

void smr_update_target_hit_by_arrow(smr_target *target) {
  smr_arrow* arrow_stucked = target->arrow_stucked;
  if (arrow_stucked) {
    if (arrow_stucked->is_looking_left) { // TODO macro to detect if arrow is looking left macro or function
      target->x = arrow_stucked->x - SMR_TARGET_WIDTH / 2;
    } else {
      target->x = arrow_stucked->x + SMR_TARGET_WIDTH / 2;
    }
    t_sprites* target_sprite = target->target_sprite;
    s16 target_sprite_x = target->x + SMR_TARGET_SPRITE_OFFSET_X;
    target_sprite->oamx = target_sprite_x;
    oamDynamic32Draw(target_sprite - oambuffer);

    t_sprites* balloon_sprite = target->balloon_sprite;
    balloon_sprite->oamx = target_sprite_x + SMR_BALLOON_SPRITE_OFFSET_X;
    oamDynamic32Draw(balloon_sprite - oambuffer);

    if (target_sprite_x > SMR_WORLD_BOUNDS_RIGHT || target_sprite_x < (SMR_WORLD_BOUNDS_LEFT + SMR_BALLOON_SPRITE_OFFSET_X - SMR_BALLOON_WIDTH)) {
      target->arrow_stucked = NULL;
      target->state = SMR_TARGET_STATE_LAUNCHABLE;
    }
    if (target->arrow_stucked->tick >= SMR_ARROW_MAX_TICKS) {
      target->arrow_stucked = NULL;
    }
  } else {
    smr_streamed_animation_start(&target->balloon_animation, &balloon_pop, target->balloon_sprite);
    oamDynamic32Draw(target->target_sprite - oambuffer);
    oamDynamic32Draw(target->balloon_sprite - oambuffer);
    target->state = SMR_TARGET_STATE_POP;
  }
}

void smr_update_target_pop(smr_target *target) {
  t_sprites* balloon_sprite =  target->balloon_sprite;
  smr_streamed_animation_update(&target->balloon_animation, balloon_sprite);
  if (target->balloon_animation.loop_count > 0) {
    target->state = SMR_TARGET_STATE_FALL;
    balloon_sprite->oamgraphics = balloon_fly.data;
    balloon_sprite->oamrefresh = 1;
    balloon_sprite->oamframeid = 0;
    balloon_sprite->oamx = -100; // TODO find better technique to hide sprite
  }
  oamDynamic32Draw(target->target_sprite - oambuffer);
  oamDynamic32Draw(balloon_sprite - oambuffer);
}

void smr_update_target_fall(smr_target *target) {
  s16 y = target->y;
  y += SMR_TARGET_FALL_SPEED_Y;
  target->y = y;
  t_sprites* target_sprite = target->target_sprite;
  target->target_sprite->oamy = y + SMR_TARGET_SPRITE_OFFSET_Y;
  if (y >= (SMR_WORLD_BOUNDS_BOTTOM)) {
    smr_reset_target(target);
  }
  oamDynamic32Draw(target_sprite - oambuffer);
  oamDynamic32Draw(target->balloon_sprite - oambuffer);
}

void smr_update_target(smr_target *target) {
  switch (target->state) {
  case SMR_TARGET_STATE_LAUNCHABLE:
    break;
  case SMR_TARGET_STATE_FLY:
    smr_update_target_fly(target);
    break;
  case SMR_TARGET_STATE_HIT_BY_ARROW:
    smr_update_target_hit_by_arrow(target);
    break;
  case SMR_TARGET_STATE_POP:
    smr_update_target_pop(target);
    break;
  case SMR_TARGET_STATE_FALL:
    smr_update_target_fall(target);
    break;
  }
}

void smr_update_targets() {
  int t;
  for (t = 0; t < SMR_MAX_TARGET; ++t) {
    smr_update_target(&targets[t]);
  }
}