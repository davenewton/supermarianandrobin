#ifndef SMR_OAM_H
#define SMR_OAM_H

//usefull to find all oambuffer usage in all source files
#define SMR_OAMBUFFER_ID_TO_PTR(id) (&oambuffer[id])

//usefull to find all static oam usage in all source files
#define SMR_OAM_ID(id) (id * 4)


//usefull to find all vram/tile usage in all source files
#define SMR_VRAM_SPRITE_16_TILE_INDEX(tile_index) (tile_index)
#define SMR_VRAM_SPRITE_16_TILE_ADDRESS(address) (address)
#define SMR_VRAM_SPRITE_32_TILE_ADDRESS(address) (address)
#define SMR_VRAM_SPRITE_32_ADDRESS (0)
#define SMR_VRAM_SPRITE_16_ADDRESS (0x1000)



#endif