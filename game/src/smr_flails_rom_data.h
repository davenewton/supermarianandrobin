#ifndef SMR_FLAIL_ROM_DATA
#define SMR_FLAIL_ROM_DATA

#include <stdint.h>

typedef struct {
    int16_t anchor_x __attribute__((packed));
    int16_t anchor_y __attribute__((packed));
} __attribute__((packed)) smr_flail_rom_data;

typedef struct  {
    uint16_t nb_flails __attribute__((packed));
    smr_flail_rom_data flails[];
} __attribute__((packed)) smr_flails_rom_data;

#endif