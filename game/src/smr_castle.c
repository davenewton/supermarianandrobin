#include "smr_castle.h"
#include "smr_axe.h"
#include "smr_boar.h"
#include "smr_castle_rom_data.h"
#include "smr_character.h"
#include "smr_crossbowman.h"
#include "smr_door.h"
#include "smr_flail.h"
#include "smr_guard.h"
#include "smr_oam.h"
#include "smr_pad.h"
#include "smr_palette.h"
#include "smr_platform_rom_data.h"
#include "smr_sound.h"
#include "smr_text.h"
#include "smr_treasure_room.h"
#include "smr_window.h"
#include "smr_world.h"

extern u8 castle_tileset, castle_tileset_end;

// Data in ROM
extern smr_castle_level_rom_data castle_levels[SMR_CASTLE_NB_LEVELS];

int current_castle_level = 0;

typedef enum {
  SMR_CASTLE_SCENE_STATE_FIGHT = 0,
  SMR_CASTLE_SCENE_STATE_ESCAPE = 1
} smr_castle_scene_state;

smr_adventure_outcome smr_scene_castle_level(smr_castle_level_rom_data *level) {

  smr_castle_scene_state scene_state = SMR_CASTLE_SCENE_STATE_FIGHT;

  // reinit
  setScreenOff();
  oamClear(0, 128);
  dmaClearVram();

  // Init layers with tiles
  bgInitTileSet(0, &castle_tileset, &smr_palette, 0,
                (&castle_tileset_end - &castle_tileset), 16 * 2, BG_16COLORS,
                32 * 1024 / 2);
  bgInitTileSet(1, &castle_tileset, &smr_palette, 0,
                (&castle_tileset_end - &castle_tileset), 16 * 2, BG_16COLORS,
                32 * 1024 / 2);
  smr_text_init_bg3_tileset();
  bgInitMapSet(0, level->bg1,
               level->bg1_size, SC_32x32,
               60 * 1024 / 2);
  bgInitMapSet(1, level->bg2,
               level->bg2_size, SC_32x32,
               62 * 1024 / 2);

  smr_text_init_bg3_map();
  smr_text_clear();

  // Now Put in 16 color mode and enable backgrounds
  setMode(BG_MODE1, BG3_MODE1_PRIORITY_HIGH);
  bgSetEnable(0);
  bgSetEnable(1);
  bgSetEnable(2);

  // init world
  world.platforms = level->platforms;
  world.wrap_x = true;
  world.wrap_y = false;
  world.shooting_allowed = true;

  // init sprites
  dmaCopyCGram(&smr_palette, (128 + SMR_PALETTE_SPRITE_INDEX * 16), 16 * 2);
  oamInitDynamicSprite(SMR_VRAM_SPRITE_32_ADDRESS, SMR_VRAM_SPRITE_16_ADDRESS, 0, 0, OBJ_SIZE16_L32);

  // characters
  smr_character_reset_body(&characters[SMR_CHARACTER_TYPE_ROBIN], level->header->robinX, level->header->robinY);
  smr_character_reset_sprite(&characters[SMR_CHARACTER_TYPE_ROBIN]);

  // marian
  smr_character_reset_body(&characters[SMR_CHARACTER_TYPE_MARIAN], level->header->marianX, level->header->marianY);
  smr_character_reset_sprite(&characters[SMR_CHARACTER_TYPE_MARIAN]);

  // arrows
  smr_reset_arrows();

  // door
  smr_reset_door(level->header->doorX, level->header->doorY, level->header->keyX, level->header->keyY);

  // flails
  smr_reset_flails();
  smr_add_flails(level->flails);

  // boars
  smr_reset_boars();
  smr_add_boars(level->boars);

  // axes
  smr_reset_axes();
  smr_add_axes(level->axes);

  // crossbowman
  smr_reset_crossbowman(level->crossbowman);

  // guard
  smr_reset_guard(level->guard);

  // Scroll
  bgSetScroll(0, 0, -1);
  bgSetScroll(1, 0, -1);
  bgSetScroll(2, 0, -1);

  // Screen activated
  setScreenOn();

  while (1) {
    smr_update_pads();

    int ch;
    for (ch = 0; ch < SMR_CHARACTER_TYPE_COUNT; ++ch) {
      smr_character *character = &characters[ch];
      if (character->active) {
        smr_character_control_with_pad(character, ch);
        smr_character_update_body(character);
        smr_character_update_sprite(character);
      }
    }

    smr_update_crossbowman();
    smr_update_guard();

    smr_update_arrows();
    smr_door_update();
    smr_update_flails();
    smr_update_boars();
    smr_update_axes();

    for (ch = 0; ch < SMR_CHARACTER_TYPE_COUNT; ++ch) {
      smr_character *character = &characters[ch];
      if (character->active) {
        smr_flails_collide(character);
        smr_boars_collide(character);
        smr_axes_collide(character);
      }
    }

    spcProcess();
    oamInitDynamicSpriteEndFrame();
    WaitForVBlank();
    oamVramQueueUpdate();

    switch (scene_state) {
    case SMR_CASTLE_SCENE_STATE_FIGHT: {
      if ((guard.state == SMR_GUARD_STATE_INACTIVE || guard.state == SMR_GUARD_STATE_DEAD_AND_LOVING_IT) && (crossbowman.state == SMR_CROSSBOWMAN_STATE_INACTIVE || crossbowman.state == SMR_CROSSBOWMAN_STATE_DEAD_AND_LOVING_IT)) {
        smr_reveal_key();
        scene_state = SMR_CASTLE_SCENE_STATE_ESCAPE;
      }
      break;
    }
    case SMR_CASTLE_SCENE_STATE_ESCAPE: {
      break;
    }
    }

    int nb_active = 0;
    int nb_out = 0;
    int nb_dead = 0;
    for (ch = 0; ch < SMR_CHARACTER_TYPE_COUNT; ++ch) {
      smr_character *character = &characters[ch];
      if (character->active) {
        ++nb_active;
        switch (character->state) {
        case SMR_CHARACTER_STATE_OUT:
          ++nb_out;
          break;
        case SMR_CHARACTER_STATE_DEAD_AND_LOVING_IT:
          ++nb_dead;
          break;
        default:
          break;
        }
      }
    }
    if ((nb_out + nb_dead) == nb_active) {
      return nb_out >= 1 ? SMR_ADVENTURE_OUTCOME_WIN : SMR_ADVENTURE_OUTCOME_LOSE;
    }
  }
}

smr_adventure_outcome smr_scene_castle() {
  // Music
  spcStop();
  smr_play_music(MOD_CASTLE);

  smr_adventure_outcome outcome;
  for (; current_castle_level < SMR_CASTLE_NB_LEVELS; ++current_castle_level) {
    outcome = smr_scene_castle_level(&castle_levels[current_castle_level]);
    setFadeEffect(FADE_OUT);
    if (SMR_ADVENTURE_OUTCOME_LOSE == outcome) {
      return outcome;
    }
  }
  outcome = smr_scene_treasure_room();
  setFadeEffect(FADE_OUT);
  return outcome;
}