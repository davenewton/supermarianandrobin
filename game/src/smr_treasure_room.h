#ifndef SMR_TREASURE_ROOM__H
#define SMR_TREASURE_ROOM__H

#include "smr_adventure.h"

extern smr_adventure_outcome smr_scene_treasure_room();

#endif