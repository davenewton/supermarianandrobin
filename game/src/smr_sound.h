#ifndef SMR_SOUND_H
#define SMR_SOUND_H

#include <snes.h>
#include "build/soundbank.h"

typedef enum {
    SMR_SOUND_COLLECT=0,
    SMR_SOUND_JUMP,
    SMR_SOUND_SELECT,
    SMR_SOUND_START,
} smr_sounds;

#define SMR_NB_SOUNDS (4)

extern void smr_sound_init();
extern void smr_play_music(u8 music);

 // (u16 pitch,u16 sfxIndex, u8 volpan); pitch=1=4Khz , 2=8khz, 8=32Khz
#define smr_play_sound(sound) spcEffect(3,sound,15*16+8)

#endif