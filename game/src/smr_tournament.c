#include "smr_tournament.h"
#include "build/soundbank.h"
#include "smr_character.h"
#include "smr_pad.h"
#include "smr_palette.h"
#include "smr_platform_rom_data.h"
#include "smr_target.h"
#include "smr_text.h"
#include "smr_tournament_rom_data.h"
#include "smr_window.h"
#include "smr_world.h"
#include "smr_oam.h"
#include "smr_sound.h"

extern u8 tournament_tileset, tournament_tileset_end;
extern u8 tournament_bg1, tournament_bg1_end;
extern u8 tournament_bg2, tournament_bg2_end;

// Data in ROM
extern smr_tournament_rom_data_header tournament_header_data;
extern smr_platforms_rom_data tournament_platforms_data;

typedef enum {
  SMR_TOURNAMENT_STATE_PLAY = 0,
  SMR_TOURNAMENT_STATE_SCORE = 1
} smr_tournament_state;

#define SMR_TICKS_BEFORE_TARGET_LAUNCH (90)
#define SMR_TOURNAMENT_DURATION (60 * 60)

void smr_scene_tournament() {
  // reinit
  setScreenOff();
  oamClear(0, 128);
  dmaClearVram();

  // Init layers with tiles
  bgInitTileSet(0, &tournament_tileset, &smr_palette, 0,
                (&tournament_tileset_end - &tournament_tileset), 16 * 2, BG_16COLORS,
                32 * 1024 / 2);
  bgInitTileSet(1, &tournament_tileset, &smr_palette, 0,
                (&tournament_tileset_end - &tournament_tileset), 16 * 2, BG_16COLORS,
                32 * 1024 / 2);
  smr_text_init_bg3_tileset();
  bgInitMapSet(0, &tournament_bg1,
               &tournament_bg1_end - &tournament_bg1, SC_32x32,
               60 * 1024 / 2);
  bgInitMapSet(1, &tournament_bg2,
               &tournament_bg2_end - &tournament_bg2, SC_32x32,
               62 * 1024 / 2);

  smr_text_init_bg3_map();
  smr_text_clear();

  // Now Put in 16 color mode and enable backgrounds
  setMode(BG_MODE1, BG3_MODE1_PRIORITY_HIGH);
  bgSetEnable(0);
  bgSetEnable(1);
  bgSetEnable(2);

  // init world
  world.platforms = &tournament_platforms_data;
  world.wrap_x = true;
  world.wrap_y = false;
  world.shooting_allowed = true;

  // init sprites
  dmaCopyCGram(&smr_palette, (128 + SMR_PALETTE_SPRITE_INDEX * 16), 16 * 2);
  oamInitDynamicSprite(SMR_VRAM_SPRITE_32_ADDRESS, SMR_VRAM_SPRITE_16_ADDRESS, 0, 0, OBJ_SIZE16_L32); // TODO: comprendre les valeurs magiques tirées d'un exemple de pvsneslib

  // robin
  smr_character_reset_body(&characters[SMR_CHARACTER_TYPE_ROBIN], tournament_header_data.robinX, tournament_header_data.robinY);
  smr_character_reset_sprite(&characters[SMR_CHARACTER_TYPE_ROBIN]);

  // marian
  smr_character_reset_body(&characters[SMR_CHARACTER_TYPE_MARIAN], tournament_header_data.marianX, tournament_header_data.marianY);
  smr_character_reset_sprite(&characters[SMR_CHARACTER_TYPE_MARIAN]);

  // arrows
  smr_reset_arrows();

  // targets
  smr_reset_targets();

  // Scroll
  bgSetScroll(0, 0, 0);
  bgSetScroll(1, 0, 0);
  bgSetScroll(2, 0, 0);

  // Music
  smr_play_music(MOD_TOURNAMENT);

  // Screen activated
  setScreenOn();

  smr_tournament_state state = SMR_TOURNAMENT_STATE_PLAY;
  u16 tick = 0;
  u16 nextTargetTickCountDown = SMR_TICKS_BEFORE_TARGET_LAUNCH;
  while (1) {
    smr_update_pads();

    int ch;
    for (ch = 0; ch < SMR_CHARACTER_TYPE_COUNT; ++ch) {
      smr_character *character = &characters[ch];
      if(character->active) {
        smr_character_control_with_pad(character, ch);
        smr_character_update_body(character);
        smr_character_update_sprite(character);
      }
    }
    smr_update_arrows();
    smr_update_targets();

    spcProcess();
    oamInitDynamicSpriteEndFrame();
    WaitForVBlank();
    oamVramQueueUpdate();

    switch (state) {
    case SMR_TOURNAMENT_STATE_PLAY:
      if (nextTargetTickCountDown == 0) {
        smr_launch_one_target((rand() & 1) ? true : false);
        nextTargetTickCountDown = SMR_TICKS_BEFORE_TARGET_LAUNCH;
      } else {
        --nextTargetTickCountDown;
      }
      ++tick;
      int nb_active = 0;
      int nb_dead = 0;
      int ch;
      for (ch = 0; ch < SMR_CHARACTER_TYPE_COUNT; ++ch) {
        smr_character *character = &characters[ch];
        if (character->active) {
          ++nb_active;
          if (character->state == SMR_CHARACTER_STATE_DEAD_AND_LOVING_IT) {
            ++nb_dead;
          }
        }
      }
      if (nb_dead == nb_active || tick > SMR_TOURNAMENT_DURATION) {
        state = SMR_TOURNAMENT_STATE_SCORE;
        tick = 0;
        world.shooting_allowed = false;
      }
      break;
    case SMR_TOURNAMENT_STATE_SCORE:
      if (tick < 121) {
        ++tick;
      } else if (smr_test_and_reset_if_pressed(0, KEY_START) || smr_test_and_reset_if_pressed(1, KEY_START)) {
        smr_hide_fullscreen_window();
        return;
      }
      if (tick == 1) {
        smr_text_clear();
        smr_text_print(10, 6, 24, 1, SMR_TEXT_MASK_WHITE, "RESULTS");
        WaitForVBlank();
        smr_show_fullscreen_window();
        smr_text_update_vram();
      } else if (tick == 60) {
        smr_text_print(10, 11, 24, 1, SMR_TEXT_MASK_WHITE, "HITS ");
        smr_text_print_number(17, 11, 24, 1, SMR_TEXT_MASK_WHITE, target_hits, SMR_TEXT_NUMBER_FORMAT_2_DIGITS);
        WaitForVBlank();
        smr_text_update_vram();
      } else if (tick == 120) {
        smr_text_print(10, 20, 24, 28, SMR_TEXT_MASK_WHITE, "PRESS START");
        WaitForVBlank();
        smr_text_update_vram();
      }
      break;
    }
  }
}