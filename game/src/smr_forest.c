#include "smr_forest.h"
#include "build/soundbank.h"
#include "smr_arrow.h"
#include "smr_character.h"
#include "smr_forest_rom_data.h"
#include "smr_oam.h"
#include "smr_pad.h"
#include "smr_palette.h"
#include "smr_platform_rom_data.h"
#include "smr_text.h"
#include "smr_window.h"
#include "smr_world.h"
#include "smr_sound.h"

extern u8 forest_tileset, forest_tileset_end;
extern u8 forest_bg1, forest_bg1_end;
extern u8 forest_bg2, forest_bg2_end;

// Data in ROM
extern smr_forest_rom_data_header forest_header_data;
extern smr_platforms_rom_data forest_platforms_data;

typedef enum {
  SMR_FOREST_STATE_PLAY = 0,
  SMR_FOREST_STATE_WAIT_GHOSTS = 1,
  SMR_FOREST_STATE_SCORE = 2
} smr_forest_state;

#define SMR_FOREST_SCORE_TO_WIN (3)

smr_forest_scene_outcome smr_scene_forest() {

  // reinit
  setScreenOff();
  oamClear(0, 128);
  dmaClearVram();

  // Init layers with tiles
  bgInitTileSet(0, &forest_tileset, &smr_palette, 0,
                (&forest_tileset_end - &forest_tileset), 16 * 2, BG_16COLORS,
                32 * 1024 / 2);
  bgInitTileSet(1, &forest_tileset, &smr_palette, 0,
                (&forest_tileset_end - &forest_tileset), 16 * 2, BG_16COLORS,
                32 * 1024 / 2);
  smr_text_init_bg3_tileset();
  bgInitMapSet(0, &forest_bg1,
               &forest_bg1_end - &forest_bg1, SC_32x32,
               60 * 1024 / 2);
  bgInitMapSet(1, &forest_bg2,
               &forest_bg2_end - &forest_bg2, SC_32x32,
               62 * 1024 / 2);

  smr_text_init_bg3_map();
  smr_text_clear();

  // Now Put in 16 color mode and enable backgrounds
  setMode(BG_MODE1, BG3_MODE1_PRIORITY_HIGH);
  bgSetEnable(0);
  bgSetEnable(1);
  bgSetEnable(2);

  // init world
  world.platforms = &forest_platforms_data;
  world.wrap_x = true;
  world.wrap_y = true;
  world.shooting_allowed = true;

  // init sprites
  dmaCopyCGram(&smr_palette, (128 + SMR_PALETTE_SPRITE_INDEX * 16), 16 * 2);
  oamInitDynamicSprite(SMR_VRAM_SPRITE_32_ADDRESS, SMR_VRAM_SPRITE_16_ADDRESS, 0, 0, OBJ_SIZE16_L32); // TODO: comprendre les valeurs magiques tirées d'un exemple de pvsneslib

  // marian
  smr_character *marian = &characters[SMR_CHARACTER_TYPE_MARIAN];
  smr_character_reset_body(marian, forest_header_data.marianX, forest_header_data.marianY);
  smr_character_reset_sprite(marian);

  // robin
  smr_character *robin = &characters[SMR_CHARACTER_TYPE_ROBIN];
  smr_character_reset_body(robin, forest_header_data.robinX, forest_header_data.robinY);
  smr_character_reset_sprite(robin);

  // arrows
  smr_reset_arrows();

  // Scroll
  bgSetScroll(0, 0, 0);
  bgSetScroll(1, 0, 0);
  bgSetScroll(2, 0, 0);

  // Music
  smr_play_music(MOD_FOREST);

  // Screen activated
  setScreenOn();

  smr_forest_state state = SMR_FOREST_STATE_PLAY;
  u16 tick = 0;
  u8 robin_score = 0;
  u8 marian_score = 0;
  char *round_result_comment = "";
  while (1) {
    smr_update_pads();
    int ch;
    for (ch = 0; ch < SMR_CHARACTER_TYPE_COUNT; ++ch) {
      smr_character *character = &characters[ch];
      if (character->active) {
        smr_character_control_with_pad(character, ch);
        smr_character_update_body(character);
        smr_character_update_sprite(character);
      }
    }

    smr_update_arrows();

    spcProcess();
    oamInitDynamicSpriteEndFrame();
    WaitForVBlank();
    oamVramQueueUpdate();

    switch (state) {
    case SMR_FOREST_STATE_PLAY:
      if (robin->state == SMR_CHARACTER_STATE_DEAD_AND_LOVING_IT || marian->state == SMR_CHARACTER_STATE_DEAD_AND_LOVING_IT) {
        smr_reset_arrows();
        world.shooting_allowed = false;
        state = SMR_FOREST_STATE_WAIT_GHOSTS;
      }
      break;
    case SMR_FOREST_STATE_WAIT_GHOSTS:
      if (robin->state == SMR_CHARACTER_STATE_DEAD_AND_LOVING_IT && marian->state == SMR_CHARACTER_STATE_DEAD_AND_LOVING_IT) {
        tick = 0;
        round_result_comment = "DRAW !";
        state = SMR_FOREST_STATE_SCORE;
      } else if (robin->state == SMR_CHARACTER_STATE_DEAD_AND_LOVING_IT && marian->state != SMR_CHARACTER_STATE_DEAD) {
        ++marian_score;
        tick = 0;
        round_result_comment = "MARIAN WIN !";
        state = SMR_FOREST_STATE_SCORE;
      } else if (marian->state == SMR_CHARACTER_STATE_DEAD_AND_LOVING_IT && robin->state != SMR_CHARACTER_STATE_DEAD) {
        ++robin_score;
        tick = 0;
        round_result_comment = "ROBIN WIN !";
        state = SMR_FOREST_STATE_SCORE;
      }
      break;
    case SMR_FOREST_STATE_SCORE:
      if (tick < 181) {
        ++tick;
      } else if (smr_test_and_reset_if_pressed(0, KEY_START) || smr_test_and_reset_if_pressed(1, KEY_START)) {
        world.shooting_allowed = true;

        int ch;
        for (ch = 0; ch < SMR_CHARACTER_TYPE_COUNT; ++ch) {
          smr_character *character = &characters[ch];
          if (character->active) {
            smr_character_set_animation(character, SMR_CHARACTER_ANIMATION_IDLE);
            if (character->type == SMR_CHARACTER_TYPE_MARIAN) {
              smr_character_reset_body(character, forest_header_data.marianX, forest_header_data.marianY);
            } else if (character->type == SMR_CHARACTER_TYPE_ROBIN) {
              smr_character_reset_body(character, forest_header_data.robinX, forest_header_data.robinY);
            }
            smr_character_reset_sprite(character);
          }
        }

        smr_text_clear();
        WaitForVBlank();
        smr_hide_fullscreen_window();
        smr_text_update_vram();
        if (robin_score >= SMR_FOREST_SCORE_TO_WIN) {
          return SMR_FOREST_SCENE_OUTCOME_ROBIN_S_VICTORY;
        } else if (marian_score >= SMR_FOREST_SCORE_TO_WIN) {
          return SMR_FOREST_SCENE_OUTCOME_MARIAN_S_VICTORY;
        } else {
          state = SMR_FOREST_STATE_PLAY;
        }
      }
      if (tick == 1) {
        smr_text_clear();
        smr_text_print(10, 6, 24, 1, SMR_TEXT_MASK_WHITE, round_result_comment);
        WaitForVBlank();
        smr_show_fullscreen_window();
        smr_text_update_vram();
      } else if (tick == 60) {
        smr_text_print(10, 11, 24, 1, SMR_TEXT_MASK_WHITE, "MARIAN ");
        smr_text_print_number(17, 11, 24, 1, SMR_TEXT_MASK_WHITE, marian_score, SMR_TEXT_NUMBER_FORMAT_1_DIGIT);
        WaitForVBlank();
        smr_text_update_vram();
      } else if (tick == 120) {
        smr_text_print(10, 15, 24, 1, SMR_TEXT_MASK_WHITE, "ROBIN ");
        smr_text_print_number(17, 15, 24, 1, SMR_TEXT_MASK_WHITE, robin_score, SMR_TEXT_NUMBER_FORMAT_1_DIGIT);
        WaitForVBlank();
        smr_text_update_vram();
      } else if (tick == 180) {
        smr_text_print(10, 20, 24, 28, SMR_TEXT_MASK_WHITE, "PRESS START");
        WaitForVBlank();
        smr_text_update_vram();
      }
      break;
    }
  }
}
