#ifndef SMR_GUARDS_ROM_DATA
#define SMR_GUARDS_ROM_DATA

#include <stdint.h>

typedef struct {
    int16_t x __attribute__((packed));
    int16_t y __attribute__((packed));
} __attribute__((packed)) smr_guard_position_rom_data;

typedef struct {
    uint16_t nb_life __attribute__((packed));
    uint16_t nb_positions  __attribute__((packed));
    smr_guard_position_rom_data positions[];
} __attribute__((packed)) smr_guard_rom_data;
#endif