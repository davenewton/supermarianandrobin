#ifndef SMR_CASTLE_ROM_DATA_H
#define SMR_CASTLE_ROM_DATA_H

#include "smr_axes_rom_data.h"
#include "smr_flails_rom_data.h"
#include "smr_guard_rom_data.h"
#include "smr_platform_rom_data.h"
#include "smr_boars_rom_data.h"
#include "smr_crossbowman_rom_data.h"
#include <stdint.h>

typedef struct {
    int16_t marianX __attribute__((packed));
    int16_t marianY __attribute__((packed));
    int16_t robinX __attribute__((packed));
    int16_t robinY __attribute__((packed));
    int16_t doorX __attribute__((packed));
    int16_t doorY __attribute__((packed));
    int16_t keyX __attribute__((packed));
    int16_t keyY __attribute__((packed));
} __attribute__((packed)) smr_castle_level_rom_data_header;

#define SMR_CASTLE_NB_LEVELS (5)

typedef struct {
    smr_castle_level_rom_data_header* header  __attribute__((packed));
    uint16_t bg1_size __attribute__((packed));
    uint8_t* bg1 __attribute__((packed));
    uint16_t bg2_size __attribute__((packed));
    uint8_t* bg2 __attribute__((packed));
    smr_platforms_rom_data* platforms __attribute__((packed));
    smr_flails_rom_data* flails __attribute__((packed));
    smr_boars_rom_data* boars __attribute__((packed));
    smr_axes_rom_data* axes __attribute__((packed));
    smr_crossbowman_rom_data* crossbowman __attribute__((packed));
    smr_guard_rom_data* guard __attribute__((packed));
} __attribute__((packed)) smr_castle_level_rom_data;

#endif

