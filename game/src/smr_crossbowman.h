#ifndef SMR_CROSSBOWMAN_H
#define SMR_CROSSBOWMAN_H

#include "smr_animation.h"
#include "smr_crossbowman_rom_data.h"
#include "smr_arrow.h"
#include <snes.h>

#define SMR_CROSSBOWMAN_WIDTH (24)
#define SMR_CROSSBOWMAN_HEIGHT (32)
#define SMR_CROSSBOWMAN_GHOST_HORIZONTAL_AMPLITUDE (16)
#define SMR_CROSSBOWMAN_GHOST_SPEED (2)

typedef enum {
    SMR_CROSSBOWMAN_STATE_INACTIVE=0,
    SMR_CROSSBOWMAN_APPEAR=1,
    SMR_CROSSBOWMAN_IDLE=2,
    SMR_CROSSBOWMAN_SHOOT=3,
    SMR_CROSSBOWMAN_HIT_BY_ARROW=4,
    SMR_CROSSBOWMAN_STATE_DEAD=5,
    SMR_CROSSBOWMAN_STATE_DEAD_AND_LOVING_IT=6,
} smr_crossbowman_state;

typedef struct {
    smr_streamed_animation animation;
    t_sprites* sprite;
    smr_crossbowman_state state;
    s16 shoot_x;
    s16 shoot_y;
    s16 shoot_vx;
    u16 ticks;
    smr_arrow* own_arrow;
    smr_arrow* arrow_stucked;
    s16 dead_x;
    s16 velocity_x;
    smr_crossbowman_rom_data *rom_data;
    u8 remaining_life;
} smr_crossbowman;

extern smr_crossbowman crossbowman;
extern void smr_reset_crossbowman(smr_crossbowman_rom_data* rom_data);
extern void smr_update_crossbowman();

#endif