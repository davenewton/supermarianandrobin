#ifndef SMR_ARROW_H
#define SMR_ARROW_H

#include <snes.h>

extern u8 arrow_fly, arrow_fly_end;

typedef struct {
  u16 oam_id;
  s16 x;
  s16 y;
  s16 velocity_x;
  u16 tick;
  bool is_looking_left;
} smr_arrow;

#define SMR_MAX_ARROWS (3)
#define SMR_ARROW_WIDTH (16)
#define SMR_ARROW_HEIGHT (8)
#define SMR_ARROW_VELOCITY (2)
#define SMR_ARROW_MAX_TICKS (120)

#define SMR_MARIAN_ARROW (0)
#define SMR_ROBIN_ARROW (1)
#define SMR_CROSSBOWMAN_ARROW (2)

extern smr_arrow arrows[SMR_MAX_ARROWS];

extern void smr_reset_arrows();
extern void smr_shoot_one_arrow(smr_arrow* arrow, s16 x, s16 y, s16 velocity_x);
extern void smr_disable_arrow(smr_arrow* arrow);
extern void smr_update_arrows();

#endif