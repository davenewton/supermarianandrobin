#ifndef SMR_PLATFORM_ROM_DATA
#define SMR_PLATFORM_ROM_DATA

#include <stdint.h>

#define SMR_PLATFORM_FLAG_LETHAL (1)

typedef struct {
    int16_t x __attribute__((packed));
    int16_t y __attribute__((packed));
    int16_t w __attribute__((packed));
    int16_t h __attribute__((packed));
    int16_t flags __attribute__((packed));
} __attribute__((packed)) smr_platform_rom_data;

typedef struct  {
    uint16_t nb_platforms __attribute__((packed));
    smr_platform_rom_data platforms[];
} __attribute__((packed)) smr_platforms_rom_data;

#endif