#include "smr_axe.h"
#include "smr_oam.h"
#include "smr_palette.h"

extern u8 axe_head;
extern u8 axe_handle;

smr_axe axes[SMR_MAX_AXES] = {
    {
        .oam_ids = {SMR_OAM_ID(34), SMR_OAM_ID(35), SMR_OAM_ID(36), SMR_OAM_ID(37)},
    },
    {
        .oam_ids = {SMR_OAM_ID(38), SMR_OAM_ID(39), SMR_OAM_ID(40), SMR_OAM_ID(41)},
    },
    {
        .oam_ids = {SMR_OAM_ID(42), SMR_OAM_ID(43), SMR_OAM_ID(44), SMR_OAM_ID(45)},
    },
    {
        .oam_ids = {SMR_OAM_ID(46), SMR_OAM_ID(47), SMR_OAM_ID(48), SMR_OAM_ID(49)},
    }};

void smr_reset_axes() {
  dmaCopySpr16Vram(&axe_head, SMR_VRAM_SPRITE_16_TILE_ADDRESS(0x1200));
  dmaCopySpr16Vram(&axe_handle, SMR_VRAM_SPRITE_16_TILE_ADDRESS(0x1220));

  int a;
  for (a = 0; a < SMR_MAX_AXES; ++a) {
    smr_axe *axe = &axes[a];
    axe->active = false;
    u8 attributes = 0b00100001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK;
    if (axe->is_looking_left) {
      attributes |= 0b01000000;
    }
    oamSetEx(axe->oam_ids[0], OBJ_SMALL, OBJ_HIDE);
    oamSetAttr(axe->oam_ids[0], -100, 0, SMR_VRAM_SPRITE_16_TILE_INDEX(32), attributes);
    oamSetEx(axe->oam_ids[1], OBJ_SMALL, OBJ_HIDE);
    oamSetAttr(axe->oam_ids[1], -100, 0, SMR_VRAM_SPRITE_16_TILE_INDEX(34), attributes);
    oamSetEx(axe->oam_ids[2], OBJ_SMALL, OBJ_HIDE);
    oamSetAttr(axe->oam_ids[2], -100, 0, SMR_VRAM_SPRITE_16_TILE_INDEX(34), attributes);
    oamSetEx(axe->oam_ids[3], OBJ_SMALL, OBJ_HIDE);
    oamSetAttr(axe->oam_ids[3], -100, 0, SMR_VRAM_SPRITE_16_TILE_INDEX(34), attributes);
  }
}
void smr_add_axe(smr_axe_rom_data *rom_data) {
  int a;
  for (a = 0; a < SMR_MAX_AXES; ++a) {
    smr_axe *axe = &axes[a];
    if (!axe->active) {
      axe->active = true;
      axe->ticks = rom_data->ticks_before_first_move;
      axe->ticks_between_moves = rom_data->ticks_between_moves;
      oamSetEx(axe->oam_ids[0], OBJ_SMALL, OBJ_SHOW);
      oamSetEx(axe->oam_ids[1], OBJ_SMALL, OBJ_SHOW);
      oamSetEx(axe->oam_ids[2], OBJ_SMALL, OBJ_SHOW);
      oamSetEx(axe->oam_ids[3], OBJ_SMALL, OBJ_SHOW);
      axe->is_looking_left = rom_data->is_looking_left;
      if (rom_data->is_looking_left) {
        oamFlip(axe->oam_ids[0], 1, 0);
        axe->tip_x_min = rom_data->anchor_x - 64;
        axe->tip_x_max = rom_data->anchor_x - 16;
        axe->tip_x = axe->tip_x_max;
        axe->vx = -SMR_AXE_SPEED;
      } else {
        axe->tip_x_min = rom_data->anchor_x + 16;
        axe->tip_x_max = rom_data->anchor_x + 64;
        axe->tip_x = axe->tip_x_min;
        axe->vx = SMR_AXE_SPEED;
      }
      axe->tip_y = rom_data->anchor_y;
      return;
    }
  }
}

void smr_add_axes(smr_axes_rom_data *rom_data) {
  int a;
  for (a = 0; a < rom_data->nb_axes; ++a) {
    smr_add_axe(&rom_data->axes[a]);
  }
}
void smr_update_axes() {
  int a;
  for (a = 0; a < SMR_MAX_AXES; ++a) {
    smr_axe *axe = &axes[a];
    if (axe->active) {
      if (axe->ticks == 0) {
        axe->tip_x += axe->vx;
        if (axe->tip_x >= axe->tip_x_max) {
          axe->vx = -SMR_AXE_SPEED;
          axe->ticks = axe->ticks_between_moves;
        } else if (axe->tip_x <= axe->tip_x_min) {
          axe->vx = SMR_AXE_SPEED;
          axe->ticks = axe->ticks_between_moves;
        }
      } else {
        --axe->ticks;
      }
      u16 sprite_y = axe->tip_y - 8;
      u16 sprite_x = axe->tip_x;
      if (axe->is_looking_left) {
        oamSetXY(axe->oam_ids[0], sprite_x, sprite_y);
        oamSetXY(axe->oam_ids[1], sprite_x + 16, sprite_y);
        oamSetXY(axe->oam_ids[2], sprite_x + 32, sprite_y);
        oamSetXY(axe->oam_ids[3], sprite_x + 48, sprite_y);
      } else {
        oamSetXY(axe->oam_ids[0], sprite_x - 16, sprite_y);
        oamSetXY(axe->oam_ids[1], sprite_x - 32, sprite_y);
        oamSetXY(axe->oam_ids[2], sprite_x - 48, sprite_y);
        oamSetXY(axe->oam_ids[3], sprite_x - 64, sprite_y);
      }
    }
  }
}
void smr_axes_collide(smr_character *character) {
  int a;
  for (a = 0; a < SMR_MAX_AXES; ++a) {
    smr_axe *axe = &axes[a];
    if (axe->active && axe->tip_x > character->x && axe->tip_y > character->y && axe->tip_x < (character->x + SMR_CHARACTER_WIDTH) && axe->tip_y < (character->y + SMR_CHARACTER_HEIGHT)) {
      smr_character_kill(character);
      return;
    }
  }
}