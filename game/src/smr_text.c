#include "smr_text.h"
#include "smr_palette.h"

// Font graphics in ROM
extern u8 font_tileset, font_tileset_end;

//TODO reuse other tilemap to save some RAM ?
u16 smr_text_tilemap[32 * 32];

void smr_text_init_bg3_tileset() {
      bgInitTileSet(
      2, &font_tileset, &smr_palette, 0,
      (&font_tileset_end - &font_tileset), 16 * 2, BG_4COLORS,
      56 * 1024 /
          2); // BG3 selects a palette from the first 16 entries of CGRAM.
}

void smr_text_init_bg3_map(){
  bgSetMapPtr(2, 58 * 1024 / 2, SC_32x32);
}

void smr_text_clear() {
  u16 i;
  for (i = 0; i < 32 * 32; ++i) {
    smr_text_tilemap[i] = 0;
  }
}

#define SMR_TEXT_NB_CHARACTER_IN_TILESET (39)
#define SMR_TEXT_FIRST_LETTER_INDEX (3)
#define SMR_TEXT_FIRST_NUMBER_INDEX (29)

void smr_text_print(u16 x, u16 y, u16 w, u16 h, u16 mask, const char *text) {
  char c;
  u8 isValidMapEntry;
  u16 tilemapEntry;
  u16 xc = x, yc = y, maxX = x + w, maxY = y + h;
  if (maxX > 31) {
    maxX = 31;
  }
  if (maxY > 30) {
    maxY = 30;
  }
  while ((c = *(text++))) {

    if (xc > maxX) {
      xc = x;
      yc += 3;
    }
    if (yc > maxY) {
      break;
    }
    isValidMapEntry = 1;
    switch (c) {
    case '.':
      tilemapEntry = 0;
      break;
    case '?':
      tilemapEntry = 1;
      break;
    case '!':
      tilemapEntry = 2;
      break;
    default:
      if (c >= 'A' && c <= 'Z') {
        tilemapEntry = SMR_TEXT_FIRST_LETTER_INDEX+ c - 'A';
      } else if (c >= '0' && c <= '9') { 
        tilemapEntry = SMR_TEXT_FIRST_NUMBER_INDEX + c - '0';
      }else {
        isValidMapEntry = 0;
      }
      break;
    }
    if (isValidMapEntry) {
      smr_text_tilemap[yc * 32 + xc] = tilemapEntry | mask;
      smr_text_tilemap[(yc + 1) * 32 + xc] = (tilemapEntry + SMR_TEXT_NB_CHARACTER_IN_TILESET) | mask;
    } else {
      smr_text_tilemap[yc * 32 + xc] = 0;
      smr_text_tilemap[(yc + 1) * 32 + xc] = 0;
    }
    ++xc;
  }
}

void smr_text_print_number(u16 x, u16 y, u16 w, u16 h, u16 mask, u16 num, smr_text_number_format format) {
  char c;
  u16 tilemapEntry;
  u16 xc = x, yc = y, maxX = x + w, maxY = y + h;
  if (maxX > 31) {
    maxX = 31;
  }
  if (maxY > 30) {
    maxY = 30;
  }
  do {
    u16 n = (num / format) % 10;
    if (xc > maxX) {
      xc = x;
      yc += 3;
    }
    if (yc > maxY) {
      break;
    }
    tilemapEntry = SMR_TEXT_FIRST_NUMBER_INDEX + n;
    smr_text_tilemap[yc * 32 + xc] = tilemapEntry | mask;
    smr_text_tilemap[(yc + 1) * 32 + xc] = (tilemapEntry + SMR_TEXT_NB_CHARACTER_IN_TILESET) | mask;
    ++xc;
    format /= 10;
  } while(format > 0);
}


void smr_text_update_vram() {
  dmaCopyVram((u8 *)smr_text_tilemap, 58 * 1024 / 2, 32 * 32 * 2);
}