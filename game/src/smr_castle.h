#ifndef SMR_CASTLE_H
#define SMR_CASTLE_H

#include "smr_adventure.h"

extern int current_castle_level;
extern smr_adventure_outcome smr_scene_castle();

#endif