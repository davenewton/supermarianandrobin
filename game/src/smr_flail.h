#ifndef SMR_FLAIL_H
#define SMR_FLAIL_H

#include "smr_flails_rom_data.h"
#include "smr_character.h"

#define SMR_FLAIL_NB_LINKS (8)
#define SMR_FLAIL_SPRITE_OFFSET_X (-8)
#define SMR_FLAIL_SPRITE_OFFSET_Y (-8)
#define SMR_FLAIL_BALL_WIDTH (16)
#define SMR_FLAIL_BALL_HEIGHT (16)

typedef struct {
  s16 anchor_x;
  s16 anchor_y;
  s16 ball_x;
  s16 ball_y;
  u16 ball_oam_id;
  u16 links_oam_ids[SMR_FLAIL_NB_LINKS];
  bool active;
  u16 ball_tick;
} smr_flail;

#define SMR_MAX_FLAILS (2)

extern smr_flail flails[SMR_MAX_FLAILS];

extern void smr_reset_flails();
extern void smr_add_flails(smr_flails_rom_data* rom_data);
extern void smr_update_flails();
extern void smr_flails_collide(smr_character *character);

#endif