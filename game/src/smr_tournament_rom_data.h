#ifndef SMR_TOURNAMENT_ROM_DATA_H
#define SMR_TOURNAMENT_ROM_DATA_H

#include <stdint.h>

typedef struct {
    int16_t marianX __attribute__((packed));
    int16_t marianY __attribute__((packed));
    int16_t robinX __attribute__((packed));
    int16_t robinY __attribute__((packed));
} __attribute__((packed)) smr_tournament_rom_data_header;

#endif

