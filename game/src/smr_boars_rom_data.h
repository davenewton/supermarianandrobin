#ifndef SMR_BOARD_ROM_DATA
#define SMR_BOARD_ROM_DATA

#include <stdint.h>

typedef struct {
    int16_t x __attribute__((packed));
    int16_t y __attribute__((packed));
    uint16_t ticks_before_first_spit __attribute__((packed));
    uint16_t ticks_between_spits __attribute__((packed));
} __attribute__((packed)) smr_boar_rom_data;

typedef struct  {
    uint16_t nb_boars __attribute__((packed));
    smr_boar_rom_data boars[];
} __attribute__((packed)) smr_boars_rom_data;

#endif