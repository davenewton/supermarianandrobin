#ifndef SMR_BOAR_H
#define SMR_BOAR_H

#include "smr_boars_rom_data.h"
#include "smr_character.h"

#define SMR_BOAR_NB_SPITS (4)
#define SMR_BOAR_SPIT_SPRITE_COLLISION_WIDTH (16)
#define SMR_BOAR_SPIT_SPRITE_COLLISION_HEIGHT (16)
#define SMR_BOAR_HEAD_WIDTH (16)
#define SMR_BOAR_HEAD_HEIGHT (16)
#define SMR_BOAR_SPIT_WIDTH (8)
#define SMR_BOAR_SPIT_HEIGHT (16)

typedef struct {
  u16 oam_id;
  s16 x;
  s16 y;
  bool active;
} smr_boar_spit;

typedef struct {
  u16 head_oam_id;
  s16 head_x;
  s16 head_y;
  smr_boar_spit spits[SMR_BOAR_NB_SPITS];
  bool active;
  u16 ticks;
  u16 ticks_between_spits;
} smr_boar;

#define SMR_MAX_BOARS (2)

extern smr_boar boars[SMR_MAX_BOARS];

extern void smr_reset_boars();
extern void smr_add_boars(smr_boars_rom_data* rom_data);
extern void smr_update_boars();
extern void smr_boars_collide(smr_character *character);

#endif