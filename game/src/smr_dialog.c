#include "smr_dialog.h"
#include "smr_arrow.h"
#include "smr_pad.h"
#include "smr_palette.h"
#include "smr_text.h"
#include "smr_oam.h"
#include "smr_sound.h"

// Graphics in ROM
extern u8 dialog_tileset, dialog_tileset_end;
extern u8 dialog_bg1, dialog_bg1_end;
extern u8 dialog_bg2, dialog_bg2_end;

#define SMR_DIALOG_ARROW_Y_BASE (82)
#define SMR_DIALOG_ARROW_Y_INCREMENT (24)
#define SMR_DIALOG_ARROW_X (6 * 8 + 4)

u8 smr_scene_dialog(const smr_dialog *const dialog) {

  // reinit
  setScreenOff();
  oamClear(0, 128);
  dmaClearVram();

  // Init layers with tiles
  bgInitTileSet(0, &dialog_tileset, &smr_palette, 0,
                (&dialog_tileset_end - &dialog_tileset), 16 * 2, BG_16COLORS,
                32 * 1024 / 2);
  bgInitTileSet(1, &dialog_tileset, &smr_palette, 0,
                (&dialog_tileset_end - &dialog_tileset), 16 * 2, BG_16COLORS,
                32 * 1024 / 2);
  smr_text_init_bg3_tileset();
  bgInitMapSet(0, &dialog_bg1,
               &dialog_bg1_end - &dialog_bg1, SC_32x32,
               60 * 1024 / 2);
  bgInitMapSet(1, &dialog_bg2,
               &dialog_bg2_end - &dialog_bg2, SC_32x32,
               62 * 1024 / 2);
  smr_text_init_bg3_map();
  smr_text_clear();
  smr_text_print(8, 2, 21, 6, SMR_TEXT_MASK_BROWN, dialog->marian_line);
  smr_text_print(2, 20, 21, 6, SMR_TEXT_MASK_BROWN, dialog->robin_line);
  smr_text_print(9, 10, 20, 2, SMR_TEXT_MASK_WHITE, dialog->choice1);
  smr_text_print(9, 13, 20, 2, SMR_TEXT_MASK_WHITE, dialog->choice2);
  smr_text_print(9, 16, 20, 2, SMR_TEXT_MASK_WHITE, dialog->choice3);
  smr_text_update_vram();

  // Now Put in 16 color mode and enable backgrounds
  setMode(BG_MODE1, BG3_MODE1_PRIORITY_HIGH);
  bgSetEnable(0);
  bgSetEnable(1);
  bgSetEnable(2);

  // Scroll
  bgSetScroll(0, 0, 0);
  bgSetScroll(1, 0, 0);
  bgSetScroll(2, 0, 0);

  // init sprites
  dmaCopyCGram(&smr_palette, (128 + SMR_PALETTE_SPRITE_INDEX * 16), 16 * 2);
  dmaCopySpr16Vram(&arrow_fly, SMR_VRAM_SPRITE_16_ADDRESS);
  oamInitDynamicSprite(SMR_VRAM_SPRITE_32_ADDRESS, SMR_VRAM_SPRITE_16_ADDRESS, 0, 0, OBJ_SIZE16_L32);
  
  u8 currentChoice = 0;

  oamSetEx(0, OBJ_SMALL, OBJ_SHOW);
  oamSetAttr(0, SMR_DIALOG_ARROW_X, 0, SMR_VRAM_SPRITE_16_TILE_INDEX(0), 0b00110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK);

  // Music
  smr_play_music(MOD_DIALOG);

  // Screen activated
  setScreenOn();

  while (1) {
    smr_update_pads();
    if (smr_test_if_confirm()) {
      smr_play_sound(SMR_SOUND_START);
      spcProcess();
      return currentChoice;
    }
    if (smr_test_and_reset_if_pressed(0, KEY_DOWN) || smr_test_and_reset_if_pressed(1, KEY_DOWN)) {
      smr_play_sound(SMR_SOUND_SELECT);
      if (currentChoice == 2) {
        currentChoice = 0;
      } else {
        ++currentChoice;
      }
    }
    if (smr_test_and_reset_if_pressed(0, KEY_UP) || smr_test_and_reset_if_pressed(1, KEY_UP)) {
      smr_play_sound(SMR_SOUND_SELECT);
      if (currentChoice == 0) {
        currentChoice = 2;
      } else {
        --currentChoice;
      }
    }

    oamSetXY(0 * 4, SMR_DIALOG_ARROW_X, SMR_DIALOG_ARROW_Y_BASE + currentChoice * SMR_DIALOG_ARROW_Y_INCREMENT);
    spcProcess();
    WaitForVBlank();
  }
}
