#ifndef SMR_CHARACTER_H
#define SMR_CHARACTER_H

#include "smr_animation.h"
#include "smr_arrow.h"

#define SMR_CHARACTER_MIN_VELOCITY_X (-4)
#define SMR_CHARACTER_MIN_VELOCITY_Y (-4)
#define SMR_CHARACTER_MAX_VELOCITY_X (4)
#define SMR_CHARACTER_MAX_VELOCITY_Y (4)

#define SMR_CHARACTER_WIDTH (24)
#define SMR_CHARACTER_HEIGHT (32)

#define SMR_CHARACTER_SPRITE_OFFSET_X (-4)
#define SMR_CHARACTER_SPRITE_OFFSET_Y (0)

#define SMR_CHARACTER_WALK_SPEED (2)
#define SMR_CHARACTER_JUMP_IMPULSE (-8)
#define SMR_CHARACTER_JUMP_IMPULSE_FRAMES (10)

#define SMR_CHARACTER_GHOST_HORIZONTAL_AMPLITUDE (16)

typedef enum {
  SMR_CHARACTER_TYPE_MARIAN = 0,
  SMR_CHARACTER_TYPE_ROBIN = 1
} smr_character_type;
#define SMR_CHARACTER_TYPE_COUNT (2)

typedef enum {
    SMR_CHARACTER_ANIMATION_GHOST = 0,
    SMR_CHARACTER_ANIMATION_IDLE = 1,
    SMR_CHARACTER_ANIMATION_OHAI = 2,
    SMR_CHARACTER_ANIMATION_SHOOT = 3,
    SMR_CHARACTER_ANIMATION_WALK = 4
} smr_character_animation_type;
#define SMR_CHARACTER_ANIMATION_COUNT (5)

typedef enum {
  SMR_CHARACTER_STATE_MOVING=0,
  SMR_CHARACTER_STATE_SHOOTING=1,
  SMR_CHARACTER_STATE_HIT_BY_ARROW=2,
  SMR_CHARACTER_STATE_DEAD=3,
  SMR_CHARACTER_STATE_DEAD_AND_LOVING_IT=4,
  SMR_CHARACTER_STATE_OUT=5
} smr_character_state;

typedef struct {
  smr_streamed_animation animation;
  t_sprites* sprite;
  s16 x;
  s16 y;
  s16 velocity_x;
  s16 velocity_y;
  smr_character_type type;
  smr_character_animation_type animation_type;
  u16 jump_frames;
  smr_character_state state;
  bool is_on_ground;
  smr_arrow* own_arrow;
  smr_arrow* arrow_stucked;
  s16 dead_x;
  bool active;
} smr_character;

extern void smr_character_set_animation(smr_character *character, smr_character_animation_type state);
extern void smr_character_reset_body(smr_character* character, s16 x, s16 y);
extern void smr_character_control_with_pad(smr_character* character, u16 pad);
extern void smr_character_update_body(smr_character* character);
extern void smr_character_reset_sprite(smr_character* character);
extern void smr_character_update_sprite(smr_character* character);
extern void smr_character_set_out(smr_character* character);
extern void smr_character_kill(smr_character *character);

extern smr_character characters[SMR_CHARACTER_TYPE_COUNT];

#endif