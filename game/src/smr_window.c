#include "smr_window.h"
#include "snes/video.h"
#include <snes.h>

void smr_show_fullscreen_window() {
    // Set BG3 SubScreen and
    //bgSetEnableSub(1);
    /*
    7  bit  0
    ---- ----
    MMSS ..AD
    ||||   ||
    ||||   |+- Direct color mode
    ||||   +-- Addend (0 = fixed color, 1 = subscreen)
    ||++------ Sub screen color window transparent region
    ++-------- Main screen color window black region
    */
    REG_CGWSEL = 0b00000000;

    /*
    7  bit  0
    ---- ----
    BGRC CCCC
    |||| ||||
    |||+-++++- Color value
    ||+------- Write color value to blue channel
    |+-------- Write color value to green channel
    +--------- Write color value to red channel
    */
    REG_COLDATA = 0b11100000;

    /*
    7  bit  0
    ---- ----
    MHBO 4321
    |||| ||||
    |||| |||+- BG1 color math enable
    |||| ||+-- BG2 color math enable
    |||| |+--- BG3 color math enable
    |||| +---- BG4 color math enable
    |||+------ OBJ color math enable (palettes 4-7 only)
    ||+------- Backdrop color math enable
    |+-------- Half color math
    +--------- Operator type (0 = add, 1 = subtract)
    */
    REG_CGADSUB = 0b01011011;

    // enable Subscreen Color ADD/SUB and Color addition on BG1 and Backdrop color
    //setColorEffect(CM_SUBBGOBJ_ENABLE, CM_MSCR_BACK | CM_MSCR_BG1);
    
}

void smr_hide_fullscreen_window() {
    REG_CGWSEL = 0;
    REG_COLDATA = 0;
    REG_CGADSUB = 0;
}