#ifndef SMR_LOOKUP_TABLES_ROM_DATA_H
#define SMR_LOOKUP_TABLES_ROM_DATA_H

#include <stdint.h>
typedef struct {
  int16_t x __attribute__((packed));
  int16_t y __attribute__((packed));
} __attribute__((packed)) smr_lookup_path_point;

#define SMR_LOOKUP_CIRCULAR_PATH_NB_POINTS (256)
#define SMR_LOOKUP_CIRCULAR_PATH_RADIUS (256)
#define SMR_LOOKUP_CIRCULAR_PATH_RADIUS_DIVIDE_SHIFT (8)

#endif