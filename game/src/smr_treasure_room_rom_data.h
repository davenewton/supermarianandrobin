#ifndef SMR_TREASURE_ROOM_ROM_DATA_H
#define SMR_TREASURE_ROOM_ROM_DATA_H

#include "smr_platform_rom_data.h"
#include <stdint.h>

typedef struct {
  int16_t marianX __attribute__((packed));
  int16_t marianY __attribute__((packed));
  int16_t robinX __attribute__((packed));
  int16_t robinY __attribute__((packed));
  int16_t kingX __attribute__((packed));
  int16_t kingY __attribute__((packed));
  int16_t chestX __attribute__((packed));
  int16_t chestY __attribute__((packed));
} __attribute__((packed)) smr_treasure_room_rom_data_header;

#endif
