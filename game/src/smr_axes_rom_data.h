#ifndef SMR_AXE_ROM_DATA
#define SMR_AXE_ROM_DATA

#include <stdint.h>

typedef struct {
    int16_t anchor_x __attribute__((packed));
    int16_t anchor_y __attribute__((packed));
    uint16_t ticks_before_first_move __attribute__((packed));
    uint16_t ticks_between_moves __attribute__((packed));
    uint16_t is_looking_left __attribute__((packed));
} __attribute__((packed)) smr_axe_rom_data;

typedef struct  {
    uint16_t nb_axes __attribute__((packed));
    smr_axe_rom_data axes[];
} __attribute__((packed)) smr_axes_rom_data;

#endif