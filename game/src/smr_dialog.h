#ifndef SMR_DIALOG_H
#define SMR_DIALOG_H

#include <snes.h>

typedef struct {
  const char *marian_line __attribute__((packed));
  const char *robin_line __attribute__((packed));
  const char *choice1 __attribute__((packed));
  const char *choice2 __attribute__((packed));
  const char *choice3 __attribute__((packed));
} __attribute__((packed)) smr_dialog;

extern smr_dialog dialog_character_select;
extern smr_dialog dialog_activity_select;
extern smr_dialog dialog_mariansvictory;
extern smr_dialog dialog_robinsvictory;
extern smr_dialog dialog_castlebadending;
extern smr_dialog dialog_castlegoodending;

typedef enum  {
  SMR_SELECT_MARIAN = 0,
  SMR_SELECT_ROBIN = 1,
  SMR_SELECT_BOTH = 2,
} smr_character_selection;

typedef enum {
  SMR_CASTLE = 0,
  SMR_TOURNAMENT = 1,
  SMR_FOREST = 2,
} smr_activity_selection;

typedef enum {
  SMR_PLAY_AGAIN = 0,
  SMR_DO_SOMETHING_ELSE = 1,
  SMR_EXIT = 2
} smr_after_activity_selection;

extern u8 smr_scene_dialog(const smr_dialog *const dialog);

#endif