#include "smr_crossbowman.h"
#include "smr_animation.h"
#include "smr_arrow.h"
#include "smr_character.h"
#include "smr_oam.h"
#include "smr_palette.h"
#include "smr_world.h"

extern smr_animation_rom_data crossbowman_appear;
extern smr_animation_rom_data crossbowman_idle;
extern smr_animation_rom_data crossbowman_ghost;
extern smr_animation_rom_data crossbowman_shoot;

smr_crossbowman crossbowman =
    {.sprite = SMR_OAMBUFFER_ID_TO_PTR(10),
     .own_arrow = &arrows[SMR_CROSSBOWMAN_ARROW]};

void make_crossbowman_appear() {
  u16 position_index = rand() % crossbowman.rom_data->nb_positions;
  smr_crossbowman_position_rom_data *position = &crossbowman.rom_data->positions[position_index];

  crossbowman.state = SMR_CROSSBOWMAN_IDLE;
  crossbowman.arrow_stucked = 0;
  crossbowman.ticks = 0;
  crossbowman.sprite->oamx = position->x;
  crossbowman.sprite->oamy = position->y;
  crossbowman.shoot_y = position->y + 13;
  crossbowman.sprite->oamattribute = 0b00110000 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK; // sprite 32x32 and priority 3
  if (position->is_looking_left) {
    crossbowman.shoot_x = position->x + 9;
    crossbowman.shoot_vx = -SMR_ARROW_VELOCITY;
    crossbowman.sprite->oamattribute |= 0b01000000;
  } else {
    crossbowman.shoot_x = position->x + 22;
    crossbowman.shoot_vx = SMR_ARROW_VELOCITY;
  }
  crossbowman.state = SMR_CROSSBOWMAN_APPEAR;
  smr_streamed_animation_start(&crossbowman.animation, &crossbowman_appear, crossbowman.sprite);
}

void smr_reset_crossbowman(smr_crossbowman_rom_data *rom_data) {

  crossbowman.rom_data = rom_data;
  crossbowman.remaining_life = rom_data->nb_life;
  if (crossbowman.remaining_life > 0) {
    make_crossbowman_appear();
  }
}

bool is_character_in_line_with_crossbowman() {
  int ch;
  for (ch = 0; ch < SMR_CHARACTER_TYPE_COUNT; ++ch) {
    smr_character *character = &characters[ch];
    if(!character->active) {
      continue;
    }
    s16 shoot_y = crossbowman.shoot_y;
    s16 character_top = character->y;
    if (shoot_y > character_top) {
      s16 character_bottom = character->y + SMR_CHARACTER_HEIGHT;
      if (shoot_y < character_bottom) {
        return true;
      }
    }
  }
  return false;
}

void collides_crossbowman_with_arrows() {

  int a;
  for (a = 0; a < SMR_MAX_ARROWS; ++a) {
    smr_arrow *arrow = &arrows[a];
    if (arrow->tick >= SMR_ARROW_MAX_TICKS || arrow == crossbowman.own_arrow) {
      continue;
    }
    s16 arrow_tip_x = arrow->is_looking_left ? arrow->x : arrow->x + SMR_ARROW_WIDTH;
    s16 crossbowman_left = crossbowman.sprite->oamx;
    s16 crossbowman_right = crossbowman.sprite->oamx + SMR_CROSSBOWMAN_WIDTH;
    if (arrow_tip_x > crossbowman_left && arrow_tip_x < crossbowman_right) {
      s16 arrow_tip_y = arrow->y + SMR_ARROW_HEIGHT / 2;
      s16 crossbowman_top = crossbowman.sprite->oamy;
      s16 crossbowman_bottom = crossbowman.sprite->oamy + SMR_CROSSBOWMAN_HEIGHT;
      if (arrow_tip_y > crossbowman_top && arrow_tip_y < crossbowman_bottom) {
        crossbowman.state = SMR_CROSSBOWMAN_HIT_BY_ARROW;
        crossbowman.arrow_stucked = arrow;
        return;
      }
    }
  }
}

void smr_update_crossbowman() {
  switch (crossbowman.state) {
  case SMR_CROSSBOWMAN_STATE_INACTIVE: {
    return;
  }
  case SMR_CROSSBOWMAN_APPEAR: {
    if (crossbowman.animation.loop_count >= 1) {
      smr_streamed_animation_start(&crossbowman.animation, &crossbowman_idle, crossbowman.sprite);
      crossbowman.state = SMR_CROSSBOWMAN_IDLE;
    }
    break;
  }
  case SMR_CROSSBOWMAN_IDLE: {
    if (crossbowman.ticks > 60) {
      if (crossbowman.own_arrow->tick >= SMR_ARROW_MAX_TICKS && is_character_in_line_with_crossbowman()) {
        smr_streamed_animation_start(&crossbowman.animation, &crossbowman_shoot, crossbowman.sprite);
        crossbowman.state = SMR_CROSSBOWMAN_SHOOT;
      }
    } else {
      ++crossbowman.ticks;
    }
    collides_crossbowman_with_arrows();
    break;
  }
  case SMR_CROSSBOWMAN_SHOOT: {
    if (crossbowman.animation.loop_count >= 1) {
      smr_shoot_one_arrow(crossbowman.own_arrow, crossbowman.shoot_x, crossbowman.shoot_y, crossbowman.shoot_vx);
      crossbowman.ticks = 0;
      smr_streamed_animation_start(&crossbowman.animation, &crossbowman_idle, crossbowman.sprite);
      crossbowman.state = SMR_CROSSBOWMAN_IDLE;
    }
    collides_crossbowman_with_arrows();
    break;
  }
  case SMR_CROSSBOWMAN_HIT_BY_ARROW: {
    smr_arrow* arrow_stucked = crossbowman.arrow_stucked;
    if (arrow_stucked) {
      if (arrow_stucked->is_looking_left) { // TODO macro to detect if arrow is looking left macro or function
        crossbowman.sprite->oamx = arrow_stucked->x - SMR_CROSSBOWMAN_WIDTH / 2;
      } else {
        crossbowman.sprite->oamx = arrow_stucked->x + SMR_ARROW_WIDTH / 2 - SMR_CROSSBOWMAN_WIDTH / 2;
      }

      if (crossbowman.arrow_stucked->tick >= SMR_ARROW_MAX_TICKS) {
        crossbowman.arrow_stucked = NULL;
        crossbowman.state = SMR_CROSSBOWMAN_STATE_DEAD;
        crossbowman.dead_x = crossbowman.sprite->oamx;
        crossbowman.velocity_x = 1;
        smr_streamed_animation_start(&crossbowman.animation, &crossbowman_ghost, crossbowman.sprite);
      }
    } else {
      // TODO
    }
    break;
  }
  case SMR_CROSSBOWMAN_STATE_DEAD: {
    crossbowman.sprite->oamx += crossbowman.velocity_x;
    if (crossbowman.sprite->oamx > (crossbowman.dead_x + SMR_CROSSBOWMAN_GHOST_HORIZONTAL_AMPLITUDE)) {
      crossbowman.velocity_x = -1;
    } else if (crossbowman.sprite->oamx < (crossbowman.dead_x - SMR_CROSSBOWMAN_GHOST_HORIZONTAL_AMPLITUDE)) {
      crossbowman.velocity_x = 1;
    }
    crossbowman.sprite->oamy -= SMR_CROSSBOWMAN_GHOST_SPEED;
    if (crossbowman.sprite->oamy < (SMR_WORLD_BOUNDS_TOP - SMR_CROSSBOWMAN_HEIGHT)) {
      if (crossbowman.remaining_life > 1) {
        --crossbowman.remaining_life;
        make_crossbowman_appear();
      } else {
        crossbowman.state = SMR_CROSSBOWMAN_STATE_DEAD_AND_LOVING_IT;
      }
    }
    break;
  }
  case SMR_CROSSBOWMAN_STATE_DEAD_AND_LOVING_IT: {
    break;
  }
  }
  smr_streamed_animation_update(&crossbowman.animation, crossbowman.sprite);
  oamDynamic32Draw(crossbowman.sprite - oambuffer);
}
