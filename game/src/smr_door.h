#ifndef SMR_DOOR_H
#define SMR_DOOR_H

#include <snes.h>

#define SMR_DOOR_OPEN_FRAME (0)
#define SMR_DOOR_CLOSE_FRAME (1)
#define SMR_DOOR_WIDTH (32)
#define SMR_DOOR_HEIGHT (32)
#define SMR_DOOR_COLLIDE_DISTANCE (8)

typedef enum {
    SMR_DOOR_STATE_CLOSE = 0,
    SMR_DOOR_STATE_OPEN = 1
} smr_door_state;

typedef struct {
  t_sprites* sprite;
  smr_door_state state;
  s16 x;
  s16 y;
} smr_door;

#define SMR_KEY_WIDTH (16)
#define SMR_KEY_HEIGHT (16)
#define SMR_KEY_COLLIDE_DISTANCE (4)

typedef enum {
    SMR_KEY_STATE_HIDDEN = 0,
    SMR_KEY_STATE_IDLE = 1,
    SMR_KEY_PICKED_UP = 2
} smr_key_state;

typedef struct {
  u16 oam_id;
  s16 x;
  s16 y;
  smr_key_state state;
} smr_key;

extern smr_door door;
extern smr_key key;

extern void smr_reset_door(s16 doorX, s16 doorY, s16 keyX, s16 keyY);
extern void smr_reveal_key();
extern void smr_door_update();

#endif