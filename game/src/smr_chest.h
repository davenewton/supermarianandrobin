#ifndef SMR_CHEST_H
#define SMR_CHEST_H

#include "snes/sprite.h"
#include <snes.h>

#define SMR_CHEST_WIDTH (32)
#define SMR_CHEST_HEIGHT (32)
#define SMR_CHEST_FALL_SPEED (2)
#define SMR_CHEST_COLLIDE_DISTANCE (8)

typedef enum {
  SMR_CHEST_STATE_INACTIVE = 0,
  SMR_CHEST_STATE_FALL = 1,
  SMR_CHEST_STATE_OPEN = 2
} smr_chest_state;

typedef struct {
  t_sprites *sprite;
  s16 x;
  s16 y;
  smr_chest_state state;
} smr_chest;

extern void smr_add_chest(s16 x, s16 y);
extern void smr_chest_set_fall();
extern void smr_update_chest();

#endif