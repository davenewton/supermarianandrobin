#include "smr_boar.h"
#include "smr_boars_rom_data.h"
#include "smr_character.h"
#include "smr_lookup_tables.h"
#include "smr_oam.h"
#include "smr_palette.h"
#include "smr_world.h"
#include "snes/snestypes.h"
#include "snes/sprite.h"

extern u8 boar_head;
extern u8 boar_spit;

#define SMR_BOAR_SPIT_SPEED (1)

smr_boar boars[SMR_MAX_BOARS] = {
    {.head_oam_id = SMR_OAM_ID(24),
     .spits = { {.oam_id = SMR_OAM_ID(25)}, {.oam_id = SMR_OAM_ID(26)}, {.oam_id = SMR_OAM_ID(27)}, {.oam_id = SMR_OAM_ID(28)}},
     .active = false},
    {.head_oam_id = SMR_OAM_ID(29),
     .spits = { {.oam_id = SMR_OAM_ID(30)}, {.oam_id = SMR_OAM_ID(31)}, {.oam_id = SMR_OAM_ID(32)}, {.oam_id = SMR_OAM_ID(33)}},
     .active = false}};

void smr_reset_boars() {
  dmaCopySpr16Vram(&boar_head, SMR_VRAM_SPRITE_16_TILE_ADDRESS(0x1080));
  dmaCopySpr16Vram(&boar_head + 64, SMR_VRAM_SPRITE_16_TILE_ADDRESS(0x10A0));
  dmaCopySpr16Vram(&boar_head + 128, SMR_VRAM_SPRITE_16_TILE_ADDRESS(0x10C0));
  dmaCopySpr16Vram(&boar_spit, SMR_VRAM_SPRITE_16_TILE_ADDRESS(0x10E0));

  int b;
  for (b = 0; b < SMR_MAX_BOARS; ++b) {
    smr_boar *boar = &boars[b];
    boar->active = false;
    oamSetEx(boar->head_oam_id, OBJ_SMALL, OBJ_HIDE);
    oamSetAttr(boar->head_oam_id, -100, 0, SMR_VRAM_SPRITE_16_TILE_INDEX(8), 0b00110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK);
    int s;
    for (s = 0; s < SMR_BOAR_NB_SPITS; ++s) {
      smr_boar_spit* spit = &boar->spits[s];
      spit->active = false;
      oamSetEx(spit->oam_id, OBJ_SMALL, OBJ_HIDE);
      oamSetAttr(spit->oam_id, -100, 0, SMR_VRAM_SPRITE_16_TILE_INDEX(14), 0b00110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK);
    }
  }
}

void smr_add_boar(smr_boar_rom_data *rom_data) {
  int b;
  for (b = 0; b < SMR_MAX_BOARS; ++b) {
    smr_boar* boar = &boars[b];
    if (!boar->active) {
      boar->ticks = rom_data->ticks_before_first_spit;
      boar->ticks_between_spits = rom_data->ticks_between_spits;
      boar->active = true;
      boar->head_x = rom_data->x;
      boar->head_y = rom_data->y;
      u16 oam_id = boar->head_oam_id;
      oamSetAttr(oam_id, -100, 0, SMR_VRAM_SPRITE_16_TILE_INDEX(8), 0b00110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK);
      oamSetEx(oam_id, OBJ_SMALL, OBJ_SHOW);
      oamSetXY(oam_id, boar->head_x, boar->head_y);
      return;
    }
  }
}

void smr_boar_spit_one(smr_boar *boar) {
  int s;
  for (s = 0; s < SMR_BOAR_NB_SPITS; ++s) {
    smr_boar_spit* spit = &boar->spits[s];
    if (!spit->active) {
      spit->active = true;
      spit->x = boar->head_x + SMR_BOAR_HEAD_WIDTH / 2 - SMR_BOAR_HEAD_WIDTH / 2;
      spit->y = boar->head_y + SMR_BOAR_HEAD_HEIGHT / 2;
      oamSetEx(spit->oam_id, OBJ_SMALL, OBJ_SHOW);
      return;
    }
  }
}

void smr_add_boars(smr_boars_rom_data *rom_data) {
  int b;
  for (b = 0; b < rom_data->nb_boars; ++b) {
    smr_add_boar(&rom_data->boars[b]);
  }
}

void smr_boars_collide(smr_character *character) {
  int b;
  s16 character_center_x = character->x + SMR_CHARACTER_WIDTH / 2;
  s16 character_center_y = character->y + SMR_CHARACTER_WIDTH / 2;
  for (b = 0; b < SMR_MAX_BOARS; ++b) {
    smr_boar *boar = &boars[b];
    if (!boar->active) {
      continue;
    }
    int s;
    for (s = 0; s < SMR_BOAR_NB_SPITS; ++s) {
    smr_boar_spit *spit = &boar->spits[s];
      if (!spit->active) {
        continue;
      }
      s16 spit_left = spit->x;
      if (character_center_x > spit_left && character_center_x < (spit_left + 16)) {
        s16 spit_top = spit->y;
        if (character_center_y > spit_top && character_center_y < (spit_top + 16)) {
          smr_character_kill(character);
          return;
        }
      }
    }
  }
}

void smr_update_boar(smr_boar *boar) {
  if (!boar->active) {
    return;
  }
  // open mouth progressivly
  u16 head_oam_id = boar->head_oam_id;
  if (boar->ticks == (boar->ticks_between_spits / 2)) {
    oamSetAttr(head_oam_id, boar->head_x, boar->head_y, SMR_VRAM_SPRITE_16_TILE_INDEX(10), 0b00110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK);
    boar->ticks -= 1;
  } else if (boar->ticks == (boar->ticks_between_spits / 4)) {
    oamSetAttr(head_oam_id, boar->head_x, boar->head_y, SMR_VRAM_SPRITE_16_TILE_INDEX(12), 0b00110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK);
    boar->ticks -= 1;
  } else if (boar->ticks == 0) {
    oamSetAttr(head_oam_id, boar->head_x, boar->head_y, SMR_VRAM_SPRITE_16_TILE_INDEX(8), 0b00110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK);
    boar->ticks = boar->ticks_between_spits;
    smr_boar_spit_one(boar);
  }
  --boar->ticks;
  int s;
  for (s = 0; s < SMR_BOAR_NB_SPITS; ++s) {
    smr_boar_spit *spit = &boar->spits[s];
    if (spit->active) {
      spit->y += SMR_BOAR_SPIT_SPEED;
      if (spit->y >= SMR_WORLD_BOUNDS_BOTTOM) {
        spit->active = false;
        oamSetEx(spit->oam_id, OBJ_SMALL, OBJ_HIDE);
      }
      oamSetXY(spit->oam_id, spit->x, spit->y);
    }
  }
}

extern void smr_update_boars() {
  int f;
  for (f = 0; f < SMR_MAX_BOARS; ++f) {
    smr_update_boar(&boars[f]);
  }
}