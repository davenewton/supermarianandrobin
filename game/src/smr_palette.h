#ifndef SMR_PALETTE_H
#define SMR_PALETTE_H

#include <stdint.h>

#define SMR_PALETTE_SPRITE_INDEX (4)
#define SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK (4 << 1)
#define SMR_RED_PALETTE_SPRITE_INDEX (5)
#define SMR_RED_PALETTE_SPRITE_OAMATTRIBUTE_MASK (5 << 1)

// Palette in ROM
extern uint8_t smr_palette;
extern uint8_t smr_red_palette;

#endif