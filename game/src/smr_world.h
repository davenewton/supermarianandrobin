#ifndef SMR_WORLD_H
#define SMR_WORLD_H

#include "smr_platform_rom_data.h"
#include <snes.h>

#define SMR_WORLD_BOUNDS_TOP (0)
#define SMR_WORLD_BOUNDS_LEFT (0)
#define SMR_WORLD_BOUNDS_BOTTOM (224)
#define SMR_WORLD_BOUNDS_RIGHT (256)
#define SMR_WORLD_WIDTH (SMR_WORLD_BOUNDS_RIGHT-SMR_WORLD_BOUNDS_LEFT)
#define SMR_WORLD_CENTER_X ((SMR_WORLD_BOUNDS_LEFT+SMR_WORLD_BOUNDS_RIGHT)/2)
#define SMR_WORLD_GRAVITY (1)

typedef struct {
    smr_platforms_rom_data* platforms;
    bool wrap_x;
    bool wrap_y;
    bool shooting_allowed;
} smr_world;

extern smr_world world;

#endif