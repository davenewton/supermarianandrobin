#include "smr_guard.h"
#include "smr_animation.h"
#include "smr_arrow.h"
#include "smr_character.h"
#include "smr_oam.h"
#include "smr_palette.h"
#include "smr_world.h"

extern smr_animation_rom_data guard_appear;
extern smr_animation_rom_data guard_attack;
extern smr_animation_rom_data guard_idle;
extern smr_animation_rom_data guard_ghost;
extern smr_animation_rom_data guard_walk;

smr_guard guard =
    {.sprite = SMR_OAMBUFFER_ID_TO_PTR(11),
     .min_x = SMR_WORLD_BOUNDS_LEFT,
     .max_x = SMR_WORLD_BOUNDS_RIGHT - SMR_GUARD_WIDTH};

void make_guard_appear() {
  u16 position_index = rand() % guard.rom_data->nb_positions;
  smr_guard_position_rom_data *position = &guard.rom_data->positions[position_index];

  guard.state = SMR_GUARD_STATE_IDLE;
  guard.arrow_stucked = 0;
  guard.ticks = 0;
  guard.x = position->x;
  guard.y = position->y;
  guard.sprite->oamattribute = 0b00110000 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK; // sprite 32x32 and priority 3
  guard.target_character = NULL;
  guard.state = SMR_GUARD_STATE_APPEAR;
  smr_streamed_animation_start(&guard.animation, &guard_appear, guard.sprite);
}

void smr_reset_guard(smr_guard_rom_data *rom_data) {

  guard.rom_data = rom_data;
  guard.remaining_life = rom_data->nb_life;
  if (guard.remaining_life > 0) {
    make_guard_appear();
  }
}

smr_character *get_nearest_character_in_line_with_guard() {
  smr_character *nearest_character = 0;
  s16 nearest_character_horizontal_distance = SMR_WORLD_BOUNDS_RIGHT - SMR_WORLD_BOUNDS_LEFT;
  s16 guard_center_x = guard.x + SMR_GUARD_WIDTH / 2;
  s16 guard_center_y = guard.y + SMR_GUARD_HEIGHT / 2;

  int ch;
  for (ch = 0; ch < SMR_CHARACTER_TYPE_COUNT; ++ch) {
    smr_character *character = &characters[ch];
    if (!character->active) {
      continue;
    }
    s16 character_top = character->y;
    if (guard_center_y > character_top) {
      s16 character_bottom = character->y + SMR_CHARACTER_HEIGHT;
      if (guard_center_y < character_bottom) {
        s16 horizontal_distance = guard_center_x - (character->x + SMR_CHARACTER_WIDTH / 2);
        if (horizontal_distance < 0) {
          horizontal_distance = -horizontal_distance;
        }
        if (NULL == nearest_character || (horizontal_distance < nearest_character_horizontal_distance)) {
          nearest_character = character;
          nearest_character_horizontal_distance = horizontal_distance;
        }
      }
    }
  }
  return nearest_character;
}

void collides_guard_with_arrows() {
  int a;
  for (a = 0; a < SMR_MAX_ARROWS; ++a) {
    smr_arrow *arrow = &arrows[a];
    if (arrow->tick >= SMR_ARROW_MAX_TICKS) {
      continue;
    }
    s16 arrow_tip_x = arrow->is_looking_left ? arrow->x : arrow->x + SMR_ARROW_WIDTH;
    s16 guard_left = guard.x;
    if (arrow_tip_x > guard_left) {
      s16 guard_right = guard.x + SMR_GUARD_WIDTH;
      if (arrow_tip_x < guard_right) {
        s16 arrow_tip_y = arrow->y + SMR_ARROW_HEIGHT / 2;
        s16 guard_top = guard.y;
        if (arrow_tip_y > guard_top) {
          s16 guard_bottom = guard.y + SMR_GUARD_HEIGHT;
          if (arrow_tip_y < guard_bottom) {
            guard.state = SMR_GUARD_STATE_HIT_BY_ARROW;
            guard.arrow_stucked = arrow;
            return;
          }
        }
      }
    }
  }
}

void smr_update_guard() {
  switch (guard.state) {
  case SMR_GUARD_STATE_INACTIVE: {
    return;
  }
  case SMR_GUARD_STATE_APPEAR: {
    if (guard.animation.loop_count >= 1) {
      smr_streamed_animation_start(&guard.animation, &guard_idle, guard.sprite);
      guard.state = SMR_GUARD_STATE_IDLE;
    }
    break;
  }
  case SMR_GUARD_STATE_IDLE: {
    if (guard.ticks > 60) {
      smr_character *nearest_character = get_nearest_character_in_line_with_guard();
      if (nearest_character) {
        smr_streamed_animation_start(&guard.animation, &guard_walk, guard.sprite);
        guard.target_character = nearest_character;
        guard.state = SMR_GUARD_STATE_WALK;
      }
    } else {
      ++guard.ticks;
    }
    collides_guard_with_arrows();
    break;
  }
  case SMR_GUARD_STATE_WALK: {
    s16 dx = guard.x + SMR_GUARD_WIDTH / 2 - (guard.target_character->x + SMR_CHARACTER_WIDTH / 2);
    if (dx > 0) {
      guard.sprite->oamattribute |= 0b01000000;
      if (dx < SMR_GUARD_ATTACK_FROM_RIGHT_DISTANCE) {
        smr_streamed_animation_start(&guard.animation, &guard_attack, guard.sprite);
        guard.state = SMR_GUARD_STATE_ATTACK;
      } else {
        guard.x -= SMR_GUARD_WALK_SPEED;
      }
    } else {
      guard.sprite->oamattribute &= 0b10111111;
      if (dx > -SMR_GUARD_ATTACK_FROM_LEFT_DISTANCE) {
        smr_streamed_animation_start(&guard.animation, &guard_attack, guard.sprite);
        guard.state = SMR_GUARD_STATE_ATTACK;
      } else {
        guard.x += SMR_GUARD_WALK_SPEED;
      }
    }
    collides_guard_with_arrows();
    break;
  }
  case SMR_GUARD_STATE_ATTACK: {
    if (guard.animation.loop_count >= 1) {
      guard.ticks = 0;
      smr_streamed_animation_start(&guard.animation, &guard_idle, guard.sprite);
      guard.state = SMR_GUARD_STATE_IDLE;
      guard.target_character = NULL;
    } else {
      switch (guard.sprite->oamframeid) {
      case 1:
      case 4: {
        smr_character *target_character = guard.target_character;
        s16 guard_center_x = guard.x + SMR_GUARD_WIDTH / 2;
        s16 guard_center_y = guard.y + SMR_GUARD_HEIGHT / 2;
        s16 character_top = target_character->y;
        if (guard_center_y > character_top) {
          s16 character_bottom = target_character->y + SMR_CHARACTER_HEIGHT;
          if (guard_center_y < character_bottom) {
            s16 character_left = target_character->x;
            s16 spearX = (guard.sprite->oamattribute & 0b01000000) ? guard.x : guard.x + SMR_GUARD_WIDTH;
            if (spearX >= character_left && spearX <= (character_left + SMR_CHARACTER_WIDTH)) {
              smr_character_kill(target_character);
            }
          }
          break;
        }
      }
      }
    }
    collides_guard_with_arrows();
    break;
  }
  case SMR_GUARD_STATE_HIT_BY_ARROW: {
    smr_arrow* arrow_stucked = guard.arrow_stucked;
    if (arrow_stucked) {
      if (arrow_stucked->is_looking_left) { // TODO macro to detect if arrow is looking left macro or function
        guard.x = arrow_stucked->x - SMR_GUARD_WIDTH / 2;
      } else {
        guard.x = arrow_stucked->x + SMR_ARROW_WIDTH / 2 - SMR_GUARD_WIDTH / 2;
      }

      if (guard.arrow_stucked->tick >= SMR_ARROW_MAX_TICKS) {
        guard.arrow_stucked = NULL;
        guard.state = SMR_GUARD_STATE_DEAD;
        guard.dead_x = guard.sprite->oamx;
        guard.dead_velocity_x = SMR_GUARD_GHOST_SPEED;
        smr_streamed_animation_start(&guard.animation, &guard_ghost, guard.sprite);
      }
    } else {
      // TODO
    }
    break;
  }
  case SMR_GUARD_STATE_DEAD: {
    guard.x += guard.dead_velocity_x;
    if (guard.x > (guard.dead_x + SMR_GUARD_GHOST_HORIZONTAL_AMPLITUDE)) {
      guard.dead_velocity_x = -SMR_GUARD_GHOST_SPEED;
    } else if (guard.x < (guard.dead_x - SMR_GUARD_GHOST_HORIZONTAL_AMPLITUDE)) {
      guard.dead_velocity_x = SMR_GUARD_GHOST_SPEED;
    }
    guard.y -= SMR_GUARD_GHOST_SPEED;
    if (guard.y < (SMR_WORLD_BOUNDS_TOP - SMR_GUARD_HEIGHT)) {
      if (guard.remaining_life > 1) {
        --guard.remaining_life;
        make_guard_appear();
      } else {
        guard.state = SMR_GUARD_STATE_DEAD_AND_LOVING_IT;
      }
    }
    break;
  }
  case SMR_GUARD_STATE_DEAD_AND_LOVING_IT: {
    break;
  }
  }
  smr_streamed_animation_update(&guard.animation, guard.sprite);
  guard.sprite->oamx = guard.x;
  guard.sprite->oamy = guard.y;
  oamDynamic32Draw(guard.sprite - oambuffer);
}
