#include "smr_door.h"
#include "smr_character.h"
#include "smr_crossbowman.h"
#include "smr_guard.h"
#include "smr_oam.h"
#include "smr_palette.h"
#include "smr_sound.h"

extern u8 key_idle;
extern u8 door_open_close;

smr_door door = {.sprite = SMR_OAMBUFFER_ID_TO_PTR(30)};
smr_key key = {.oam_id = SMR_OAM_ID(5)};

void smr_reset_door(s16 doorX, s16 doorY, s16 keyX, s16 keyY) {
  door.sprite->oamgraphics = &door_open_close;
  door.sprite->oamframeid = SMR_DOOR_CLOSE_FRAME;
  door.sprite->oamrefresh = 1;
  door.sprite->oamx = doorX;
  door.sprite->oamy = doorY;
  door.sprite->oamattribute = 0b00110000 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK; // sprite 32x32 and priority 3;
  door.x = doorX;
  door.y = doorY;
  door.state = SMR_DOOR_STATE_CLOSE;

  dmaCopySpr16Vram(&key_idle, SMR_VRAM_SPRITE_16_TILE_ADDRESS(0x1060));
  oamSetEx(key.oam_id, OBJ_SMALL, OBJ_HIDE);
  oamSetAttr(key.oam_id, -100, 0, SMR_VRAM_SPRITE_16_TILE_INDEX(6), 0b00110001 | SMR_PALETTE_SPRITE_OAMATTRIBUTE_MASK);
  key.x = keyX;
  key.y = keyY;
  key.state = SMR_KEY_STATE_HIDDEN;
}

void smr_key_collide(smr_character *character) {
  s16 character_center_x = character->x + SMR_CHARACTER_WIDTH / 2;
  if (character_center_x >= key.x && character_center_x <= (key.x + SMR_KEY_WIDTH)) {
    s16 character_center_y = character->y + SMR_CHARACTER_HEIGHT / 2;
    if (character_center_y >= key.y && character_center_y <= (key.y + SMR_KEY_HEIGHT)) {
      smr_play_sound(SMR_SOUND_COLLECT);
      key.state = SMR_KEY_PICKED_UP;
      oamSetEx(key.oam_id, OBJ_SMALL, OBJ_HIDE);
      door.state = SMR_DOOR_STATE_OPEN;
      door.sprite->oamframeid = SMR_DOOR_OPEN_FRAME;
      door.sprite->oamrefresh = 1;
    }
  }
}

void smr_key_update() {
  switch (key.state) {
  case SMR_KEY_STATE_HIDDEN:
    break;
  case SMR_KEY_STATE_IDLE: {
    int ch;
    for (ch = 0; ch < SMR_CHARACTER_TYPE_COUNT; ++ch) {
      smr_character *character = &characters[ch];
      if (character->active) {
        smr_key_collide(character);
      }
    }
  } break;
  case SMR_KEY_PICKED_UP:
    break;
  }
}

void smr_reveal_key() {
  oamSetEx(key.oam_id, OBJ_SMALL, OBJ_SHOW);
  oamSetXY(key.oam_id, key.x, key.y);
  key.state = SMR_KEY_STATE_IDLE;
}

void smr_door_collide(smr_character *character) {
  s16 character_center_x = character->x + SMR_CHARACTER_WIDTH / 2;
  if (character_center_x >= (door.x + SMR_DOOR_COLLIDE_DISTANCE) && character_center_x <= (door.x + SMR_DOOR_WIDTH - SMR_DOOR_COLLIDE_DISTANCE)) {
    s16 character_center_y = character->y + SMR_CHARACTER_HEIGHT / 2;
    if (character_center_y >= door.y && character_center_y <= (door.y + SMR_DOOR_HEIGHT)) {
      smr_play_sound(SMR_SOUND_START);
      smr_character_set_out(character);
    }
  }
}

void smr_door_update() {
  switch (door.state) {
  case SMR_DOOR_STATE_CLOSE:
    smr_key_update();
    break;
  case SMR_DOOR_STATE_OPEN: {
    int ch;
    for (ch = 0; ch < SMR_CHARACTER_TYPE_COUNT; ++ch) {
      smr_character *character = &characters[ch];
      if (character->active && character->state == SMR_CHARACTER_STATE_MOVING) {
        smr_door_collide(character);
      }
    }
  } break;
  }
  oamDynamic32Draw(door.sprite - oambuffer);
}