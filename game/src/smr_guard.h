#ifndef SMR_GUARD_H
#define SMR_GUARD_H

#include "smr_animation.h"
#include "smr_guard_rom_data.h"
#include "smr_arrow.h"
#include "src/smr_character.h"
#include <snes.h>

#define SMR_GUARD_WIDTH (24)
#define SMR_GUARD_HEIGHT (32)
#define SMR_GUARD_WALK_SPEED (2)
#define SMR_GUARD_GHOST_HORIZONTAL_AMPLITUDE (16)
#define SMR_GUARD_GHOST_SPEED (2)
#define SMR_GUARD_ATTACK_FROM_LEFT_DISTANCE (20)
#define SMR_GUARD_ATTACK_FROM_RIGHT_DISTANCE (12)

typedef enum {
    SMR_GUARD_STATE_INACTIVE=0,
    SMR_GUARD_STATE_APPEAR=1,
    SMR_GUARD_STATE_IDLE=2,
    SMR_GUARD_STATE_WALK=3,
    SMR_GUARD_STATE_ATTACK=4,
    SMR_GUARD_STATE_HIT_BY_ARROW=5,
    SMR_GUARD_STATE_DEAD=6,
    SMR_GUARD_STATE_DEAD_AND_LOVING_IT=7,
} smr_guard_state;

typedef struct {
    smr_streamed_animation animation;
    t_sprites* sprite;
    smr_guard_state state;
    u16 ticks;
    smr_arrow* arrow_stucked;
    s16 dead_velocity_x;
    s16 dead_x;
    s16 x;
    s16 y;
    s16 min_x;
    s16 max_x;
    smr_guard_rom_data *rom_data;
    u8 remaining_life;
    smr_character* target_character;
} smr_guard;

extern smr_guard guard;
extern void smr_reset_guard(smr_guard_rom_data* rom_data);
extern void smr_update_guard();

#endif