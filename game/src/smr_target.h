#ifndef SMR_TARGET_H
#define SMR_TARGET_H

#include "smr_arrow.h"
#include "smr_animation.h"

typedef enum {
  SMR_TARGET_STATE_LAUNCHABLE=0,
  SMR_TARGET_STATE_FLY=1,
  SMR_TARGET_STATE_HIT_BY_ARROW=2,
  SMR_TARGET_STATE_POP=3,
  SMR_TARGET_STATE_FALL=4
} smr_target_state;

typedef struct {
  smr_streamed_animation balloon_animation;
  t_sprites* balloon_sprite;
  t_sprites* target_sprite;
  smr_target_state state;
  s16 x;
  s16 y;
  smr_arrow* arrow_stucked;
  bool is_looking_left;
} smr_target;

#define SMR_MAX_TARGET (4)
#define SMR_TARGET_WIDTH (16)
#define SMR_TARGET_HEIGHT (32)
#define SMR_TARGET_SPRITE_OFFSET_X (-8)
#define SMR_TARGET_SPRITE_OFFSET_Y (0)

#define SMR_TARGET_FLY_SPEED_Y (1)
#define SMR_TARGET_FALL_SPEED_Y (2)

#define SMR_BALLOON_WIDTH (32)
#define SMR_BALLOON_HEIGHT (32)
#define SMR_BALLOON_SPRITE_OFFSET_X (0)
#define SMR_BALLOON_SPRITE_OFFSET_Y (0)

extern u16 target_hits;
extern smr_target targets[SMR_MAX_TARGET];

extern void smr_reset_targets();
extern void smr_launch_one_target(bool is_looking_left);
extern void smr_update_targets();

#endif