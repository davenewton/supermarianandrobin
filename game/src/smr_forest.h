#ifndef SMR_FOREST_H
#define SMR_FOREST_H

typedef enum {
  SMR_FOREST_SCENE_OUTCOME_MARIAN_S_VICTORY = 0,
  SMR_FOREST_SCENE_OUTCOME_ROBIN_S_VICTORY = 1
} smr_forest_scene_outcome;

extern smr_forest_scene_outcome smr_scene_forest();

#endif