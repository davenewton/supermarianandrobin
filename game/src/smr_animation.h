#ifndef SMR_ANIMATION_H
#define SMR_ANIMATION_H

#include "snes/sprite.h"
#include <snes.h>

typedef struct {
    u8 frame_count __attribute__((packed));
    u8 tick_per_frame __attribute__((packed));
    u8 data[] __attribute__((packed));
} __attribute__((packed)) smr_animation_rom_data;

typedef struct {
  u8 tick_per_frame;
  u8 current_tick;
  u8 frame_count;
  u8 loop_count;
} smr_streamed_animation;

extern void smr_streamed_animation_start(smr_streamed_animation* animation, smr_animation_rom_data* rom_data, t_sprites* sprite);
extern void smr_streamed_animation_update(smr_streamed_animation *animation, t_sprites* sprite);

#endif