#ifndef SMR_TEXT_H
#define SMR_TEXT_H

#include <snes.h>

/*
  VMDATAH     VMDATAL
   $4119       $4118
15  bit  8   7  bit  0
 ---- ----   ---- ----
 VHPC CCTT   TTTT TTTT
 |||| ||||   |||| ||||
 |||| ||++---++++-++++- Tile index
 |||+-++--------------- Palette selection
 ||+------------------- Priority
 ++-------------------- Flip vertical (V) or horizontal (H)
*/

#define SMR_TEXT_MASK_WHITE 0b0010000000000000
#define SMR_TEXT_MASK_BROWN 0b0010010000000000
#define SMR_TEXT_MASK_YELLOW 0b0010100000000000
#define SMR_TEXT_MASK_BLACK 0b0011000000000000

typedef enum {
  SMR_TEXT_NUMBER_FORMAT_1_DIGIT = 1,
  SMR_TEXT_NUMBER_FORMAT_2_DIGITS = 10,
  SMR_TEXT_NUMBER_FORMAT_3_DIGITS = 100  
} smr_text_number_format;

extern void smr_text_init_bg3_tileset();
extern void smr_text_init_bg3_map();
extern void smr_text_clear();
extern void smr_text_print(u16 x, u16 y, u16 w, u16 h, u16 mask, const char *text);
extern void smr_text_print_number(u16 x, u16 y, u16 w, u16 h, u16 mask, u16 num, smr_text_number_format format);
extern void smr_text_update_vram();

#endif