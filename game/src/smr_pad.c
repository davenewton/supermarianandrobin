#include "smr_pad.h"
#include "snes/snestypes.h"

u16 pad_keys_was_not_pressed[2] = {0, 0 };

void smr_update_pads() {
  pad_keys_was_not_pressed[0] |= ~padsCurrent(0);
  pad_keys_was_not_pressed[1] |= ~padsCurrent(1);
}

bool smr_test_and_reset_if_pressed(u16 pad, u16 key) {
  if (pad_keys_was_not_pressed[pad] & padsCurrent(pad) & key) {
    pad_keys_was_not_pressed[pad] ^= key;
    return true;
  } else {
    return false;
  }
}

bool smr_test_if_confirm() {
  return smr_test_and_reset_if_pressed(0, KEY_START) || smr_test_and_reset_if_pressed(1, KEY_START) || 
    smr_test_and_reset_if_pressed(0, KEY_A) || smr_test_and_reset_if_pressed(1, KEY_A) || 
    smr_test_and_reset_if_pressed(0, KEY_B) || smr_test_and_reset_if_pressed(1, KEY_B);
}