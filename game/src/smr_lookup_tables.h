#ifndef SMR_LOOKUP_TABLES_H
#define SMR_LOOKUP_TABLES_H

#include "smr_lookup_tables_rom_data.h"

extern smr_lookup_path_point circular_path[SMR_LOOKUP_CIRCULAR_PATH_NB_POINTS];

#endif