#ifndef SMR_ADVENTURE_H
#define SMR_ADVENTURE_H

typedef enum {
  SMR_ADVENTURE_OUTCOME_LOSE = 0,
  SMR_ADVENTURE_OUTCOME_WIN = 1
} smr_adventure_outcome;


#endif