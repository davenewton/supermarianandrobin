#include "smr_animation.h"

void smr_streamed_animation_start(smr_streamed_animation* animation, smr_animation_rom_data* rom_data, t_sprites* sprite) {
  animation->current_tick = 0;
  animation->tick_per_frame = rom_data->tick_per_frame;
  animation->frame_count = rom_data->frame_count;
  animation->loop_count = 0;
  sprite->oamframeid = 0;
  if(sprite->oamgraphics != rom_data->data) {
    sprite->oamgraphics = rom_data->data;
    sprite->oamrefresh = 1;
  }
}

void smr_streamed_animation_update(smr_streamed_animation *animation, t_sprites* sprite) {
  ++animation->current_tick;
  if (animation->current_tick > animation->tick_per_frame) {
    animation->current_tick = 0;
    u16 new_oamframeid = sprite->oamframeid;
    u16 old_oamframeid = new_oamframeid++;
    if (new_oamframeid >= animation->frame_count) {
      new_oamframeid = 0;
      ++animation->loop_count;
    }
    if(new_oamframeid != old_oamframeid) {
      sprite->oamframeid = new_oamframeid;
      sprite->oamrefresh = 1;
    }
  }
}