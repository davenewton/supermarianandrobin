.include "hdr.asm"

.section ".rodata1" superfree

circular_path:
.incbin "build/assets/circular_lookup_table"

font_tileset:
.incbin "build/assets/menus/font/font_tileset.pic"
font_tileset_end:

title_tileset:
.incbin "build/assets/menus/title/title_tileset.pic"
title_tileset_end:

smr_palette:
.incbin "build/assets/menus/title/title_tileset.pal"

.macro redify_palette
.redefine _out \1 | 31
.endm

smr_red_palette:
.incbin "build/assets/menus/title/title_tileset.pal" FILTER redify_palette FILTERSIZE 2

.ends

.section ".rodata2" superfree

title_bg1:
.incbin "build/assets/menus/title/title_bg1"
title_bg1_end:

title_bg2:
.incbin "build/assets/menus/title/title_bg2"
title_bg2_end:

.ends

.section ".rodata3" superfree

.STRUCT dialog
marian_line dl
padding0 db
robin_line dl
padding1 db
choice1 dl
padding2 db
choice2 dl
padding3 db
choice3 dl
padding4 db
.ENDST

dialog_character_select_marian_line:
.db "DO YOU WANT TO PLAY   WITH ME ROBIN ?", 0

dialog_character_select_robin_line:
.db "AS YOU WISH MARIAN.", 0

dialog_character_choice1:
.db "ONLY MARIAN PAD 1", 0

dialog_character_choice2:
.db "ONLY ROBIN PAD 2", 0

dialog_character_choice3:
.db "PLAY WITH BOTH", 0

.DSTRUCT dialog_character_select INSTANCEOF dialog VALUES
marian_line: .dl dialog_character_select_marian_line
padding0: .db 0
robin_line: .dl dialog_character_select_robin_line
padding1: .db 0
choice1: .dl dialog_character_choice1
padding2: .db 0
choice2: .dl dialog_character_choice2
padding3: .db 0
choice3: .dl dialog_character_choice3
padding4: .db 0
.ENDST

dialog_activity_select_marian_line:
.db "WHAT SHOULD WE DO     TODAY ?", 0

dialog_activity_select_robin_line:
.db "LET ME SEE THE        SCRIPT !", 0

dialog_activity_choice1:
.db "ROB CASTLE", 0

dialog_activity_choice2:
.db "COMPETE IN TOURNAMENT", 0

dialog_activity_choice3:
.db "SHOOT EACH OTHER", 0

.DSTRUCT dialog_activity_select INSTANCEOF dialog VALUES
marian_line: .dl dialog_activity_select_marian_line
padding0: .db 0
robin_line: .dl dialog_activity_select_robin_line
padding1: .db 0
choice1: .dl dialog_activity_choice1
padding2: .db 0
choice2: .dl dialog_activity_choice2
padding3: .db 0
choice3: .dl dialog_activity_choice3
padding4: .db 0
.ENDST

dialog_mariansvictory_marian_line:
.db "I WON !", 0

dialog_mariansvictory_robin_line:
.db "WAIT A MINUTE I AM NOTSUPPOSE TO LOSE !", 0

dialog_mariansvictory_choice1:
.db "FIGHT AGAIN", 0

dialog_mariansvictory_choice2:
.db "DO SOMETHING ELSE", 0

dialog_mariansvictory_choice3:
.db "EXIT", 0

.DSTRUCT dialog_mariansvictory INSTANCEOF dialog VALUES
marian_line: .dl dialog_mariansvictory_marian_line
padding0: .db 0
robin_line: .dl dialog_mariansvictory_robin_line
padding1: .db 0
choice1: .dl dialog_mariansvictory_choice1
padding2: .db 0
choice2: .dl dialog_mariansvictory_choice2
padding3: .db 0
choice3: .dl dialog_mariansvictory_choice3
padding4: .db 0
.ENDST

dialog_robinsvictory_marian_line:
.db "I LOSE ! IT S NOT     FAIR !", 0

dialog_robinsvictory_robin_line:
.db "WHO SAYS LIFE IS      FAIR ?", 0

dialog_robinsvictory_choice1:
.db "PLAY AGAIN", 0

dialog_robinsvictory_choice2:
.db "DO SOMETHING ELSE", 0

dialog_robinsvictory_choice3:
.db "EXIT", 0

.DSTRUCT dialog_robinsvictory INSTANCEOF dialog VALUES
marian_line: .dl dialog_robinsvictory_marian_line
padding0: .db 0
robin_line: .dl dialog_robinsvictory_robin_line
padding1: .db 0
choice1: .dl dialog_robinsvictory_choice1
padding2: .db 0
choice2: .dl dialog_robinsvictory_choice2
padding3: .db 0
choice3: .dl dialog_robinsvictory_choice3
padding4: .db 0
.ENDST

dialog_castlebadending_marian_line:
.db "WE ARE DEAD ROBIN !", 0

dialog_castlebadending_robin_line:
.db "YES AND LOVING IT !", 0

dialog_castlebadending_choice1:
.db "TRY AGAIN", 0

dialog_castlebadending_choice2:
.db "DO SOMETHING ELSE", 0

dialog_castlebadending_choice3:
.db "EXIT", 0

.DSTRUCT dialog_castlebadending INSTANCEOF dialog VALUES
marian_line: .dl dialog_castlebadending_marian_line
padding0: .db 0
robin_line: .dl dialog_castlebadending_robin_line
padding1: .db 0
choice1: .dl dialog_castlebadending_choice1
padding2: .db 0
choice2: .dl dialog_castlebadending_choice2
padding3: .db 0
choice3: .dl dialog_castlebadending_choice3
padding4: .db 0
.ENDST

dialog_castlegoodending_marian_line:
.db "WE HAVE SO MUCH TO    GIVE TO THE POOR !", 0

dialog_castlegoodending_robin_line:
.db "EXCEPT WHAT WE KEEP   FOR EXPENSES.", 0

dialog_castlegoodending_choice1:
.db "ROB AGAIN", 0

dialog_castlegoodending_choice2:
.db "DO SOMETHING ELSE", 0

dialog_castlegoodending_choice3:
.db "EXIT", 0

.DSTRUCT dialog_castlegoodending INSTANCEOF dialog VALUES
marian_line: .dl dialog_castlegoodending_marian_line
padding0: .db 0
robin_line: .dl dialog_castlegoodending_robin_line
padding1: .db 0
choice1: .dl dialog_castlegoodending_choice1
padding2: .db 0
choice2: .dl dialog_castlegoodending_choice2
padding3: .db 0
choice3: .dl dialog_castlegoodending_choice3
padding4: .db 0
.ENDST

dialog_bg1:
.incbin "build/assets/menus/dialog/dialog_bg1"
dialog_bg1_end:

dialog_bg2:
.incbin "build/assets/menus/dialog/dialog_bg2"
dialog_bg2_end:

dialog_tileset:
.incbin "build/assets/menus/dialog/dialog_tileset.pic"
dialog_tileset_end:

.ends

.section ".rodata4" superfree

forest_tileset:
.incbin "build/assets/levels/forest/forest_tileset.pic"
forest_tileset_end:

.ends

.section ".rodata5" superfree

forest_bg1:
.incbin "build/assets/levels/forest/forest_bg1"
forest_bg1_end:

forest_bg2:
.incbin "build/assets/levels/forest/forest_bg2"
forest_bg2_end:

forest_header_data:
.incbin "build/assets/levels/forest/forest_header_data"

forest_platforms_data:
.incbin "build/assets/levels/forest/forest_platforms_data"

.ends

.section ".rodata6" superfree

.STRUCT animation
frame_count    db
tick_per_frame db
.ENDST

arrow_fly:
.incbin "build/assets/sprites/16/arrow_fly.pic"
arrow_fly_end:

.DSTRUCT robin_ghost INSTANCEOF animation VALUES
    frame_count:    .db 1
    tick_per_frame: .db 60
.ENDST
.incbin "build/assets/sprites/32/robin_ghost.pic"

.DSTRUCT robin_idle INSTANCEOF animation VALUES
    frame_count:    .db 2
    tick_per_frame: .db 60
.ENDST
.incbin "build/assets/sprites/32/robin_idle.pic"

.DSTRUCT robin_ohai INSTANCEOF animation VALUES
    frame_count:    .db 2
    tick_per_frame: .db 30
.ENDST
.incbin "build/assets/sprites/32/robin_ohai.pic"

.DSTRUCT robin_shoot INSTANCEOF animation VALUES
    frame_count:    .db 3
    tick_per_frame: .db 10
.ENDST
.incbin "build/assets/sprites/32/robin_shoot.pic"

.DSTRUCT robin_walk INSTANCEOF animation VALUES
    frame_count:    .db 4
    tick_per_frame: .db 5
.ENDST
.incbin "build/assets/sprites/32/robin_walk.pic"

.DSTRUCT marian_ghost INSTANCEOF animation VALUES
    frame_count:    .db 1
    tick_per_frame: .db 60
.ENDST
.incbin "build/assets/sprites/32/marian_ghost.pic"

.DSTRUCT marian_idle INSTANCEOF animation VALUES
    frame_count:    .db 2
    tick_per_frame: .db 60
.ENDST
.incbin "build/assets/sprites/32/marian_idle.pic":

.DSTRUCT marian_ohai INSTANCEOF animation VALUES
    frame_count:    .db 2
    tick_per_frame: .db 30
.ENDST
.incbin "build/assets/sprites/32/marian_ohai.pic"

.DSTRUCT marian_shoot INSTANCEOF animation VALUES
    frame_count:    .db 3
    tick_per_frame: .db 10
.ENDST
.incbin "build/assets/sprites/32/marian_shoot.pic"

.DSTRUCT marian_walk INSTANCEOF animation VALUES
    frame_count:    .db 4
    tick_per_frame: .db 5
.ENDST
.incbin "build/assets/sprites/32/marian_walk.pic"

.ends

.section ".rodata7" superfree

tournament_tileset:
.incbin "build/assets/levels/tournament/tournament_tileset.pic"
tournament_tileset_end:

.ends

.section ".rodata8" superfree

.DSTRUCT balloon_fly INSTANCEOF animation VALUES
    frame_count:    .db 1
    tick_per_frame: .db 60
.ENDST
.incbin "build/assets/sprites/32/balloon_fly.pic"

.DSTRUCT balloon_pop INSTANCEOF animation VALUES
    frame_count:    .db 3
    tick_per_frame: .db 30
.ENDST
.incbin "build/assets/sprites/32/balloon_pop.pic"

.DSTRUCT target_fly INSTANCEOF animation VALUES
    frame_count:    .db 1
    tick_per_frame: .db 60
.ENDST
.incbin "build/assets/sprites/32/target_fly.pic"

tournament_bg1:
.incbin "build/assets/levels/tournament/tournament_bg1"
tournament_bg1_end:

tournament_bg2:
.incbin "build/assets/levels/tournament/tournament_bg2"
tournament_bg2_end:

tournament_header_data:
.incbin "build/assets/levels/tournament/tournament_header_data"

tournament_platforms_data:
.incbin "build/assets/levels/tournament/tournament_platforms_data"

.ends

.section ".rodata9" superfree

door_open_close:
.incbin "build/assets/sprites/32/door_open_close.pic"

key_idle:
.incbin "build/assets/sprites/16/key_idle.pic"

flail_ball:
.incbin "build/assets/sprites/16/flail_ball.pic"

flail_link:
.incbin "build/assets/sprites/16/flail_link.pic"

boar_head:
.incbin "build/assets/sprites/16/boar_head.pic"

boar_spit:
.incbin "build/assets/sprites/16/boar_spit.pic"

axe_head:
.incbin "build/assets/sprites/16/axe_head.pic"

axe_handle:
.incbin "build/assets/sprites/16/axe_handle.pic"

.ends

.section ".rodata10" superfree

castle_tileset:
.incbin "build/assets/levels/castle/castle_tileset.pic"
castle_tileset_end:

.STRUCT castle_level
header dl
padding0 db
bg1_size dw
bg1 dl
padding1 db
bg2_size dw
bg2 dl
padding2 db
platforms dl
padding3 db
flails dl
padding4 db
boars dl
padding5 db
axes dl
padding6 db
crossbowman dl
padding7 db
guards dl
padding8 db
.ENDST

.REPEAT 5 INDEX level_index

castle{%.2i{level_index+1}}_header:
.incbin {"build/assets/levels/castle/castle{%.2i{level_index+1}}_header_data"}

castle{%.2i{level_index+1}}_bg1:
.incbin {"build/assets/levels/castle/castle{%.2i{level_index+1}}_bg1"} FSIZE castle{%.2i{level_index+1}}_bg1_size

castle{%.2i{level_index+1}}_bg2:
.incbin {"build/assets/levels/castle/castle{%.2i{level_index+1}}_bg2"} FSIZE castle{%.2i{level_index+1}}_bg2_size

castle{%.2i{level_index+1}}_platforms:
.incbin {"build/assets/levels/castle/castle{%.2i{level_index+1}}_platforms_data"}

castle{%.2i{level_index+1}}_flails:
.incbin {"build/assets/levels/castle/castle{%.2i{level_index+1}}_flails_data"}

castle{%.2i{level_index+1}}_boars:
.incbin {"build/assets/levels/castle/castle{%.2i{level_index+1}}_boars_data"}

castle{%.2i{level_index+1}}_axes:
.incbin {"build/assets/levels/castle/castle{%.2i{level_index+1}}_axes_data"}

castle{%.2i{level_index+1}}_crossbowman:
.incbin {"build/assets/levels/castle/castle{%.2i{level_index+1}}_crossbowman_data"}

castle{%.2i{level_index+1}}_guards:
.incbin {"build/assets/levels/castle/castle{%.2i{level_index+1}}_guard_data"}

.ENDR

castle_levels:
.REPEAT 5 INDEX level_index
.DSTRUCT castle{%.2i{level_index+1}} INSTANCEOF castle_level VALUES
header: .dl castle{%.2i{level_index+1}}_header
padding0: .db 0
bg1_size: .dw castle01_bg1_size
bg1: .dl castle{%.2i{level_index+1}}_bg1
padding1: .db 0
bg2_size: .dw castle{%.2i{level_index+1}}_bg2_size
bg2: .dl castle{%.2i{level_index+1}}_bg2
padding2: .db 0
platforms: .dl castle{%.2i{level_index+1}}_platforms
padding3: .db 0
flails: .dl castle{%.2i{level_index+1}}_flails
padding4: .db 0
boars: .dl castle{%.2i{level_index+1}}_boars
padding5: .db 0
axes: .dl castle{%.2i{level_index+1}}_axes
padding6: .db 0
crossbowman: .dl castle{%.2i{level_index+1}}_crossbowman
padding7: .db 0
guards: .dl castle{%.2i{level_index+1}}_guards
padding8: .db 0
.ENDST

.ENDR

.ends

.section ".rodata11" superfree

.DSTRUCT crossbowman_appear INSTANCEOF animation VALUES
    frame_count:    .db 5
    tick_per_frame: .db 10
.ENDST
.incbin "build/assets/sprites/32/crossbowman_appear.pic"

.DSTRUCT crossbowman_shoot INSTANCEOF animation VALUES
    frame_count:    .db 5
    tick_per_frame: .db 10
.ENDST
.incbin "build/assets/sprites/32/crossbowman_shoot.pic"

.DSTRUCT crossbowman_ghost INSTANCEOF animation VALUES
    frame_count:    .db 1
    tick_per_frame: .db 60
.ENDST
.incbin "build/assets/sprites/32/crossbowman_ghost.pic"

.DSTRUCT crossbowman_idle INSTANCEOF animation VALUES
    frame_count:    .db 2
    tick_per_frame: .db 60
.ENDST
.incbin "build/assets/sprites/32/crossbowman_idle.pic"

.DSTRUCT guard_appear INSTANCEOF animation VALUES
    frame_count:    .db 5
    tick_per_frame: .db 10
.ENDST
.incbin "build/assets/sprites/32/guard_appear.pic"

.DSTRUCT guard_walk INSTANCEOF animation VALUES
    frame_count:    .db 4
    tick_per_frame: .db 5
.ENDST
.incbin "build/assets/sprites/32/guard_walk.pic"

.DSTRUCT guard_attack INSTANCEOF animation VALUES
    frame_count:    .db 6
    tick_per_frame: .db 15
.ENDST
.incbin "build/assets/sprites/32/guard_attack.pic"

.DSTRUCT guard_ghost INSTANCEOF animation VALUES
    frame_count:    .db 1
    tick_per_frame: .db 60
.ENDST
.incbin "build/assets/sprites/32/guard_ghost.pic"

.DSTRUCT guard_idle INSTANCEOF animation VALUES
    frame_count:    .db 2
    tick_per_frame: .db 60
.ENDST
.incbin "build/assets/sprites/32/guard_idle.pic"

.ends

.section ".rodata12" superfree

treasure_room_tileset:
.incbin "build/assets/levels/treasure_room/treasure_room_tileset.pic"
treasure_room_tileset_end:

treasure_room_bg1:
.incbin "build/assets/levels/treasure_room/treasure_room_bg1"
treasure_room_bg1_end:

treasure_room_bg2:
.incbin "build/assets/levels/treasure_room/treasure_room_bg2"
treasure_room_bg2_end:

treasure_room_header_data:
.incbin "build/assets/levels/treasure_room/treasure_room_header_data"

treasure_room_platforms_data:
.incbin "build/assets/levels/treasure_room/treasure_room_platforms_data"

king_attacks:
.incbin "build/assets/sprites/32/king_attacks.pic"

king_cry:
.incbin "build/assets/sprites/32/king_cry.pic"

king_scepter:
.incbin "build/assets/sprites/16/king_scepter.pic"

king_flail_ball:
.incbin "build/assets/sprites/16/king_flail_ball.pic"

king_flail_link:
.incbin "build/assets/sprites/16/king_flail_link.pic"

chest_open_close:
.incbin "build/assets/sprites/32/chest_open_close.pic"

.ends
