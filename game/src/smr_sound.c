#include "smr_sound.h"

extern u8 SOUNDBANK__0, SOUNDBANK__1;

u8 current_music;

void smr_sound_init() {

  current_music = 255;

  spcSetBank(&SOUNDBANK__1);
  spcSetBank(&SOUNDBANK__0);

  spcStop();
  spcLoad(MOD_EFFECTS);
  int e;
  for (e = 0; e < SMR_NB_SOUNDS; ++e) {
    spcLoadEffect(e);
  }
}

void smr_play_music(u8 music) {
  if (music != current_music) {
    current_music = music;
    spcStop();
    spcLoad(music);
    int e;
    for (e = 0; e < SMR_NB_SOUNDS; ++e) {
      spcLoadEffect(e);
    }
    spcPlay(music);
  }
}